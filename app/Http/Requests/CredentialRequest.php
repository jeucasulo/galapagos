<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CredentialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'author'=>'required|max:25',
          'email'=>'required|max:25',
          'phone'=>'required|max:20',
          'phone'=>'integer',
          'message'=>'required|max:500',
          'new'=>'required|max:20',
        ];
    }
    public function messages()
    {
      return [
          'author.required'=>'Favor inserir o nome',
          'email.required'=>'Favor inserir o email',
          'phone.required'=>'Favor inserir o telefone',
          'phone.integer'=>'Favor inserir somente números',
          'message.required'=>'Field required',
          'new.required'=>'Field required',
      ];
    }
    //rules options: max:number|min:number|email|unique:posts|bail|nullable|date

}
