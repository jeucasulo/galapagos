<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EcRestServerController extends Controller
{
    public function __construct()
    {
        $this->middleware('credential.check');
        $this->middleware('access_token.check');
        $this->middleware('auth');

    }


      // public function token(){
      //   // return \Auth::user()->activeCredential->token->access_token;
      //   return json_encode(\Auth::user()->activeCredential->token);
      // }
    public function token(){
      return \Auth::user()->activeCredential->token->access_token;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ec-rest-server.index');
    }
    public function clientV2()
    {
        $clientId = \Auth::user()->activeCredential->client_id;
        // dd($clientId);
        return view('ec-rest.clientV2',compact('clientId'));
    }
    public function server()
    {
        return view('ec-rest.server');
    }

    public function createPayment(){
      $accessToken = $this->token();

      $createPaymentJson = $_POST['createPaymentJson'];
      // file_put_contents('log/_createPaymentJsonCustom',$createPaymentJson);
      $createPaymentJsonCustom = $_POST['createPaymentJsonCustom'];
      // file_put_contents('log/_createPaymentJsonCustomBool',$createPaymentJsonCustom);
      $textareaJsonCreatePaymentHeader = $_POST['textareaJsonCreatePaymentHeader'];
      // $textareaJsonCreatePaymentHeader = str_replace("\\","",$textareaJsonCreatePaymentHeader);
      // file_put_contents('log/_textareaJsonCreatePaymentHeader',$textareaJsonCreatePaymentHeader);
      $createPaymentJsonMock = $_POST['createPaymentJsonMock'];

      file_put_contents('log/_ec-rest-server-create-payment-post',json_encode($_POST,JSON_PRETTY_PRINT));


      $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment";

      // $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken, $createPaymentJsonMock, $textareaJsonCreatePaymentHeader);
      // $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken,"PayPal-Mock-Response: {\"mock_application_codes\": \"MALFORMED_REQUEST\"}");
      file_put_contents('log/_headersCreatePaymentMock.json',json_encode($paymentHeaders));


      // JSON FORMAT SOURCE : https://developer.paypal.com/docs/api/overview/#make-your-first-call
      $postfields = '{
         "intent": "sale",
         "redirect_urls": {
           "return_url": "https://example.com/your_redirect_url.html",
           "cancel_url": "https://example.com/your_cancel_url.html"
         },
         "payer": {
           "payment_method": "paypal"
         },
         "transactions": [{
           "amount": {
             "total": "5.00",
             "currency": "BRL"
           }
         }]
       }';
      $postfields = ($createPaymentJsonCustom)?$createPaymentJson:$postfields;
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      $runObj = json_decode($run, 1);

      echo $run;

    }

    public function executePayment(){
      $accessToken = $this->token();

      $PaymentID = $_POST['paymentID'];
      $PayerID = $_POST['payerID'];

      $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment/".$PaymentID."/execute";
      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
      $postfields = "{\"payer_id\":\"$PayerID\"}";

      // source: https://developer.paypal.com/docs/api/payments/v1/#payment_execute

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      $runArray = json_decode($run, 1);

      // writes the execute payment response for checking purposes
      file_put_contents("executePaymentResponse.txt", $run);



      // return the response from the paypal site trought the curl post execute
      echo $run;
    }

    public function authcap(){
      return view('ec-rest.authcap');
    }

    public function createAuth(){
      $accessToken = $this->token();



      $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);


      // JSON FORMAT SOURCE : https://developer.paypal.com/docs/api/overview/#make-your-first-call
      $postfields = '{
         "intent": "authorize",
         "redirect_urls": {
           "return_url": "https://example.com/your_redirect_url.html",
           "cancel_url": "https://example.com/your_cancel_url.html"
         },
         "payer": {
           "payment_method": "paypal"
         },
         "transactions": [{
           "amount": {
             "total": "7.47",
             "currency": "BRL"
           }
         }]
       }';

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      file_put_contents("log/_createCaptureResponse.txt", $run);

      echo $run;

    }

    public function confirmAuth(){
      $accessToken = $this->token();

      $PaymentID = $_POST['paymentID'];
      $PayerID = $_POST['payerID'];

      $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment/".$PaymentID."/execute";
      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
      $postfields = "{\"payer_id\":\"$PayerID\"}";

      // source: https://developer.paypal.com/docs/api/payments/v1/#payment_execute

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      $runArray = json_decode($run, 1);

      // writes the execute payment response for checking purposes
      file_put_contents("log/_confirmAuthResponse.txt", $run);



      // return the response from the paypal site trought the curl post execute
      echo $run;
    }

    public function executeCap(){
      $accessToken = $this->token();
      $payIdToCap = $_POST['payIdToCap'];
      file_put_contents("log/_executeCaptureResponse.txt", $payIdToCap);


      $endpoint = "https://api.sandbox.paypal.com/v1/payments/authorization/".$payIdToCap."/capture";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);


      $postfields ='{
        "amount": {
          "currency": "BRL",
          "total": "7.47"
        },
        "is_final_capture": true
      }';

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);


      echo $run;

    }
    public function serverV2(){
      return view('ec-rest.serverV2');
    }

    public function createOrder(){
      // dd('test');
     $accessToken = $this->token();

     $url = "https://api.sandbox.paypal.com/v2/checkout/orders";

     $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

     $postfields = '{
        "intent": "CAPTURE",
        "purchase_units": [
          {
            "amount": {
              "currency_code": "USD",
              "value": "100.00"
            }
          }
        ]
      }';

     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($ch, CURLOPT_VERBOSE, 1);
     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
     curl_setopt($ch, CURLOPT_POST, true);
     $run = curl_exec($ch);
     curl_close($ch);

     file_put_contents("log/_createOrder.json", $run);
     echo $run;
    }

    public function captureOrder(Request $request){
      // $orderId = $_GET['orderId'];
      // $orderId = $request->orderID;
      // $orderId = $_POST['orderID'];
      // file_put_contents("log/_captureOrderApagar2.json", $_POST['orderID']);

      // dd($request);
      // $orderId = $orderId->$orderID;
      $extract = strstr($request, '{"orderID":');
      $extractOrderId = json_decode($extract);

      file_put_contents("log/_captureOrder.json", $extractOrderId->orderID);

      $accessToken = $this->token();

      $url = "https://api.sandbox.paypal.com/v2/checkout/orders/".$extractOrderId->orderID."/capture";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken,"PayPal-Request-Id: 7b92603e-77ed-4896-8e78-5dea2050476a");

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);
      $run = curl_exec($ch);
      curl_close($ch);

      // file_put_contents("log/_captureOrder.json", $run);

      echo $run;

      }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
