<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InvoicingController extends Controller
{
  public function __construct()
  {
    $this->middleware('credential.check');
    $this->middleware('access_token.check');
    $this->middleware('auth');

  }


  // public function token(){
  //   // return \Auth::user()->activeCredential->token->access_token;
  //   return json_encode(\Auth::user()->activeCredential->token);
  // }
  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('invoicing.index');
  }

  public function invoicingCreate(){
    $accessToken = $this->token();

    $url = "https://api.sandbox.paypal.com/v1/invoicing/invoices/";

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    // JSON FORMAT SOURCE : https://developer.paypal.com/docs/invoicing/integrate/create-and-send-invoices/#1-create-draft-invoice
    $postfields = '{
      "merchant_info": {
        "email": "jeucasulo-facilitator@hotmail.com",
        "first_name": "Jeú",
        "last_name": "Junior",
        "business_name": "Jeú Casulo",
        "phone": {
          "country_code": "055",
          "national_number": "4085551234"
        }
      },
      "billing_info": [{
        "email": "jeucasulo-buyer@hotmail.com",
        "first_name": "Stephanie",
        "last_name": "Meyers"
      }],
      "items": [{
        "name": "Zoom System wireless headphones",
        "quantity": 2,
        "unit_price": {
          "currency": "BRL",
          "value": "120"
        },
        "tax": {
          "name": "Tax",
          "percent": 8
        }
      }],
      "discount": {
        "percent": 1
      },
      "shipping_cost": {
        "amount": {
          "currency": "BRL",
          "value": "10"
        }
      },
      "note": "Thank you for your business.",
      "terms": "No refunds after 30 days."
    }';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);

    $runObj = json_decode($run, 1);

    $runJson = json_encode($runObj);

    file_put_contents('response_invoicing.json',$run);

    echo $run;
  }

  public function invoicingExecute(){
    $accessToken = $this->token();

    $invoice_id = $_POST['invoice_id'];

    $url = "https://api.sandbox.paypal.com/v1/invoicing/invoices/".$invoice_id."/send?notify_merchant=true?notify_customer=true";
    // file_put_contents('_url',$url);

    $headers = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

 // JSON FORMAT SOURCE : https://developer.paypal.com/docs/invoicing/integrate/create-and-send-invoices/#1-create-draft-invoice

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    curl_setopt($ch, CURLOPT_HEADER, 1);




    $run = curl_exec($ch);

    // Then, after your curl_exec call:
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($run, 0, $header_size);
    $body = substr($run, $header_size);


    curl_close($ch);


    // echo $run;
// $header
    file_put_contents('log/____headertest.txt',$header);
    // echo $header;

    //test
    //https://stackoverflow.com/questions/9183178/can-php-curl-retrieve-response-headers-and-body-in-a-single-request

    echo json_encode(array('Body'=>'No body response','Header'=>$header));
  }

}
