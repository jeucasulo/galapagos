<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RtCtbNoCfoController extends Controller
{

  public function __construct()
  {
    $this->middleware('credential.check');
    $this->middleware('access_token.check');
    $this->middleware('auth');
  }



  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $activeCredentialId = \Auth::user()->activeCredential->id;
    $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
    $hasToken = ($batoken->count()===1?true:false);

    // dd($hasToken);

    return view('rt-ctb-no-cfo.index',compact('hasToken'));
  }


  public function rtBaToken(){

    $accessToken = $this->token();

    $url = "https://api.sandbox.paypal.com/v1/billing-agreements/agreement-tokens";

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
    $postfields = '{
      "description": "Billing Agreement",
      "shipping_address":
      {
        "line1": "1350 North First Street",
        "city": "San Jose",
        "state": "CA",
        "postal_code": "95112",
        "country_code": "US",
        "recipient_name": "John Doe"
      },
      "payer":
      {
        "payment_method": "PAYPAL"
      },
      "plan":
      {
        "type": "MERCHANT_INITIATED_BILLING",
        "merchant_preferences":
        {
          "return_url": "https://example.com/return",
          "cancel_url": "https://example.com/cancel",
          "notify_url": "https://example.com/notify",
            "accepted_pymt_type": "INSTANT",
            "skip_shipping_address": false,
            "immutable_shipping_address": true
          }
        }
      }';

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      file_put_contents("log/_responseAgreementCreate.json", $run);

      echo $run;
    }


    public function rtBaCreate(){

      // file_put_contents('_token_id.txt', $token_id);
      $accessToken = $this->token();

      $token_id = $_POST['baToken'];

      // $path = file_get_contents('responseAgreementCreate.json') ;
      // $json = json_decode($path); // decode the JSON into an associative array
      // $BillingAgreementId = $json->id;
      // $BillingAgreementState = $json->state;
      // file_put_contents('_isToken.txt', 'id: '. $BillingAgreementId.' state: '.$BillingAgreementState);

      $url = "https://api.sandbox.paypal.com/v1/billing-agreements/agreements";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

      // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
      $postfields = '{
        "token_id": "'.$token_id.'"
      }';

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      file_put_contents("log/_responseAgreementCreate.json", $run);
      echo $run;
    }

    public function rtCtbCfo(){
      // return '{"tes":"te"}'

      $accessToken = $this->token();

      $activeCredential = \Auth::user()->activeCredential;
      file_put_contents('log/___activeCredential.json',json_encode($activeCredential));
      $activeCredentialId = \Auth::user()->activeCredential->id;
      $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
      $hasToken = ($batoken->count()===1?true:false);
      if($hasToken){
        // file_put_contents('log/____batoken.txt',$batoken);
        foreach($batoken as $key=>$value){
            if($value != null){
                $BillingAgreementId = $batoken[$key]->batoken;
              break;
            }
        }

        // file_put_contents('log/_RtCtb_BillingAgreementId.txt',$BillingAgreementId);

      }


      file_put_contents('log/_RtCtb_BillingAgreementId.txt',$BillingAgreementId);


      $finalValue = $_POST['finalValue'];

      $url = "https://api.sandbox.paypal.com/v1/credit/calculated-financing-options";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

      // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
      $postfields = '{
        "financing_country_code": "BR",
        "transaction_amount": {
          "value": "'.$finalValue.'",
          "currency_code": "BRL"
        },
        "funding_instrument": {
          "type": "BILLING_AGREEMENT",
          "billing_agreement": {
            "billing_agreement_id": "'.$BillingAgreementId.'"
          }
        }
      }';

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      // $runObj = json_decode($run, 1);
      //
      //
      // $runJson = json_encode($runObj);



      file_put_contents("log/_InstallsOff.json", $run);

      echo $run;

    }
    public function rtCtbBaRemove(){
        // dd('asdf');
        $activeCredentialId = \Auth::user()->activeCredential->id;
        $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
        $hasToken = ($batoken->count()===1?true:false);
        if($hasToken){

          foreach($batoken as $key=>$value){
              if($value != null){
                  $id = $batoken[$key]->id;
                break;
              }
          }

          // $batoken->delete();
          \App\Batoken::destroy($id);
        }
        $res = $id;
        // echo '{"reponse":'.$batoken[0].'}';
        echo '{"reponse":"Done"}';
      }


    public function rtCtbPayment(){
      // dd($_POST);

      $term = $_POST['term'];
      // $amount = $_POST['amount'];
      $discount = $_POST['discount'];
      $discountPercentage = $_POST['discountPercentage'];
      $amountFinal = $_POST['amountFinal'];
      $amountFinalMonthly = $_POST['amountFinalMonthly'];
      // $finalAmount = 95;
      file_put_contents('log/___RtCtbPost.json',json_encode($_POST));
      if($term == 1){
        $amountFinal = ($amountFinal+$discount);
      }

      if($term > 1){
        $discount = 0;
        $discountPercentage = 0;
      }

      $accessToken = $this->token();

      $activeCredentialId = \Auth::user()->activeCredential->id;

      //
      $activeCredential = \Auth::user()->activeCredential;
      file_put_contents('log/___activeCredential.json',json_encode($activeCredential));
      $activeCredentialId = \Auth::user()->activeCredential->id;
      $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
      $hasToken = ($batoken->count()===1?true:false);
      if($hasToken){
        // file_put_contents('log/____batoken.txt',$batoken);
        foreach($batoken as $key=>$value){
            if($value != null){
                $BillingAgreementId = $batoken[$key]->batoken;
              break;
            }
        }
      }

      file_put_contents('_APAGARtestBA_ID.txt',$BillingAgreementId);

      $url = "https://api.sandbox.paypal.com/v1/payments/payment";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

      $invoiceNumber = time();

      $postfields = '
      {
        "intent":"sale",
        "payer":{
          "payment_method":"paypal",
          "funding_instruments":[
            {
              "billing":{
                "billing_agreement_id":"'.$BillingAgreementId.'",
                "selected_installment_option":{
                  "term":"'.$term.'",
                  "monthly_payment":{
                    "value":"'.$amountFinalMonthly.'",
                    "currency":"BRL"
                  },
                  "discount_percentage":"'.$discountPercentage.'",
                  "discount_amount":{
                    "value":"'.$discount.'",
                    "currency":"BRL"
                  }
                }
              }
            }
          ]
        },
        "transactions":[
          {
            "amount":{
              "currency":"BRL",
              "total":"'.$amountFinal.'"
            },
            "description":"This is the description of the transaction",
            "custom":"custom123",
            "payment_options":{
              "allowed_payment_method":"IMMEDIATE_PAY"
            },
            "item_list":{
              "shipping_address":{
                "recipient_name":"Nome Recebedor",
                "line1":"Avenida Paulista, 1048",
                "line2":"Bela Vista",
                "city":"Sao Paulo",
                "country_code":"BR",
                "postal_code":"01310-100",
                "state":"Sao Paulo",
                "phone":"911111111"
              },
              "items":[
                {
                  "name":"Item Teste",
                  "description":"Descricao Item Teste",
                  "quantity":"1",
                  "price":"'.$amountFinal.'",
                  "tax":"0",
                  "sku":"sku123",
                  "currency":"BRL"
                }
              ]
            }
          }
        ],
        "redirect_urls":{
          "return_url":"https://www.yourreturnurlhere.com",
          "cancel_url":"https://www.yourcancelurlhere.com"
        }
      }';


      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);




      file_put_contents("log/_create_payment_response.json", $run);

      echo $run;




    // echo json_encode($_POST);
    // echo json_encode($_POST);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
