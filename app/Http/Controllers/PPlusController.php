<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PPlusController extends Controller
{


  public function __construct()
  {
    $this->middleware('credential.check');
    $this->middleware('access_token.check');
    $this->middleware('auth');
  }


  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('pplus.pplus');
  }


  public function createPayment(){
    $accessToken = $this->token();



    $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment";

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    $totalPostfields = '100.00';
    $subtotalPostfields = '100.00';
    $pricePostfields = '100.00';

    $postfields = '{
      "intent":"sale",
      "redirect_urls":{
        "return_url":"www.sometest.com/success",
        "cancel_url":"www.sometest.com/cancel"
      },
      "payer":{
        "payment_method":"paypal"
      },
      "application_context":{
        "brand_name":"PayPalPlus Test",
        "shipping_preference":"NO_SHIPPING"
      },
      "transactions":[
        {
          "amount":{
            "total":"'.$totalPostfields.'",
            "currency":"BRL",
            "details":{
              "subtotal":"'.$subtotalPostfields.'"
            }
          },
          "description":"Order From Test.com",
          "payment_options":{
            "allowed_payment_method":"IMMEDIATE_PAY"
          },
          "item_list":{
            "items":[
              {
                "name":"God Of War",
                "description":"God Of War ps4",
                "quantity":1,
                "price":"'.$pricePostfields.'",
                "sku":"#33",
                "currency":"BRL"
              }
            ]
          }
        }
      ]
    }';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);



    file_put_contents('log/_createPaymentResponse.json',$run);
    echo $run;

  }

  public function executePayment(){
    // file_put_contents('log/_executeURL.txt','execute...');
    $accessToken = $this->token();

    // $PaymentID = $_POST['paymentID'];
    $ExecuteURL = $_POST['executeURL'];
    $PayerID = $_POST['payerID'];
    file_put_contents('log/_executeURL.txt',$ExecuteURL);
    file_put_contents('log/_payerID.txt',$PayerID);

    // $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment/".$PaymentID."/execute";
    $endpoint = $ExecuteURL;
    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
    // $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken,"PayPal-Mock-Response:{\"mock_application_codes\":\"INSTRUMENT_DECLINED\"}");
    $postfields = "{\"payer_id\":\"$PayerID\"}";

    // source: https://developer.paypal.com/docs/api/payments/v1/#payment_execute

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    // $run = curl_exec($ch);

    curl_close($ch);

    $runArray = json_decode($run, 1);

    // writes the execute payment response for checking purposes
    // file_put_contents("log/_executePaymentResponse.json", $run);



    // return the response from the paypal site trought the curl post execute
    echo json_encode(['test'=>'testando']);
  }


  // CTB //




  public function indexCTB()
  {
    return view('pplus.pplus-ctb');
  }


  public function createPaymentCTB(Request $request){
    $accessToken = $this->token();

    $installments =  $_POST['installments'];
    $finalAmount = $_POST['finalAmount'];
    file_put_contents('log/_finalAmount.txt',$installments);

    // $installments =  $_GET['installments'];
    // $finalAmount = $_GET['finalAmount'];
    $finalAmount -= 1; // in the js file I had to add 1+ to shipping, and here i have to remove -1

    //   $total = 100 + (100/$installments);
    $url = "https://api.sandbox.paypal.com/v1/payments/payment";

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);



    $postfields = '{
      "intent":"sale",
      "redirect_urls":{
        "return_url":"www.sometest.com/success",
        "cancel_url":"www.sometest.com/cancel"
      },
      "payer":{
        "payment_method":"paypal"
      },
      "application_context":{
        "brand_name":"PayPalPlus Test",
        "shipping_preference":"NO_SHIPPING"
      },
      "transactions":[
        {
          "amount":{
            "total":"101.00",
            "currency":"BRL",
            "details":{
              "subtotal":"101.00"
            }
          },
          "description":"Order From Test.com",
          "payment_options":{
            "allowed_payment_method":"IMMEDIATE_PAY"
          },
          "item_list":{
            "items":[
              {
                "name":"God Of War",
                "description":"God Of War ps4",
                "quantity":1,
                "price":"101.00",
                "sku":"#33",
                "currency":"BRL"
              }
            ]
          }
        }
      ]
    }';

    $data = array(
      "intent" => "sale",
      "payer" => array(
        "payment_method" => "paypal"
      ),
      "application_context" => array(
        "brand_name"=>"PayPalPlus Test",
        "shipping_preference"=>"SET_PROVIDED_ADDRESS"
      ),

      "transactions" => array(
        array(
          "amount" => array(
            "total" => $finalAmount+1,
            "currency" => "BRL",
            "details" => array(
              "subtotal" => $finalAmount,
              "shipping" => "1",
              // "shipping_discount" => "-1.00"///
            )
          ),


          "description" => "The payment transaction description.",
          "payment_options" => array(
            "allowed_payment_method" => "IMMEDIATE_PAY"
          ),
          "item_list" => array(
            "items" => array(
              array(
                "name" => "hat",
                "description" => "Brown hat.",
                "quantity" => "1",
                "price" => $finalAmount,
                "sku" => "1",
                "currency" => "BRL"
              )
            ),
            "shipping_address" =>  array(
              "recipient_name"  =>  "Luiz Fabio Rezende Antunes",
              "line1" =>  "Rua Sara Braune, 91",
              "line2" =>  "Braunes",
              "city" =>  "Nova Friburgo",
              "country_code" =>  "BR",
              "postal_code" =>  "28633000",
              "state" =>  "RJ",
              "phone" =>  "2225265283"
            )
          )
        )
      ),
      "note_to_payer" => "Contact us for any questions on your order.",
      "redirect_urls" => array(
        "return_url" => "https://www.amorebrasil.com.br/return",
        "cancel_url" => "https://www.amorebrasil.com.br/cancel"
      )
    );

    $data = json_encode($data);
    // file_put_contents('_jsonFromLuiz.json', $data);


    // $postfields = json_encode($postfields);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    //   curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);

    $runObj = json_decode($run);

    $approval = $runObj->links[1]->href;
    $execute = $runObj->links[2]->href;

    file_put_contents('_approval.txt',$approval);

    //   $_SESSION['approval_url'] = $approval;
    // $_SESSION['execute'] = $execute;




    //   echo "<script>console.log($run)</script>";
    $myArray = json_decode($run,1);
    //
    function recursive($array, $level = 1){
      foreach($array as $key => $value){
        //If $value is an array.
        if(is_array($value)){
          //We need to loop through it.
          recursive($value, $level + 1);
        } else{
          //It is not an array, so print it out.
          // echo "<span class='text-danger'>" . $key . "</span>: " . $value, '<br>';
        }
      }
    }
    // recursive($myArray,1);
    //

    file_put_contents("log/_create_payment_response-pplus-ctb.json", $run);

    echo $run;
  }

  public function executePaymentCTB(){
    // file_put_contents('log/_executeURL.txt','execute...');
    $accessToken = $this->token();

    // $PaymentID = $_POST['paymentID'];
    $ExecuteURL = $_POST['executeURL'];
    $PayerID = $_POST['payerID'];
    file_put_contents('log/_executeURL.txt',$ExecuteURL);
    file_put_contents('log/_payerID.txt',$PayerID);

    // $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment/".$PaymentID."/execute";
    $endpoint = $ExecuteURL;
    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
    $postfields = "{\"payer_id\":\"$PayerID\"}";

    // source: https://developer.paypal.com/docs/api/payments/v1/#payment_execute

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);

    $runArray = json_decode($run, 1);

    // writes the execute payment response for checking purposes
    file_put_contents("log/_executePaymentResponse.json", $run);



    // return the response from the paypal site trought the curl post execute
    echo $run;
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
