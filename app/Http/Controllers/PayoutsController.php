<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PayoutsController extends Controller
{
  public function __construct()
  {
    $this->middleware('credential.check');
    $this->middleware('access_token.check');
    $this->middleware('auth');

  }


  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('payouts.index');
  }


  public function payoutsExecute(){
    $accessToken = $this->token();

    $url = "https://api.sandbox.paypal.com/v1/payments/payouts";

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

   // $test = "test123";
    $batch = date("Y-m-d H:i:s");
    $postfields ='{
       "sender_batch_header": {
         "sender_batch_id": "Payouts_2019_'.$batch.'",
         "email_subject": "You have a payout!",
         "email_message": "You have received a payout! Thanks for using our service!"
       },
       "items": [
         {
           "recipient_type": "EMAIL",
           "amount": {
             "value": "666.66",
             "currency": "BRL"
           },
           "note": "Thanks for your patronage!",
           "sender_item_id": "201403140001",
           "receiver": "jeucasulo-buyer@hotmail.com"
         },
         {
           "recipient_type": "EMAIL",
           "amount": {
             "value": "5.32",
             "currency": "BRL"
           },
           "note": "Thanks for your patronage!",
           "sender_item_id": "201403140003",
           "receiver": "renan.alvs-buyer@gmail.com"
         },
         {
           "recipient_type": "EMAIL",
           "amount": {
             "value": "5.32",
             "currency": "BRL"
           },
           "note": "Thanks for your patronage!",
           "sender_item_id": "201403140003",
           "receiver": "jessica.h.lopez-buyer@gmail.com"
         }

       ]
     }';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);
    $run = curl_exec($ch);
    curl_close($ch);

    echo $run;
  }

}
