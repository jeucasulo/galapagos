<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubController extends Controller
{

  public function __construct()
  {
    $this->middleware('credential.check');
    $this->middleware('access_token.check');
    $this->middleware('auth');
  }



  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(){
    $activeCredentialId = \Auth::user()->activeCredential->id;
    // $plan = \App\Plan::all()->where('credential_id',$activeCredentialId)->first();
    $plan = Auth()->user()->activeCredential->plan === null? false :  Auth()->user()->activeCredential->plan;

    // dd($plan->plan);

    // $hasPlan = ($plan->count()===1?true:false);
    if($plan){
      // $isActive = $this->subPlanCheck();
      // $isActive =  json_decode($isActive);
      $myPlan = $this->subPlanCheck($plan->plan);
      $isActive = json_decode($myPlan);
      $isActive = $isActive->state;
      // dd($isActive);
      // dd($this->subPlanCheck($plan->plan));
      // $isActive =  ($plan->state==='ACTIVE')?true:false;
      $isActive =  ($isActive==='ACTIVE')?true:false;
      $hasPlan = true;
      // dd($isActive);
    }else{
      $isActive = false;
      $hasPlan = false;
    }


    // dd($hasPlan);

    return view('sub.index',compact('hasPlan','isActive'));
  }

  public function subPlanCreate(){
    if($planId = Auth()->user()->activeCredential->plan !== null){
      return json_encode(array('error'=>'Plan already exists'));
    }

    // return \Auth::user()->activeCredential;
    // return \App\Plan::all()->where('credential_id',4)->count();
    $accessToken = $this->token();
    //
    $url = "https://api.sandbox.paypal.com/v1/payments/billing-plans/";
    //
    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    // // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement

    $postfields = '{
      "name": "Plan with Regular and Trial Payment Definitions",
      "description": "Plan with regular and trial payment definitions.",
      "type": "FIXED",
      "payment_definitions": [
        {
          "name": "Regular payment definition",
          "type": "REGULAR",
          "frequency": "MONTH",
          "frequency_interval": "2",
          "amount": {
            "value": "100",
            "currency": "BRL"
          },
          "cycles": "12",
          "charge_models": [
            {
              "type": "SHIPPING",
              "amount": {
                "value": "10",
                "currency": "BRL"
              }
            },
            {
              "type": "TAX",
              "amount": {
                "value": "12",
                "currency": "BRL"
              }
            }
          ]
        },
        {
          "name": "Trial payment definition",
          "type": "TRIAL",
          "frequency": "WEEK",
          "frequency_interval": "5",
          "amount": {
            "value": "9.19",
            "currency": "BRL"
          },
          "cycles": "2",
          "charge_models": [
            {
              "type": "SHIPPING",
              "amount": {
                "value": "1",
                "currency": "BRL"
              }
            },
            {
              "type": "TAX",
              "amount": {
                "value": "2",
                "currency": "BRL"
              }
            }
          ]
        }
      ],
      "merchant_preferences": {
        "setup_fee": {
          "value": "1",
          "currency": "BRL"
        },
        "return_url": "https://29428e7dd9774defbbde15e87e849c88.vfs.cloud9.us-east-2.amazonaws.com/?r=return",
        "cancel_url": "https://29428e7dd9774defbbde15e87e849c88.vfs.cloud9.us-east-2.amazonaws.com/?r=cancel",
        "auto_bill_amount": "YES",
        "initial_fail_amount_action": "CONTINUE",
        "max_fail_attempts": "0"
      }
    }';


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);

    // file_put_contents("log/_SubCreatePlan.json", json_encode(json_decode($run),JSON_PRETTY_PRINT));


    $runObj = json_decode($run);

    $plan = new \App\Plan();

    $plan->plan = $runObj->id;
    $plan->state = $runObj->state;
    $plan->app_name = \Auth::user()->activeCredential->app_name;
    $plan->credential_id = \Auth::user()->activeCredential->id;
    $plan->status = 'enabled';
    $plan->save();

    // $batoken = new \App\Batoken();
    // $batoken->batoken = json_decode($run)->id;
    // $batoken->app_name = \Auth::user()->activeCredential->app_name;
    // // $batoken->app_id_name = \Auth::user()->activeCredential->id;
    // $batoken->credential_id = \Auth::user()->activeCredential->id;
    // $batoken->status = 'enabled';
    // $batoken->save();


    echo $run;
  }
  public function subPlanCheck(){
    $accessToken = $this->token();

    $plan = Auth()->user()->activeCredential->plan === null? false :  Auth()->user()->activeCredential->plan;
    if(!$plan){
      return json_encode(array('error'=>'No plan found'));
    }else{

    $activeCredentialId = \Auth::user()->activeCredential->id;

    // $plan = \App\Plan::all()->where('credential_id',$activeCredentialId);
    // $plan = \App\Plan::all()->where('credential_id',$activeCredentialId)->first();
    // return $plan;
    // file_put_contents('log/_subTest.txt',json_decode($plan[0]->id));
    // file_put_contents('log/_subTest.txt',$plan[0]->plan);
    // return
    // return $plan->count();
    // $hasPlan = ($plan->count()===1?true:false);
    // return $plan;
    if(!$plan){
      return json_encode(array('Error'=>$plan->count()));
    }
    $pId = $plan->plan;


    // $pId = $hasPlan->id;
    // file_put_contents('log/_subPlanId.txt',$plan);

    $url = "https://api.sandbox.paypal.com/v1/payments/billing-plans/".$pId."";
    //   $url = "https://api.sandbox.paypal.com/v1/payments/payment/PAY-0US81985GW1191216KOY7OXA ";
    // file_put_contents('log_/url.txt',$url);

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    //   curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, false);

    $run = curl_exec($ch);
    // file_put_contents('log/_____plan.txt',$run);


    curl_close($ch);
    // echo $run;
    $runObj = json_decode($run);
    return json_encode(array("id"=>$runObj->id,"state"=>$runObj->state));
    }
  }
  public function subPlanActive(){
    $accessToken = $this->token();
    $plan = $this->subPlanCheck();
    // return $plan;
    // return $plan;
    $planArray = json_decode($plan);
    if($planArray->state === 'ACTIVE'){
      return json_encode(array("id"=>$planArray->id,"state"=>$planArray->state));
    }

    // return json_encode(array("id"=>$planArray->id,"state"=>$planArray->state));


    // $url = "https://api.sandbox.paypal.com/v1/payments/billing-plans/P-95A43884C4161510XWICBWVY";
    $url = "https://api.sandbox.paypal.com/v1/payments/billing-plans/".$planArray->id."/";
    // file_put_contents('log/_subTest.txt',$url);
    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
    // // // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
    $postfields = '[{
      "op": "replace",
      "path": "/",
      "value":
      {
        "state": "ACTIVE"
      }
    }]';


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

    // curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);

    file_put_contents("log/_SubActivePlan.json", json_encode(json_decode($run),JSON_PRETTY_PRINT));

    echo json_encode(array($planArray->id => 'ativado com sucesso'));
  }
  public function subGetPlan(){
    $accessToken = $this->token();

    $activeCredentialId = \Auth::user()->activeCredential->id;

    // $plan = \App\Plan::all()->where('credential_id',$activeCredentialId)->get(); error
    // $plan = \App\Plan::all()->where('credential_id',$activeCredentialId)->first(); gives 2
    $plan = \App\Plan::all()->where('credential_id',$activeCredentialId); // gives 2
    // $plan = \App\Plan::where('credential_id',$activeCredentialId);
    // return $plan->count();


    $hasPlan = ($plan->count()===1?true:false);
    // return json_encode($hasPlan);
    // if(!$hasPlan){
      // return json_encode(array('Error'=>$plan->count()));
    // }

    if($hasPlan){

      foreach($plan as $key=>$value){
          if($value != null){
              $planId = $plan[$key]->plan;
            break;
          }
      }
      // foreach($plan as $key=>$value){
      //     if($value != null){
      //         $planId = $planId[$key]->batoken;
      //       break;
      //     }
      // }
      $planId = $planId;

      // return json_encode($planId);
    }
      // $batoken->delete();
      // \App\Batoken::destroy($id);
    // }


    // return $hasPlan;
    // return json_encode(gettype($plan));
    // $planId = $plan->plan;


    // return $planId;

    $url = "https://api.sandbox.paypal.com/v1/payments/billing-plans/".$planId."";
    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, false);
    $run = curl_exec($ch);
    curl_close($ch);
    return $run;

    // return json_encode($plan);
  }
  public function subPlanRemove(){
    $accessToken = $this->token();

    $plan = $this->subGetPlan();
    // return $plan;
    $planId = json_decode($plan)->id;

    // return json_encode($plan);
    //
    // $accessToken = $this->token();

    $url = "https://api.sandbox.paypal.com/v1/payments/billing-plans/".$planId."/";
    // file_put_contents('log/_subTest.txt',$url);
    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
    // // // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
    $postfields = '[{
      "op": "replace",
      "path": "/",
      "value":
      {
        "state": "INACTIVE"
      }
    }]';


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
    $run = curl_exec($ch);
    curl_close($ch);

    $plan = $this->subGetPlan();

    $activeCredentialId = \Auth::user()->activeCredential->id;
    $planDB = \App\Plan::where('credential_id',$activeCredentialId)->first();
    // $hasPlan = ($planDB->count()===1?true:false);
    // if(!$hasPlan){
    //   return json_encode(array('Error'=>$plan->count()));
    // }

    \App\Plan::destroy($planDB->id);




    // return json_encode($plan->id);
    // Remove
    return json_encode($plan);

  }


  public function subBa(){

    $accessToken = $this->token();

    $plan = $this->subPlanCheck();
    $planArray = json_decode($plan);
    $pId = $planArray->id;

    file_put_contents('log/___plan.txt',$pId);




    $url = "https://api.sandbox.paypal.com/v1/payments/billing-agreements/";


    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    // $currentDay =  date('d');
    $tomorrow =  date('d')+1;
    $currentMounth =  date('m');
    $currentYear =  date('Y');

    // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
    $postfields = '{
      "name": "Magazine Subscription  ",
      "description": "Monthly agreement with a regular monthly payment definition and two-month trial payment definition.",
      "start_date": "'.$currentYear.'-'.$currentMounth.'-'.$tomorrow.'T09:13:49Z",
      "plan":
      {
        "id": "'.$pId.'"
      },
      "payer":
      {
        "payment_method": "paypal"
      },
      "shipping_address":
      {
        "line1": "751235 Stout Drive",
        "line2": "0976249 Elizabeth Court",
        "city": "Quimby",
        "state": "IA",
        "postal_code": "51049",
        "country_code": "BR"
      }
    }';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);

    file_put_contents("log/___responseAgreementCreate.json", $run);

    echo $run;
  }

  public function subBaExecute(){
    // https://developer.paypal.com/docs/archive/subscriptions/integrate/integrate-steps/#5-execute-an-agreement
    $accessToken = $this->token();

    // $plan = $this->subPlanCheck();
    // $planArray = json_decode($plan);
    // $pId = $planArray->id;

    // file_put_contents('log/___plan.txt',$pId);

    $ecToken = $_POST['ecToken'];


    $url = "https://api.sandbox.paypal.com/v1/payments/billing-agreements/" .$ecToken. "/"."agreement-execute/";
    // $url = "https://api.sandbox.paypal.com/v1/payments/billing-agreements/EC-7WN97463LN263864T/agreement-execute/";


    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    // $currentDay =  date('d');
    // $tomorrow =  date('d')+1;
    // $currentMounth =  date('m');
    // $currentYear =  date('Y');

    // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, true);

    $run = curl_exec($ch);

    curl_close($ch);

    file_put_contents("log/___responseAgreementExecute.json", $run);
    file_put_contents("log/___responseAgreementEcToken.json", $ecToken);

    echo $run;
  }

  public function rtBaRemove(){
    // dd('asdf');
    $activeCredentialId = \Auth::user()->activeCredential->id;
    $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
    return $batoken;
    $hasToken = ($batoken->count()===1?true:false);
    if($hasToken){

      foreach($batoken as $key=>$value){
        if($value != null){
          $id = $batoken[$key]->id;
          break;
        }
      }

      // $batoken->delete();
      \App\Batoken::destroy($id);
    }
    $res = $id;
    // echo '{"reponse":'.$batoken[0].'}';
    echo '{"reponse":"Done"}';
  }

  public function rtPayment(){
    // if(file_exists('REF-REST/_responseAgreementCreate.json')){ //php
    //   // if(file_exists('_responseAgreementCreate.json')){ //js
    //   $tokenAgreement->check = true;
    // }else{
    //   $tokenAgreement->check = false;
    // }

    // $path = file_get_contents('_responseAgreementCreate.json') ;
    // $json = json_decode($path); // decode the JSON into an associative array
    // $BillingAgreementId = $json->id;
    // $BillingAgreementState = $json->state;
    $accessToken = $this->token();

    $activeCredential = \Auth::user()->activeCredential;
    file_put_contents('log/___activeCredential.json',json_encode($activeCredential));
    $activeCredentialId = \Auth::user()->activeCredential->id;
    $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
    $hasToken = ($batoken->count()===1?true:false);
    if($hasToken){
      // file_put_contents('log/____batoken.txt',$batoken);
      foreach($batoken as $key=>$value){
        if($value != null){
          $BillingAgreementId = $batoken[$key]->batoken;
          break;
        }
      }
    }

    file_put_contents('log/_BillingAgreementId',$BillingAgreementId);

    $url = "https://api.sandbox.paypal.com/v1/payments/payment";

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    $invoiceNumber = time();
    file_put_contents('log/_invoiceNumber.txt', $invoiceNumber);

    $postfields = '{
      "intent": "sale",
      "application_context":
      {
        "shipping_preference":"NO_SHIPPING"
      },
      "payer":
      {
        "payment_method": "PAYPAL",
        "funding_instruments": [
          {
            "billing":
            {
              "billing_agreement_id": "'.$BillingAgreementId.'"
            }
          }]
        },
        "transactions": [
          {
            "amount":
            {
              "currency": "BRL",
              "total": "100.00"
            },
            "description": "Payment transaction.",
            "custom": "Payment custom field.",
            "note_to_payee": "Note to payee field.",
            "invoice_number": "'.$invoiceNumber.'",
            "item_list":
            {
              "items": [
                {
                  "sku": "skuitemNo1",
                  "name": "ItemNo1",
                  "description": "The item description.",
                  "quantity": "1",
                  "price": "100.00",
                  "currency": "BRL"
                }]
              },
              "payment_options":
              {
                "allowed_payment_method":"IMMEDIATE_PAY"
              }
            }],
            "redirect_urls":
            {
              "return_url": "https://example.com/return",
              "cancel_url": "https://example.com/cancel"
            }
          }';
          file_put_contents('_apagarPostField',$postfields);

          $ch = curl_init();

          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

          curl_setopt($ch, CURLOPT_VERBOSE, 1);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_POST, true);

          $run = curl_exec($ch);

          curl_close($ch);

          $runObj = json_decode($run);


          file_put_contents("_rt_payment_response.json", $run);

          echo $run;

        }


        /**
        * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
        */
        public function create()
        {
          //
        }

        /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
        public function store(Request $request)
        {
          //
        }

        /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function show($id)
        {
          //
        }

        /**
        * Show the form for editing the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function edit($id)
        {
          //
        }

        /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function update(Request $request, $id)
        {
          //
        }

        /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function destroy($id)
        {
          //
        }
      }
