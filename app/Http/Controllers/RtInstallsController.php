<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RtInstallsController extends Controller
{

  public function __construct()
  {
    $this->middleware('credential.check');
    $this->middleware('access_token.check');
    $this->middleware('auth');
  }



  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(){
    $activeCredentialId = \Auth::user()->activeCredential->id;
    $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
    $hasToken = ($batoken->count()===1?true:false);

    // dd($hasToken);

    return view('rt-installs.index',compact('hasToken'));
  }


  public function rtInstallsBaToken(){

    if(Auth()->user()->activeCredential->batoken !== null){
      return json_encode(array('error'=>'Token already exists'));
    }


    $accessToken = $this->token();

    $url = "https://api.sandbox.paypal.com/v1/billing-agreements/agreement-tokens";

    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
    $postfields = '{
      "description": "Billing Agreement",
      "shipping_address":
      {
        "line1": "1350 North First Street",
        "city": "San Jose",
        "state": "CA",
        "postal_code": "95112",
        "country_code": "US",
        "recipient_name": "John Doe"
      },
      "payer":
      {
        "payment_method": "PAYPAL"
      },
      "plan":
      {
        "type": "MERCHANT_INITIATED_BILLING",
        "merchant_preferences":
        {
          "return_url": "https://example.com/return",
          "cancel_url": "https://example.com/cancel",
          "notify_url": "https://example.com/notify",
            "accepted_pymt_type": "INSTANT",
            "skip_shipping_address": false,
            "immutable_shipping_address": true
          }
        }
      }';

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      // file_put_contents("log/_responseAgreementCreate.json", $run);
      // file_put_contents("log/_batoken.json", json_decode($run)->token_id);

      echo $run;
    }


    public function rtInstallsBaCreate(){

      $accessToken = $this->token();

      $token_id = $_POST['baToken'];

      file_put_contents('log/_token_id.txt', $token_id);


      // $path = file_get_contents('responseAgreementCreate.json') ;
      // $json = json_decode($path); // decode the JSON into an associative array
      // $BillingAgreementId = $json->id;
      // $BillingAgreementState = $json->state;
      // file_put_contents('_isToken.txt', 'id: '. $BillingAgreementId.' state: '.$BillingAgreementState);

      $url = "https://api.sandbox.paypal.com/v1/billing-agreements/agreements";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

      // JSON FORMAT SOURCE : https://developer.paypal.com/docs/subscriptions/integrate/integrate-steps/#3-create-an-agreement
      $postfields = '{
        "token_id": "'.$token_id.'"
      }';

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);

      file_put_contents("log/_responseAgreementCreate.json", $run);

      $batoken = new \App\Batoken();
      $batoken->batoken = json_decode($run)->id;
      $batoken->app_name = \Auth::user()->activeCredential->app_name;
      // $batoken->app_id_name = \Auth::user()->activeCredential->id;
      $batoken->credential_id = \Auth::user()->activeCredential->id;
      $batoken->status = 'enabled';
      $batoken->save();

      echo $run;
    }

    public function rtInstallsBaRemove(){
      // dd('asdf');
      $activeCredentialId = \Auth::user()->activeCredential->id;
      $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
      $hasToken = ($batoken->count()===1?true:false);
      if($hasToken){

        foreach($batoken as $key=>$value){
          if($value != null){
            $id = $batoken[$key]->id;
            break;
          }
        }
        foreach($batoken as $key=>$value){
          if($value != null){
            $BillingAgreementId = $batoken[$key]->batoken;
            break;
          }
        }
        $baId = $BillingAgreementId;

        // $batoken->delete();
        \App\Batoken::destroy($id);
      }
      $res = $id;

      // echo json_encode(array($BillingAgreementId));
      // echo '{"reponse":'.$BillingAgreementId.'}';
      // echo '{"reponse":"Done"}';


      $accessToken = $this->token();

      $url = "https://api.sandbox.paypal.com/v1/billing-agreements/agreements/".$baId."/cancel";
      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
      // curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, true);

      $run = curl_exec($ch);

      curl_close($ch);
      // echo '{"reponse":"deletado"}';
      echo json_encode(array("Canceled"=>$baId));

    }

    public function rtInstallsPayment(Request $request){
      // if(file_exists('REF-REST/_responseAgreementCreate.json')){ //php
      //   // if(file_exists('_responseAgreementCreate.json')){ //js
      //   $tokenAgreement->check = true;
      // }else{
      //   $tokenAgreement->check = false;
      // }

      // $path = file_get_contents('_responseAgreementCreate.json') ;
      // $json = json_decode($path); // decode the JSON into an associative array
      // $BillingAgreementId = $json->id;
      // $BillingAgreementState = $json->state;
      $accessToken = $this->token();

      $activeCredential = \Auth::user()->activeCredential;
      file_put_contents('log/___activeCredential.json',json_encode($activeCredential));
      $activeCredentialId = \Auth::user()->activeCredential->id;
      $batoken = \App\Batoken::all()->where('credential_id',$activeCredentialId);
      $hasToken = ($batoken->count()===1?true:false);
      if($hasToken){
        // file_put_contents('log/____batoken.txt',$batoken);
        foreach($batoken as $key=>$value){
          if($value != null){
            $BillingAgreementId = $batoken[$key]->batoken;
            break;
          }
        }
      }

      // file_put_contents('log/_BillingAgreementId',$BillingAgreementId);

      $url = "https://api.sandbox.paypal.com/v1/payments/payment";

      $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

      $invoiceNumber = time();
      // file_put_contents('log/_invoiceNumber.txt', $invoiceNumber);

      $postfields = '{
        "intent": "sale",
        "application_context":
        {
          "shipping_preference":"NO_SHIPPING"
        },
        "payer":
        {
          "payment_method": "PAYPAL",
          "funding_instruments": [
            {
              "billing":
              {
                "billing_agreement_id": "'.$BillingAgreementId.'"
              }
            }]
          },
          "transactions": [
            {
              "amount":
              {
                "currency": "BRL",
                "total": "100.00"
              },
              "description": "Payment transaction.",
              "custom": "Payment custom field.",
              "note_to_payee": "Note to payee field.",
              "invoice_number": "'.$invoiceNumber.'",
              "item_list":
              {
                "items": [
                  {
                    "sku": "skuitemNo1",
                    "name": "ItemNo1",
                    "description": "The item description.",
                    "quantity": "1",
                    "price": "100.00",
                    "currency": "BRL"
                  }]
                },
                "payment_options":
                {
                  "allowed_payment_method":"IMMEDIATE_PAY"
                }
              }],
              "redirect_urls":
              {
                "return_url": "https://example.com/return",
                "cancel_url": "https://example.com/cancel"
              }
            }';

            $discount_percentage = 0;
            $discount_amount = 0;
            // $term = 2;
            $term = $request->term;
            // $term_amount = 50;
            $term_amount = $request->amountFinalMonthly;
            // $amount_total = 100;
            $amount_total = $request->amountFinal;

            $customJsonPayment = $request->customJsonPayment;
            $createPaymentJsonCustom = $request->createPaymentJsonCustom;

            file_put_contents('log/____customJsonPayment.txt', $customJsonPayment);
            file_put_contents('log/____customJsonPaymentBoolean.txt', $createPaymentJsonCustom);

            $postfieldsInstalls = '{
              "intent":"sale",
              "payer":{
                "payment_method":"paypal",
                "funding_instruments":[
                  {
                    "billing":{
                      "billing_agreement_id":"'.$BillingAgreementId.'",
                      "selected_installment_option":{
                        "term":"'.$term.'",
                        "monthly_payment":{
                          "value":"'.$term_amount.'",
                          "currency":"BRL"
                        },
                        "discount_percentage":"'.$discount_percentage.'",
                        "discount_amount":{
                          "value":"'.$discount_amount.'",
                          "currency":"BRL"
                        }
                      }
                    }
                  }
                ]
              },
              "transactions":[
                {
                  "amount":{
                    "currency":"BRL",
                    "total":"'.$amount_total.'"
                  },
                  "description":"This is the description of the transaction",
                  "custom":"custom123",
                  "payment_options":{
                    "allowed_payment_method":"IMMEDIATE_PAY"
                  },
                  "item_list":{
                    "shipping_address":{
                      "recipient_name":"Nome Recebedor",
                      "line1":"Avenida Paulista, 1048",
                      "line2":"Bela Vista",
                      "city":"Sao Paulo",
                      "country_code":"BR",
                      "postal_code":"01310-100",
                      "state":"Sao Paulo",
                      "phone":"911111111"
                    },
                    "items":[
                      {
                        "name":"Item Teste Ok",
                        "description":"Descricao Item Teste",
                        "quantity":"1",
                        "price":"'.$amount_total.'",
                        "tax":"0",
                        "sku":"sku123",
                        "currency":"BRL"
                      }
                    ]
                  }
                }
              ],
              "redirect_urls":{
                "return_url":"https://www.yourreturnurlhere.com",
                "cancel_url":"https://www.yourcancelurlhere.com"
              }
            }';

            if($createPaymentJsonCustom==='true'){
              $postfieldsInstalls = $customJsonPayment;
            }

            // file_put_contents('_apagarPostField',$postfields);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfieldsInstalls);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_POST, true);

            $run = curl_exec($ch);

            curl_close($ch);

            $runObj = json_decode($run);


            // file_put_contents("_rt_payment_response.json", $run);

            echo $run;

          }


          /**
          * Show the form for creating a new resource.
          *
          * @return \Illuminate\Http\Response
          */
          public function create()
          {
            //
          }

          /**
          * Store a newly created resource in storage.
          *
          * @param  \Illuminate\Http\Request  $request
          * @return \Illuminate\Http\Response
          */
          public function store(Request $request)
          {
            //
          }

          /**
          * Display the specified resource.
          *
          * @param  int  $id
          * @return \Illuminate\Http\Response
          */
          public function show($id)
          {
            //
          }

          /**
          * Show the form for editing the specified resource.
          *
          * @param  int  $id
          * @return \Illuminate\Http\Response
          */
          public function edit($id)
          {
            //
          }

          /**
          * Update the specified resource in storage.
          *
          * @param  \Illuminate\Http\Request  $request
          * @param  int  $id
          * @return \Illuminate\Http\Response
          */
          public function update(Request $request, $id)
          {
            //
          }

          /**
          * Remove the specified resource from storage.
          *
          * @param  int  $id
          * @return \Illuminate\Http\Response
          */
          public function destroy($id)
          {
            //
          }
        }
