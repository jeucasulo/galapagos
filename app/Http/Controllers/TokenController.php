<?php

namespace App\Http\Controllers;

use App\Token;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TokenController extends Controller
{

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('credential.check');
    $this->middleware('auth');

  }



  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {

    $activeCredential = \Auth::user()->activeCredential;
    // dd($activeCredential);
    // $activeToken = \Auth::user()->activeToken;
    $activeToken = \Auth::user()->activeCredential->token;
    // $activeToken = $activeToken->count() === 0 ? "Nenhum token encontrado" : $activeToken;

    // here
    $user = \Auth::user();
    // dd(\Auth::user()->activeCredential->token);
    if(\Auth::user()->activeCredential->token===null){
      $hasExpired = 'danger';
      $expiredTime = 'No token found...';
      $fullAccessToken = false;
    }else{
      $accessToken = \Auth::user()->activeCredential->token->access_token;
      // dd(\Auth::user()->activeCredential->token);
      $fullAccessToken = \Auth::user()->activeCredential->token;
      $nonce = \Auth::user()->activeCredential->token->nonce;
      $createTimeNonce = substr($nonce,0,19);
      $createTimeCarbon = Carbon::create($createTimeNonce);
      $createTime = Carbon::create($createTimeNonce);
      $expireTime = $createTimeCarbon->addSeconds(32400);
      $current = Carbon::now();
      $hasExpired = ($current->diffInSeconds($createTimeNonce)>32400)?'danger':'primary';
      $expiredTime = $current->diff($expireTime);
      $expiredTime = $current->diffAsCarbonInterval($expireTime);
    }


    // here

    return view('token.index',compact('activeCredential','activeToken','fullAccessToken','hasExpired','expiredTime'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create($clientId,$secret)
  {
    // $clientId = $_GET['clientId'];
    // $secret = $_GET['token'];

    $url = "https://api.sandbox.paypal.com/v1/oauth2/token";

    $headers = array( "Accept"=>"application/json",
    "Accept-Language"=>"en_US",
    "Content-Type"=>"application/x-www-form-urlencoded",
  );
  $postfields = "grant_type=client_credentials";
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

  curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_POST, true);

  $run = curl_exec($ch);

  // echo $run;

  curl_close($ch);

  // with the array cast (array) returns an array as well
  // $runObj = json_decode($run, 1);

  // writes the create payment response for checking purposes

  // $_SESSION["access_token"] = $runObj["access_token"];
  // $accessToken = $runObj["access_token"];

  // file_put_contents("responseGetToken.txt", $run);

  // return $accessToken;
  // echo $run;
  // dd($run);

  return $run;
  // dd('fudeu ');

}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
  // dd($request->app_id);
  // dd(\Auth::id());
  $token = $this->create($request->client_id,$request->secret);
  $tokenArray = json_decode($token);
  if(isset($tokenArray->error)){
    return redirect()->route('token.index')->with([
      'msg'=>$tokenArray->error_description."(".$tokenArray->error.")",
      'alert'=>'alert-danger'
    ]);
    // dd($tokenArray->error);
  }
  // dd($tokenArray);
  // $tokenSave = new \App\Token();
  // dd($tokenArray->access_token);
  // $tokenSave = new \App\Token();
  // \App\Token::truncate();
  // dd(\Auth::id());
  // dd(\App\credential::all());
  // $credential = \App\Credential::all()->where('app_owner',\Auth::id());
  // $tokens = \App\Token::all();
  // dd($tokens);
  ;
  // dd($credentials);
  // $models->update(['att'=>'foo']);
  // $credentials->update(['status'=>'disabled']);

  // dd(\Auth::user()->activeCredential->id);
  // dd(\Auth::user()->activeCredential->tokens);
  //
  // $tokens = \Auth::user()->tokens;
  $tokens = \Auth::user()->activeCredential->tokens;
  $tokens->each(function ($item){
    $item->delete();
  });



  $tokenSave = new \App\Token();
  $tokenSave->scope = $tokenArray->scope;
  $tokenSave->access_token = $tokenArray->access_token;
  $tokenSave->token_type = $tokenArray->token_type;
  $tokenSave->expires_in = $tokenArray->expires_in;
  $tokenSave->nonce = $tokenArray->nonce;
  $tokenSave->app_id = $tokenArray->app_id;
  $tokenSave->token_owner = \Auth::id();
  $tokenSave->app_name = $request->app_name;
  $tokenSave->app_desc = $request->app_desc;
  $tokenSave->credential_id = \Auth::user()->activeCredential->id;
  $tokenSave->save();

  // return->route('token.index')->with(['msg'=>'Token gerado com sucesso!','alert'=>'alert-success']);

  // return redirect()->route('token.index')->with([
  //   'msg'=>'Token criado com sucesso',
  //   'alert'=>'alert-success'
  // ]);
  // echo json_encode(array('token'=>'ok'));
  echo $token;

  // ], [
  //   'foo' => request()->foo
  // ]);

  // consertar o update acima123
  //
  //         $table->bigIncrements('id');
  //         $table->string('scope');
  //         $table->string('access_token');
  //         $table->string('token_type');
  //         $table->string('app_id');
  //         $table->string('expires_in');
  //         $table->string('nonce');
  //         $table->string('app_name');
  //         $table->string('app_desc');
  //         $table->string('token_owner');
  //         $table->string('status')->default('enabled');


}

/**
* Display the specified resource.
*
* @param  \App\Token  $token
* @return \Illuminate\Http\Response
*/
public function show(Token $token)
{
  //
}

/**
* Show the form for editing the specified resource.
*
* @param  \App\Token  $token
* @return \Illuminate\Http\Response
*/
public function edit(Token $token)
{
  //
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Token  $token
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Token $token)
{
  //
}

/**
* Remove the specified resource from storage.
*
* @param  \App\Token  $token
* @return \Illuminate\Http\Response
*/
public function destroy(Token $token)
{
  //
}
}
