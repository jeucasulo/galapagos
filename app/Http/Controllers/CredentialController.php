<?php

namespace App\Http\Controllers;

use App\Credential;
use Illuminate\Http\Request;

class CredentialController extends Controller
{

      public function __construct()
      {
          $this->middleware('auth');
      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(\Auth::user()->credentials);
        $user = \App\User::find(\Auth::id());
        // dd($user->credentials);
        // $credentials = \App\Credential::all();
        $credentials = $user->credentials;
        return view('credential.index', compact('credentials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('credential.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(\App\Http\Requests\MessageRequest $request)

    // public function store(\App\Http\Requests\CredentialRequest $request)
    public function store(Request $request)
    {
        // dd(\Auth::id());
        $credential = new \App\Credential();
        $credential->app_name = $request->app_name;
        $credential->app_owner = \Auth::id();
        $credential->app_desc = $request->app_desc;
        $credential->client_id = $request->client_id;
        $credential->secret = $request->secret;
        $credential->status = 'disabled';

        $credential->save();

        return redirect()->route('credential.index')->with([
          'msg'=>'Credenciais inseridas com sucesso',
          'alert'=>'alert-success'
        ]);

        // $table->string('app_name');
        // $table->integer('app_owner');
        // $table->string('app_desc');
        // $table->string('client_id');
        // $table->string('secret');
        // $table->string('status');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($credential);
        $credential = \App\Credential::find($id);
        // dd($credential);
        return view('credential.edit', compact('credential'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Credential $credential)
    public function update(Request $request, $id)
    {
      // $credential = MyModel::whereIn('id',[1,2,3]);
      $credentials = \Auth::user()->credentials;
      // dd($credentials);
      // $models->update(['att'=>'foo']);
      // $credentials->update(['status'=>'disabled']);
      $credentials->each(function ($item){
          $item->update(['status'=>'disabled']);
      });


      $credential = \App\Credential::find( $id);
      $credential->app_name = $request->app_name;
      $credential->app_owner = \Auth::id();
      $credential->app_desc = $request->app_desc;
      $credential->client_id = $request->client_id;
      $credential->secret = $request->secret;
      $credential->status = $request->status;

      $credential->save();

      return redirect()->route('credential.index')->with([
        'msg'=>'Credenciais atualizada com sucesso',
        'alert'=>'alert-success'
      ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    public function destroy(Credential $credential)
    {
        //
    }
}
