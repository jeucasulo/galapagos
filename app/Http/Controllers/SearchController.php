<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
  public function __construct()
  {
      $this->middleware('credential.check');
      $this->middleware('access_token.check');
      $this->middleware('auth');

  }

  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }

  // function jsonToHtml($iterator,$output,$array){
  //   foreach ($array as $key => $value) {
  //     $output .= "<li><b>".$key."</b>";
  //     if (is_array($value)){
  //       $iterator ++;
  //       echo "<br>looping (is array): " . $iterator;
  //       $output .= "<ul><li><b>".$key."</b><i> : ".jsonToHtml($iterator,$output,$value)." </i></li></ul>";
  //     }else{
  //       $iterator ++;
  //       echo "<br>looping (not array): " . $iterator;
  //       $output .= " : <i>".$value." </i>";
  //       $output .= "</li>";
  //       // return;
  //     }
  //     // break;
  //   }
  //   $output .= "</ul>";
  //   return $output;
  // }

  function recursive($array, $level = 1){
    if(!isset($output)){
      $output = "";
    }
    $output .= "<ul>";
    foreach($array as $key => $value){
      //If $value is an array.
      if(is_array($value)){
        $output .= "<li><b><a class='btn btn-primary btn-sm' href='/home/parameters#".$key."' role='button' target='_blank'>" .$key . "</a></b>  <span class='valueClass'><i>";
        $output .= $this->recursive($value, $level + 1);
        $output .= '</i></span></li>';
      } else{
        $output .= "<li><b><a class='btn btn-primary btn-sm' href='/home/parameters#".$key."' role='button' target='_blank'>".$key . "</a></b> <span class='valueClass'><i> - " . $value . "</i></span></li>";
      }
    }
    $output .= "</ul>";
    return $output;
  }

  public function customSearch($id){
    $accessToken = $this->token();
    $endpoint = "https://api.sandbox.paypal.com/v1/notifications/webhooks/".$id;
    // $endpoint = "https://api.sandbox.paypal.com/v1/notifications/webhooks/".$id;
    $paymentHeaders = array("Accept: application/json","Accept-Language: en_US","Content-Type: application/json", "Authorization: Bearer ".$accessToken);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, false);
    $run = curl_exec($ch);
    curl_close($ch);

    $search = "INVALID_RESOURCE_ID";

    // $os = array("Mac", "NT", "Irix", "Linux");
    // if (in_array("Irix", $os)) {
    if (!in_array($search, json_decode($run,1))) {
        // echo "webhook..";
        $res = $this->recursive(json_decode($run,1));
        // return view('search.payment',compact('res'));
        return $res;
    }else{
      // echo "transactions...";
      // echo $id;
      $accessToken = $this->token();
      $endpointElse = "https://api.sandbox.paypal.com/v1/payments/sale/".$id;
      $paymentHeadersElse = array("Accept: application/json","Accept-Language: en_US","Content-Type: application/json", "Authorization: Bearer ".$accessToken);
      $chElse = curl_init();
      curl_setopt($chElse, CURLOPT_URL, $endpointElse);
      curl_setopt($chElse, CURLOPT_HTTPHEADER, $paymentHeadersElse);
      curl_setopt($chElse, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($chElse, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($chElse, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($chElse, CURLOPT_VERBOSE, 1);
      curl_setopt($chElse, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($chElse, CURLOPT_POST, false);
      $runElse = curl_exec($chElse);
      curl_close($chElse);
      // file_put_contents('log/___details.json',$runElse);
      // dd($runElse);
      $resElse = $this->recursive(json_decode($runElse,1));
      file_put_contents('log/___details.json',$runElse);
      // return view('search.payment',compact('resElse'));
      return $resElse;

    }

    // if (in_array("mac", $os)) {
    //     echo "Tem mac";
    // }
    // dd($found);

  }

  public function search($id){
    // dd('dasf');
    if(strpos($id, 'PAY') !== false) {
      $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment/".$id;
    }else if(strpos($id, 'P-') !== false){
      $endpoint = "https://api.sandbox.paypal.com/v1/payments/billing-plans/".$id;
    }else if(strpos($id, 'BA-') !== false){
      $endpoint = "https://api.sandbox.paypal.com/v1/billing-agreements/agreement-tokens/".$id;
    }else if(strpos($id, 'B-') !== false){
      $endpoint = "https://api.sandbox.paypal.com/v1/billing-agreements/agreements/".$id;
    }else if(strpos($id, 'INV') !== false){
      $endpoint = "https://api.sandbox.paypal.com/v1/invoicing/invoices/".$id;
    }else if(strlen($id) === 13){
      $endpoint = "https://api.sandbox.paypal.com/v1/payments/payouts/".$id;
    }else if(strlen($id) === 17){

      $res = $this->customSearch($id);
      return view('search.payment',compact('res'));

    }else{
      return view("index"); // set error response
    }

    $accessToken = $this->token();

    // $endpoint = "https://api.sandbox.paypal.com/v1/payments/payment/".$id;
    // return '$accessToken';
    $paymentHeaders = array("Accept: application/json","Accept-Language: en_US","Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, false);
    $run = curl_exec($ch);
    curl_close($ch);


    $res = $this->recursive(json_decode($run,1));
    // dd($res);
    // dd($run);

    return view('search.payment',compact('res'));


  }

}
