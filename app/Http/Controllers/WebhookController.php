<?php

// credential
// 	appName
//
// webhook
// 	appName/appId / webhookId
//
//
// notifications
// 			webhookId
//
//
//
// 1-notification traz um webhookId
// 2-loop no webhook::all pra encontrar qual desses webhookId coincide
// 3-pega o appName/appId desse webhook e usa ele pra salvar no "credential_id" do eventsDB


namespace App\Http\Controllers;

use App\Webhook;
use Illuminate\Http\Request;

class WebhookController extends Controller
{

  public function __construct()
  {
    $this->middleware('credential.check',['except'=>[
      'listener','personalListener'
    ]]);
    $this->middleware('access_token.check',['except'=>[
      'listener','personalListener'
    ]]);
    $this->middleware('auth',['except'=>[
      'listener','personalListener'
    ]]);

  }

  public function token(){
    return \Auth::user()->activeCredential->token->access_token;
  }

  public function listener(Request $request){

    //https://webhook-jj.000webhostapp.com/listener.php
    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
      header('Location: ../../index.php');
      exit();
    }

    // $headersJson = (json_encode(apache_request_headers()));
    // $rawpost = file_get_contents('php://input');


    // file_put_contents('_apacheResquestHeadesrs.txt', gettype(apache_request_headers()));
    // file_put_contents('_apacheResquestHeadesrs.json', json_encode(apache_request_headers()));
    $headersJson = (json_encode(apache_request_headers())); // heroku test ok
    // file_put_contents('___apacheResquestHeadesrsVar.json', $headersJson);
    $headersArray = json_decode($headersJson, 1);
    // file_put_contents('___apacheResquestHeadesrsType.txt', gettype($headersArray));

    // file_put_contents('___apacheResquestHeadesrsConnection.txt', $headersArray['Connection']);

    $rawpost = file_get_contents('php://input'); // heroku test ok
    //   $request = "cmd=_notify-validate&" . $rawpost;

    // file_put_contents("_rawpost.json", $rawpost);
    // file_put_contents("_rawpostType.txt", gettype($rawpost));


    // file_put_contents("_webhookResponse.json", $rawpost);
    $webhookArray = json_decode($rawpost,1);
    // file_put_contents("_webhookArrayType.txt", gettype($webhookArray));
    // file_put_contents("_webhookArrayId.txt", $webhookArray['id']);



    // $raw_post_array = explode('&', $webhookArray);
    //  $myPost = array();
    //  foreach ($raw_post_array as $keyval) {
    //    $keyval = explode ('=', $keyval);
    //    if (count($keyval) == 2)
    //       $myPost[$keyval[0]] = urldecode($keyval[1]);
    //  }
    // file_put_contents("_webhookTest.json", json_encode($myPost));

    // $jsonCollectionArray = array();

    // if(file_exists('_webhookTestCollection.json')){
    //   $jsonCollection = file_get_contents("_webhookTestCollection.json");
    //   $jsonArrayCollectionFile = array();
    //   $jsonArrayCollectionNew = array();
    //   $jsonArrayCollectionFile = json_decode($jsonCollection,true);
    //   $jsonArrayCollectionNew['transaction'] = json_decode($rawpost);
    //   array_push( $jsonArrayCollectionFile, $jsonArrayCollectionNew );
    //   file_put_contents("_webhookTestCollection.json", json_encode($jsonArrayCollectionFile));
    // }else{
    //   $jsonCollectionArray[0]['transaction'] = json_decode($rawpost);
    //   file_put_contents("_webhookTestCollection.json", json_encode($jsonCollectionArray));
    // }



    // get token
    $clientId = "AWrMp7lycUDtsRSpJNWe_0Jm9vbo7ui316BUkrjwD_3Q6n8GU8kaPHPUNvh4QQITLfvjK67sL1GfUQLH";
    $secret = "EEpu0P1ta6IiDIT7OUPuJ3WT_f6leOuWZnyZUAYj9iyqBPLnz_AL1_Ic9y-GvZiTF8CkwntENIxgUseV";

    $url = "https://api.sandbox.paypal.com/v1/oauth2/token";

    $headers = array( "Accept"=>"application/json",
    "Accept-Language"=>"en_US",
    "Content-Type"=>"application/x-www-form-urlencoded",
  );
  $postfields = "grant_type=client_credentials";
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

  curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_POST, true);

  $run = curl_exec($ch);
  curl_close($ch);

  // with the array cast (array) returns an array as well
  $runObj = json_decode($run, 1);
  $accessToken = $runObj['access_token'];

















  //Handshake


  // $handshakeRequestArray = new stdClass();
  // $handshakeRequestArray = new \stdClass();
  // $handshakeRequestArray->auth_algo = '123';
  // $handshakeRequestArray->cert_url = '456';
  // $handshakeRequestArray->transmission_id = '789';
  // $handshakeRequestArray->transmission_sig = 'abc';
  // $handshakeRequestArray->transmission_time = 'def';
  // $handshakeRequestArray->webhook_id = "4GF03916SJ763193G";
  // $handshakeRequestArray->webhook_event  = 'lalala';
  // $handshakePostfield = json_encode($handshakeRequestArray);

  $handshakeRequestArray = new \stdClass();
  $handshakeRequestArray->auth_algo = $headersArray['Paypal-Auth-Algo'];
  $handshakeRequestArray->cert_url = $headersArray['Paypal-Cert-Url'];
  $handshakeRequestArray->transmission_id = $headersArray['Paypal-Transmission-Id'];
  $handshakeRequestArray->transmission_sig = $headersArray['Paypal-Transmission-Sig'];
  $handshakeRequestArray->transmission_time = $headersArray['Paypal-Transmission-Time'];
  $handshakeRequestArray->webhook_id = "4EM20171AA011874E"; // https://paypal-galapagos.herokuapp.com/webhook/listener
  $handshakeRequestArray->webhook_event  = json_decode($rawpost);
  // $handshakeRequestArray->webhook_event  = $rawpost;
  $handshakePostfield = json_encode($handshakeRequestArray);
  // json_encode($str, JSON_UNESCAPED_SLASHES);


  $urlHandshake = "https://api.sandbox.paypal.com/v1/notifications/verify-webhook-signature";

  $paymentHeadersHandshake = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
  // $paymentHeadersHandshake = array("Content-Type: application/json", "Authorization: Bearer A21AAFufDLdguKDtESdG1YssQHhqfXuXzDpWb89WjRQWckH7sHATWZpm9f7L7R6zWX2Dmvr11gLn8nQ09TXD7k0p3PO6Q5b1g");


  $chHandshake = curl_init();
  curl_setopt($chHandshake, CURLOPT_URL, $urlHandshake);
  curl_setopt($chHandshake, CURLOPT_HTTPHEADER, $paymentHeadersHandshake);
  curl_setopt($chHandshake, CURLOPT_POSTFIELDS, $handshakePostfield);
  curl_setopt($chHandshake, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($chHandshake, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($chHandshake, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($chHandshake, CURLOPT_VERBOSE, 1);
  curl_setopt($chHandshake, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($chHandshake, CURLOPT_POST, true);
  $runHandshake = curl_exec($chHandshake);
  curl_close($chHandshake);


  // save data
  $webhook = new \App\Webhook();

  $body = json_decode($rawpost);
  $event_type = $body->event_type;
  // $now = new \DateTime();
  // $webhook->webhook = $runHandshake;
  $webhook->webhook = $body->id;
  $webhook->credential_id = 'credencial';
  $webhook->event = $body->event_type;
  $webhook->header =$headersJson;
  $webhook->body = $rawpost;
  $webhook->save();


}
public function personalListener(Request $request, $appid, $webhookid){

  //https://webhook-jj.000webhostapp.com/listener.php
  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('Location: ../../index.php');
    exit();
  }

  // $headersJson = (json_encode(apache_request_headers()));
  // $rawpost = file_get_contents('php://input');


  // file_put_contents('_apacheResquestHeadesrs.txt', gettype(apache_request_headers()));
  // file_put_contents('_apacheResquestHeadesrs.json', json_encode(apache_request_headers()));
  $headersJson = (json_encode(apache_request_headers())); // heroku test ok
  // file_put_contents('___apacheResquestHeadesrsVar.json', $headersJson);
  $headersArray = json_decode($headersJson, 1);
  // file_put_contents('___apacheResquestHeadesrsType.txt', gettype($headersArray));

  // file_put_contents('___apacheResquestHeadesrsConnection.txt', $headersArray['Connection']);

  $rawpost = file_get_contents('php://input'); // heroku test ok
  //   $request = "cmd=_notify-validate&" . $rawpost;

  // file_put_contents("_rawpost.json", $rawpost);
  // file_put_contents("_rawpostType.txt", gettype($rawpost));


  // file_put_contents("_webhookResponse.json", $rawpost);
  $webhookArray = json_decode($rawpost,1);
  // file_put_contents("_webhookArrayType.txt", gettype($webhookArray));
  // file_put_contents("_webhookArrayId.txt", $webhookArray['id']);



  // $raw_post_array = explode('&', $webhookArray);
  //  $myPost = array();
  //  foreach ($raw_post_array as $keyval) {
  //    $keyval = explode ('=', $keyval);
  //    if (count($keyval) == 2)
  //       $myPost[$keyval[0]] = urldecode($keyval[1]);
  //  }
  // file_put_contents("_webhookTest.json", json_encode($myPost));

  // $jsonCollectionArray = array();

  // if(file_exists('_webhookTestCollection.json')){
  //   $jsonCollection = file_get_contents("_webhookTestCollection.json");
  //   $jsonArrayCollectionFile = array();
  //   $jsonArrayCollectionNew = array();
  //   $jsonArrayCollectionFile = json_decode($jsonCollection,true);
  //   $jsonArrayCollectionNew['transaction'] = json_decode($rawpost);
  //   array_push( $jsonArrayCollectionFile, $jsonArrayCollectionNew );
  //   file_put_contents("_webhookTestCollection.json", json_encode($jsonArrayCollectionFile));
  // }else{
  //   $jsonCollectionArray[0]['transaction'] = json_decode($rawpost);
  //   file_put_contents("_webhookTestCollection.json", json_encode($jsonCollectionArray));
  // }



  // get token
  $clientId = "AWrMp7lycUDtsRSpJNWe_0Jm9vbo7ui316BUkrjwD_3Q6n8GU8kaPHPUNvh4QQITLfvjK67sL1GfUQLH";
  $secret = "EEpu0P1ta6IiDIT7OUPuJ3WT_f6leOuWZnyZUAYj9iyqBPLnz_AL1_Ic9y-GvZiTF8CkwntENIxgUseV";

  $url = "https://api.sandbox.paypal.com/v1/oauth2/token";

  $headers = array( "Accept"=>"application/json",
  "Accept-Language"=>"en_US",
  "Content-Type"=>"application/x-www-form-urlencoded",
);
$postfields = "grant_type=client_credentials";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_POST, true);

$run = curl_exec($ch);
curl_close($ch);

// with the array cast (array) returns an array as well
$runObj = json_decode($run, 1);
$accessToken = $runObj['access_token'];

















//Handshake


// $handshakeRequestArray = new stdClass();
// $handshakeRequestArray = new \stdClass();
// $handshakeRequestArray->auth_algo = '123';
// $handshakeRequestArray->cert_url = '456';
// $handshakeRequestArray->transmission_id = '789';
// $handshakeRequestArray->transmission_sig = 'abc';
// $handshakeRequestArray->transmission_time = 'def';
// $handshakeRequestArray->webhook_id = "4GF03916SJ763193G";
// $handshakeRequestArray->webhook_event  = 'lalala';
// $handshakePostfield = json_encode($handshakeRequestArray);

$handshakeRequestArray = new \stdClass();
$handshakeRequestArray->auth_algo = $headersArray['Paypal-Auth-Algo'];
$handshakeRequestArray->cert_url = $headersArray['Paypal-Cert-Url'];
$handshakeRequestArray->transmission_id = $headersArray['Paypal-Transmission-Id'];
$handshakeRequestArray->transmission_sig = $headersArray['Paypal-Transmission-Sig'];
$handshakeRequestArray->transmission_time = $headersArray['Paypal-Transmission-Time'];
// $handshakeRequestArray->webhook_id = "4EM20171AA011874E"; // https://paypal-galapagos.herokuapp.com/webhook/listener
$handshakeRequestArray->webhook_id = $webhookid; // https://paypal-galapagos.herokuapp.com/webhook/personal-listener/
$handshakeRequestArray->webhook_event  = json_decode($rawpost);
// $handshakeRequestArray->webhook_event  = $rawpost;
$handshakePostfield = json_encode($handshakeRequestArray);
// json_encode($str, JSON_UNESCAPED_SLASHES);


$urlHandshake = "https://api.sandbox.paypal.com/v1/notifications/verify-webhook-signature";

$paymentHeadersHandshake = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);
// $paymentHeadersHandshake = array("Content-Type: application/json", "Authorization: Bearer A21AAFufDLdguKDtESdG1YssQHhqfXuXzDpWb89WjRQWckH7sHATWZpm9f7L7R6zWX2Dmvr11gLn8nQ09TXD7k0p3PO6Q5b1g");


$chHandshake = curl_init();
curl_setopt($chHandshake, CURLOPT_URL, $urlHandshake);
curl_setopt($chHandshake, CURLOPT_HTTPHEADER, $paymentHeadersHandshake);
curl_setopt($chHandshake, CURLOPT_POSTFIELDS, $handshakePostfield);
curl_setopt($chHandshake, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($chHandshake, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($chHandshake, CURLOPT_RETURNTRANSFER, true);
curl_setopt($chHandshake, CURLOPT_VERBOSE, 1);
curl_setopt($chHandshake, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($chHandshake, CURLOPT_POST, true);
$runHandshake = curl_exec($chHandshake);
curl_close($chHandshake);


// save data
$webhook = new \App\Webhook();

$body = json_decode($rawpost);
$event_type = $body->event_type;
// $now = new \DateTime();
// $webhook->webhook = $runHandshake;
$webhook->webhook = $body->id;
$webhook->credential_id = $appid;
$webhook->event = $body->event_type;
$webhook->header =$headersJson;
$webhook->body = $rawpost;
$webhook->save();


}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function index()
{
  $webhooks = \App\Webhook::all();
  $webhooksCount = \App\Webhook::count();
  // return \Auth::user()->activeCredential->token->access_token;
  // $app_name = \Auth::user()->activeCredential->app_name;

  return view('webhook.index', compact('webhooks','webhooksCount'));
}
public function myWebhook()
{
  // return \Auth::user()->activeCredential;
  $webhooks = \Auth::user()->activeCredential->webhooks;
  $webhooksCount = \Auth::user()->activeCredential->webhooks->count();
  $credential = Auth()->user()->activeCredential;
  // return \Auth::user()->activeCredential->webhooks;

  // $webhooks = \App\Webhook::all();
  // $webhooksCount = \App\Webhook::count();
  // return \Auth::user()->activeCredential->token->access_token;
  // $app_name = \Auth::user()->activeCredential->id;
  // $webhooks = \Auth::user()->activeCredential->webhooks;
  // $userEmail = \Auth::user()->email;

  // $mywebhook = strtolower($userEmail.'-'.$app_name);

  return view('webhook.my-webhook', compact('webhooks','webhooksCount','credential'));
}
public function details($webhookId)
{
  $webhook = \App\Webhook::find($webhookId);


  return view('webhook.details', compact('webhook'));
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
  $accessToken = $this->token();

  $endpoint = "https://api.sandbox.paypal.com/v1/notifications/webhooks";

  $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, false);

    $run = curl_exec($ch);

    curl_close($ch);

    $webhooks = json_decode($run,1);

    $app_name = \Auth::user()->activeCredential->app_name;
    // dd($webhooks);
  return view('webhook.create', compact('webhooks','app_name'));
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{

  // return $appId;

  $eventObj = $request->eventObj;
  file_put_contents('log/______eventObj.txt',json_encode($eventObj));
  $accessToken = $this->token();

  $endpoint = "https://api.sandbox.paypal.com/v1/notifications/webhooks/";
  $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

  $postfields = '{
    "url": "https://paypal-galapagos.herokuapp.com/webhook/personal-listener/test",
    "event_types": '.json_encode($eventObj).'
  }';
  // $postfields = '{
  //   "url": "https://paypal-galapagos.herokuapp.com/webhook/listener/test/987",
  //   "event_types": [
  //     {
  //       "name": "PAYMENT.AUTHORIZATION.CREATED"
  //     },
  //     {
  //       "name": "PAYMENT.AUTHORIZATION.VOIDED"
  //     }
  //   ]
  // }';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $endpoint);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_POST, true);
  $run = curl_exec($ch);
  curl_close($ch);

  return $run;
}

/**
* Display the specified resource.
*
* @param  \App\Webhook  $webhook
* @return \Illuminate\Http\Response
*/
public function show(Webhook $webhook)
{
  //
}

/**
* Show the form for editing the specified resource.
*
* @param  \App\Webhook  $webhook
* @return \Illuminate\Http\Response
*/
public function edit(Webhook $webhook)
{
  //
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Webhook  $webhook
* @return \Illuminate\Http\Response
*/
public function update(Request $request)
{
  $accessToken = $this->token();
  $appId = json_encode(\Auth::user()->activeCredential->id);
  $webhookId = $request->webhook;

  // $webhookId = json_decode($run);
  // $webhookId = $webhookId->id;
  // return json_encode($webhookId);

  $endpoint = "https://api.sandbox.paypal.com/v1/notifications/webhooks/".$webhookId."/";
  file_put_contents('log/_____webhookId.txt',$endpoint);
  $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer ".$accessToken);

  // $postfields = '{
  //   "url": "https://paypal-galapagos.herokuapp.com/webhook/listener/test/'.$webhookId.'",
  //   "event_types": [
  //     {
  //       "name": "PAYMENT.AUTHORIZATION.CREATED"
  //     },
  //     {
  //       "name": "PAYMENT.AUTHORIZATION.VOIDED"
  //     }
  //   ]
  // }';
  $postfields = '[
  {
    "op": "replace",
    "path": "/url",
    "value": "https://paypal-galapagos.herokuapp.com/webhook/personal-listener/'.$appId."/".$webhookId.'"
  }
  ]';
  // $postfields = '[
  // {
  //   "op": "replace",
  //   "path": "/url",
  //   "value": "https://paypal-galapagos.herokuapp.com/webhook/listener/test/'.$webhookId.'"
  // }
  // ]';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $endpoint);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
  $run = curl_exec($ch);
  curl_close($ch);

  return $run;



}

/**
* Remove the specified resource from storage.
*
* @param  \App\Webhook  $webhook
* @return \Illuminate\Http\Response
*/
public function destroy(Webhook $webhook)
{
  //
}
}
