<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Braintree_Gateway;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  // public function __construct()
  // {
  //   $this->middleware('auth');
  // }
  public function __construct()
  {
    $this->middleware('credential.check',['except'=>[
      'checkToken','clientToken','createCostumer','transaction'
      ]]);
      $this->middleware('access_token.check',['except'=>[
        'checkToken','clientToken','createCostumer','transaction'
        ]]);
        $this->middleware('auth',['except'=>[
          'checkToken','clientToken','createCostumer','transaction'
          ]]);

        }

        //http://localhost:8000/api/token/2019-11-21T20:42:18ZLFKFclGAHXBrxxL48FJma6RQgWjsWA_aPXQm4M_-XVg
        public function checkToken($token){
          $createTimeNonce = substr($token,0,19);
          $createTimeCarbon = Carbon::create($createTimeNonce);
          $createTime = Carbon::create($createTimeNonce);
          $expireTime = $createTimeCarbon->addSeconds(32400);

          $current = Carbon::now();

          $hasExpired = ($current->diffInSeconds($createTimeNonce)>32400)?true:false;
          $expiredTime = $current->diff($expireTime);

          $expiredTime = $current->diffInDays($expireTime);

          $diffInsSeconds = $current->diffInSeconds($expireTime);


          $response = new \stdClass();

          $response->create_time = $createTimeNonce;
          $response->expire_time = substr($expireTime,0,19);
          $response->request_time = substr($current,0,19);
          $response->has_expired = $hasExpired;


          if($current->diffInSeconds($createTimeNonce)>32400){
            $response->expired_time = gmdate('H:i:s', $diffInsSeconds);
          }else{
            $response->life_time = gmdate('H:i:s', $diffInsSeconds);
          }



          // $response = json_decode($response,1);

          // return json_encode($response);
          return response()->json($response);

        }

        /**
        * Show the application dashboard.
        *
        * @return \Illuminate\Contracts\Support\Renderable
        */
        public function index()
        {
          return view('home');
        }
        public function profile()
        {

          $credential = Auth()->user()->activeCredential === null? false :  Auth()->user()->activeCredential;

          $planId = Auth()->user()->activeCredential->plan === null? false :  Auth()->user()->activeCredential->plan->plan;
          // dd($planId);
          $plan = ($planId) ? json_decode($this->Details($planId,'plan')) : false;
          // dd($plan);

          $baTokenId = Auth()->user()->activeCredential->batoken === null? false :  Auth()->user()->activeCredential->batoken->batoken;
          $baToken = ($baTokenId) ? json_decode($this->Details($baTokenId,'batoken')) : false;



          return view('profile',compact('credential','plan','baToken'));
        }
        public function token(){
          return \Auth::user()->activeCredential->token->access_token;
        }

        public function Details($search,$type){


          switch ($type) {
            case 'plan':
            // return $type;
            $endpoint = "https://api.sandbox.paypal.com/v1/payments/billing-plans/".$search."";
            break;
            case 'batoken':
            // dd($search);
            $endpoint = "https://api.sandbox.paypal.com/v1/billing-agreements/agreements/".$search;
            // dd($endpoint);
            break;

            default:
            // code...
            break;
          }


          $accessToken = $this->token();
          // return '$accessToken';
          $paymentHeaders = array("Accept: application/json","Accept-Language: en_US","Content-Type: application/json", "Authorization: Bearer ".$accessToken);

          $ch = curl_init();

          curl_setopt($ch, CURLOPT_URL, $endpoint);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_VERBOSE, 1);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_POST, false);
          $run = curl_exec($ch);
          curl_close($ch);
          // dd($run);
          return $run;
        }
        public function parameters(){
          return view('parameters.parameters');
        }
        public function errors(){
          return view('errors.errors');
        }
        public function statusCode(){
          return view('status.status');
        }
        public function coding(){
          return view('misc.coding');
        }

        public function clientToken(){
          // https://developers.braintreepayments.com/start/hello-server/php
          $gateway = new Braintree_Gateway([
            'environment' => 'sandbox',
            'merchantId' => 'vn22qp9k6sgcys5s',
            'publicKey' => 'stk8rk4qp8sf855c',
            'privateKey' => 'd079e4485744c649b62ff46fad3e0701'
          ]);

          echo($clientToken = $gateway->clientToken()->generate());


          // return "cliente token";
        }

        public function createCostumer($id){
          // https://developers.braintreepayments.com/reference/request/customer/create/php
          $gateway = new Braintree_Gateway([
            'environment' => 'sandbox',
            'merchantId' => 'vn22qp9k6sgcys5s',
            'publicKey' => 'stk8rk4qp8sf855c',
            'privateKey' => 'd079e4485744c649b62ff46fad3e0701'
          ]);

          file_put_contents('log/_braintreeCreateCustomerId.txt',$id);
          $nonceFromTheClient = $id;

          $result = $gateway->customer()->create([
            'firstName' => 'Mike',
            'lastName' => 'Jones',
            'company' => 'Jones Co.',
            'paymentMethodNonce' => $nonceFromTheClient
          ]);
          if ($result->success) {
            echo($result->customer->id);
            echo($result->customer->paymentMethods[0]->token); // this on is used to transaction function (letters only)
          } else {
            foreach($result->errors->deepAll() AS $error) {
              echo($error->code . ": " . $error->message . "\n");
            }
          }

        }

        public function transaction($id){
          $gateway = new Braintree_Gateway([
            'environment' => 'sandbox',
            'merchantId' => 'vn22qp9k6sgcys5s',
            'publicKey' => 'stk8rk4qp8sf855c',
            'privateKey' => 'd079e4485744c649b62ff46fad3e0701'
          ]);

          // https://developers.braintreepayments.com/reference/request/transaction/sale/php#storing-in-vault
          $paymentMethodToken =  $id;
          $result = $gateway->transaction()->sale(
            [
              // 'paymentMethodToken' => 'the_payment_method_token',
              'paymentMethodToken' => $paymentMethodToken,
              'amount' => '100.00'

            ]
          );



          if ($result->success) {
            // print_r("Success ID: " . $result->transaction->id);
            // echo json_encode($result->transaction->id); // payment
            echo $result->transaction->id; // payment

          } else {
            // print_r("Error Message: " . $result->message);
            // echo json_encode($result->message);
            echo $result->message;
          }


        }







      }
