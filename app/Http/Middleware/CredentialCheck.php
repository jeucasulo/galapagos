<?php

namespace App\Http\Middleware;

use Closure;

class CredentialCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        // dd($user->activeCredential->count());

        // if ($request->age <= 200) {
        if ($user->activesCredential->count()!=1) {
            return redirect('credential/index');
        }
        return $next($request);
    }
}
