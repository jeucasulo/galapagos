<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class AccessTokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if(\Auth::user()->activeCredential->token){
          $accessToken = \Auth::user()->activeCredential->token->access_token;

          $nonce = \Auth::user()->activeCredential->token->nonce;
          $createTimeNonce = substr($nonce,0,19);
          $createTimeCarbon = Carbon::create($createTimeNonce);
          $createTime = Carbon::create($createTimeNonce);
          $expireTime = $createTimeCarbon->addSeconds(32400);

          $current = Carbon::now();

          $hasExpired = ($current->diffInSeconds($createTimeNonce)>32400)?'danger':'primary';
          $expiredTime = $current->diff($expireTime);

          $expiredTime = $current->diffAsCarbonInterval($expireTime);

          if($current->diffInSeconds($createTimeNonce)>32400){


            \Session::flash('expiredTime', $expiredTime);
            \Session::flash('hasExpired', $hasExpired);

            return redirect('token/index');

          }




          // $accessToken = \Auth::user()->activeCredential->token->access_token;
        }else{
          // $accessToken = 'No token found for this app...';
          return redirect('token/index');

        }

        \Session::flash('expiredTime', $expiredTime);
        \Session::flash('hasExpired', $hasExpired);

        return $next($request);
    }
}
