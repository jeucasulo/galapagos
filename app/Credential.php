<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{

    protected $fillable = [
      'status'
    ];

    public function tokens(){
        return $this->hasMany('App\Token','credential_id','id');
    }
    public function token(){
        return $this->hasOne('App\Token','credential_id','id');
    }
    public function plan(){
        return $this->hasOne('App\Plan','credential_id','id');
    }
    public function batoken(){
        return $this->hasOne('App\Batoken','credential_id','id');
    }
    public function webhooks(){
        return $this->hasMany('App\Webhook','credential_id','id');
    }


}
