<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable = [
      'scope','access_token','token_type','expires_in','nonce','app_id','token_owner','app_name','app_desc','credential_id'
    ];


}
