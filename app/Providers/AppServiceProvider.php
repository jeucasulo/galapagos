<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// import to  View share
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

      // $user = \Auth::user();
      // $accessToken = \Auth::user()->activeCredential->activeToken;
      // $accessToken = Auth::user()->activeCredential->token->access_token;
      // $accessToken = \Auth::user()->id;
      // $accessToken = 'asdf';
      // View::share('accessToken', $accessToken);

    }
}
