<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function credentials(){
        return $this->hasMany('App\Credential','app_owner','id');
    }
    public function activeCredential(){
        return $this->hasOne('App\Credential','app_owner','id')->where('status','enabled');
    }
    public function activesCredential(){
        return $this->hasMany('App\Credential','app_owner','id')->where('status','enabled');
    }
    public function activeToken(){
        return $this->hasOne('App\Token','token_owner','id')->where('status','enabled');
    }

    // public function tokens(){
    //     return $this->hasMany('App\Token','token_owner','id');
    // }

    // public function activatedCredential(){
    //     return $this->hasMany('App\Credential','app_owner','id')->where('status','enabled');
    // }

}
