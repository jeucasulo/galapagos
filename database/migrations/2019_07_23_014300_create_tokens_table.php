<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('scope');
            $table->string('access_token');
            $table->string('token_type');
            $table->string('app_id');
            $table->string('expires_in');
            $table->string('nonce');
            $table->string('app_name');
            $table->string('app_desc');
            $table->string('token_owner');
            $table->string('status')->default('enabled');
            $table->string('credential_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokens');
    }
}
