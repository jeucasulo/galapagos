<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/profile',['uses'=>'HomeController@profile','as'=>'profile']);
Route::get('/attravel',function(){
  return view('_attravel.index');
});


Route::post('/webhook/listener',['uses'=>'WebhookController@listener','as'=>'webhook.listener']);
Route::post('/webhook/personal-listener/{appid}/{webhookid}',['uses'=>'WebhookController@personalListener','as'=>'webhook.personal.listener']);
Route::get('/webhook/index',['uses'=>'WebhookController@index','as'=>'webhook.index']);
Route::get('/my-webhook/index',['uses'=>'WebhookController@myWebhook','as'=>'my.webhook.index']);
Route::get('/webhook/details/{id}',['uses'=>'WebhookController@details','as'=>'webhook.details']);
//Route::get('/webhook/listener',['uses'=>'WebhookTestController@listener','as'=>'webhook.test.details']);
Route::get('/webhook/create',['uses'=>'WebhookController@create','as'=>'webhook.create']);
Route::post('/webhook/store',['uses'=>'WebhookController@store','store'=>'webhook.store']);
Route::put('/webhook/update',['uses'=>'WebhookController@update','store'=>'webhook.update']);

Route::get('/array2',function(){

$json = '{"line_1_col_1":{"line_2_col_1":{"line_3_col1":{"line_4_col_1":"line_4_col_2"}}}, "tab_2_title":"tab2_sub","tab3_title":"tab3_sub"}';
$json = '{"id":"P-5W179289WU715691TBEEN7NI","state":"ACTIVE","name":"Plan with Regular and Trial Payment Definitions","description":"Plan with regular and trial payment definitions.","type":"FIXED","payment_definitions":[{"id":"PD-6R486009SS742331PBEEN7NQ","name":"Trial payment definition","type":"TRIAL","frequency":"Week","amount":{"currency":"BRL","value":"9.19"},"cycles":"2","charge_models":[{"id":"CHM-1EH02738XK665372PBEEN7NQ","type":"TAX","amount":{"currency":"BRL","value":"2"}},{"id":"CHM-3X316328TX6666139BEEN7NQ","type":"SHIPPING","amount":{"currency":"BRL","value":"1"}}],"frequency_interval":"5"},{"id":"PD-56L76502LT738833YBEEN7NI","name":"Regular payment definition","type":"REGULAR","frequency":"Month","amount":{"currency":"BRL","value":"100"},"cycles":"12","charge_models":[{"id":"CHM-1CN47846A2716262TBEEN7NQ","type":"TAX","amount":{"currency":"BRL","value":"12"}},{"id":"CHM-2G076411J7861841SBEEN7NI","type":"SHIPPING","amount":{"currency":"BRL","value":"10"}}],"frequency_interval":"2"}],"merchant_preferences":{"setup_fee":{"currency":"BRL","value":"1"},"max_fail_attempts":"0","return_url":"https://29428e7dd9774defbbde15e87e849c88.vfs.cloud9.us-east-2.amazonaws.com/?r=return","cancel_url":"https://29428e7dd9774defbbde15e87e849c88.vfs.cloud9.us-east-2.amazonaws.com/?r=cancel","auto_bill_amount":"YES","initial_fail_amount_action":"CONTINUE"},"create_time":"2019-09-07T00:03:59.541Z","update_time":"2019-09-07T00:04:42.631Z","links":[{"href":"https://api.sandbox.paypal.com/v1/payments/billing-plans/P-5W179289WU715691TBEEN7NI","rel":"self","method":"GET"}]}';
$array = json_decode($json,1);
// $array = array('teste' => 'testando' );

$output = recursive($array);
echo $output;
});
// $output = "";
function recursive($array, $level = 1){
    if(!isset($output)){
        $output = "";
    }
    $output .= "<ul>";
    foreach($array as $key => $value){
        //If $value is an array.
        if(is_array($value)){
          $output .= "<li><b>" .$key . "</b> : <i>";
          $output .= recursive($value, $level + 1);
          $output .= '</i></li>';
        } else{
            $output .= "<li><b>".$key . "</b> : <i>" . $value . '</i></li>';
        }
    }
    $output .= "</ul>";
    return $output;
}
Route::get('/array',function(){
  $json = '{"tab1_title":{"tab1_sub":"tab1_sub2"}, "tab2_title":"tab2_sub","tab3_title":"tab3_sub"}';
  //$json = '{"tab1_title":"tab1_sub", "tab2_title":"tab2_sub","tab3_title":"tab3_sub"}';
  $json = '{"line_1_col_1":{"line_2_col_1":{"line_3_col1":{"line_4_col_1":"line_4_col_2"}}}, "tab_2_title":"tab2_sub","tab3_title":"tab3_sub"}';
  /*
  $json = '{"id":"P-5W179289WU715691TBEEN7NI","state":"ACTIVE","name":"Plan with Regular and Trial Payment Definitions","description":"Plan with regular and trial payment definitions.","type":"FIXED","payment_definitions":[{"id":"PD-6R486009SS742331PBEEN7NQ","name":"Trial payment definition","type":"TRIAL","frequency":"Week","amount":{"currency":"BRL","value":"9.19"},"cycles":"2","charge_models":[{"id":"CHM-1EH02738XK665372PBEEN7NQ","type":"TAX","amount":{"currency":"BRL","value":"2"}},{"id":"CHM-3X316328TX6666139BEEN7NQ","type":"SHIPPING","amount":{"currency":"BRL","value":"1"}}],"frequency_interval":"5"},{"id":"PD-56L76502LT738833YBEEN7NI","name":"Regular payment definition","type":"REGULAR","frequency":"Month","amount":{"currency":"BRL","value":"100"},"cycles":"12","charge_models":[{"id":"CHM-1CN47846A2716262TBEEN7NQ","type":"TAX","amount":{"currency":"BRL","value":"12"}},{"id":"CHM-2G076411J7861841SBEEN7NI","type":"SHIPPING","amount":{"currency":"BRL","value":"10"}}],"frequency_interval":"2"}],"merchant_preferences":{"setup_fee":{"currency":"BRL","value":"1"},"max_fail_attempts":"0","return_url":"https://29428e7dd9774defbbde15e87e849c88.vfs.cloud9.us-east-2.amazonaws.com/?r=return","cancel_url":"https://29428e7dd9774defbbde15e87e849c88.vfs.cloud9.us-east-2.amazonaws.com/?r=cancel","auto_bill_amount":"YES","initial_fail_amount_action":"CONTINUE"},"create_time":"2019-09-07T00:03:59.541Z","update_time":"2019-09-07T00:04:42.631Z","links":[{"href":"https://api.sandbox.paypal.com/v1/payments/billing-plans/P-5W179289WU715691TBEEN7NI","rel":"self","method":"GET"}]}';
  */
  $array =  json_decode($json,1);
  $output = "<ul>";
  $res = jsonToHtml(0,$output,$array);
  echo "<br>";
  echo "<br>";
  echo "json - " . $json;
  echo "<br>";
  echo "<br>";
  print_r($array);

  echo $res;
});
function jsonToHtml($iterator,$output,$array){
  foreach ($array as $key => $value) {
    $output .= "<li><b>".$key."</b>";
    if (is_array($value)){
      $iterator ++;
      echo "<br>looping (is array): " . $iterator;
      $output .= "<ul><li><b>".$key."</b><i> : ".jsonToHtml($iterator,$output,$value)." </i></li></ul>";
    }else{
      $iterator ++;
      echo "<br>looping (not array): " . $iterator;
      $output .= " : <i>".$value." </i>";
      $output .= "</li>";
      // return;
    }
    // break;
  }
  $output .= "</ul>";
  return $output;
}



Route::get('/token/test/test', function(){
  // get token
  // get token
  $clientId = "AWrMp7lycUDtsRSpJNWe_0Jm9vbo7ui316BUkrjwD_3Q6n8GU8kaPHPUNvh4QQITLfvjK67sL1GfUQLH";
  $secret = "EEpu0P1ta6IiDIT7OUPuJ3WT_f6leOuWZnyZUAYj9iyqBPLnz_AL1_Ic9y-GvZiTF8CkwntENIxgUseV";

  $url = "https://api.sandbox.paypal.com/v1/oauth2/token";

  $headers = array( "Accept"=>"application/json",
  "Accept-Language"=>"en_US",
  "Content-Type"=>"application/x-www-form-urlencoded",
);
$postfields = "grant_type=client_credentials";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_POST, true);

$run = curl_exec($ch);
curl_close($ch);

// with the array cast (array) returns an array as well
$runObj = json_decode($run, 1);
$accessToken = $runObj['access_token'];

echo $accessToken;

});

Route::get('/webhook/test', function(){
  $paymentPayoutsbatchSuccess = '{"id":"WH-5L2494116M979831S-12K72685PU115893G","event_version":"1.0","create_time":"2019-09-04T03:56:35.623Z","resource_type":"payouts","event_type":"PAYMENT.PAYOUTSBATCH.SUCCESS","summary":"Payouts batch completed successfully.","resource":{"batch_header":{"payout_batch_id":"N2EV3S3QGZF8L","batch_status":"SUCCESS","time_created":"2019-09-04T03:56:19Z","time_completed":"2019-09-04T03:56:23Z","sender_batch_header":{"sender_batch_id":"Payouts_2019_2019-09-04 03:56:18"},"amount":{"currency":"BRL","value":"677.30"},"fees":{"currency":"BRL","value":"13.44"},"payments":3},"links":[{"href":"https://api.sandbox.paypal.com/v1/payments/payouts/N2EV3S3QGZF8L","rel":"self","method":"GET","encType":"application/json"}]},"links":[{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-5L2494116M979831S-12K72685PU115893G","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-5L2494116M979831S-12K72685PU115893G/resend","rel":"resend","method":"POST"}]}';


    $searchArray = json_decode($paymentPayoutsbatchSuccess);
    $searchId = $searchArray->resource->batch_header->payout_batch_id;
    // return $searchId;
    $endpoint = "https://api.sandbox.paypal.com/v1/payments/payouts/".$searchId;

    $result = getDetails($endpoint);
    return $result;
  });

  function getDetails($endpoint){
    $paymentHeaders = array("Content-Type: application/json", "Authorization: Bearer A21AAFYSSiWh_5uXR5kBySfCypaNUKaDc82eJUXILdPeIZrQzTstrqvAiCj2fKAW4Wv4np1RFurA6rC22cJBarQ81_yApHGgA");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $paymentHeaders);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, false);
    $run = curl_exec($ch);
    curl_close($ch);
    return $run;
  }



  Route::get('/token/test', ['uses'=>'EcRestServerController@token','as'=>'token.test']);


  Route::get('/', function () {
    return view('index');
  })->middleware('auth');

  Route::get('/midhaz', function () {
    return view('midhaz');
  });




  Auth::routes();

  Route::get('/home', 'HomeController@index')->name('home');


  Route::get('/credential/index', ['uses'=>'CredentialController@index','as'=>'credential.index']);
  Route::get('/credential/create', ['uses'=>'CredentialController@create','as'=>'credential.create']);
  Route::post('/credential/store', ['uses'=>'CredentialController@store','as'=>'credential.store']);
  Route::get('/credential/edit/{id}', ['uses'=>'CredentialController@edit','as'=>'credential.edit']);
  Route::put('/credential/update/{id}', ['uses'=>'CredentialController@update','as'=>'credential.update']);

  Route::get('/token/index', ['uses'=>'TokenController@index','as'=>'token.index']);
  Route::get('/token/create', ['uses'=>'TokenController@create','as'=>'token.create']);
  Route::post('/token/store', ['uses'=>'TokenController@store','as'=>'token.store']);

  Route::get('/ec/client/clientId', ['uses'=>'EcRestClientController@clientId','as'=>'ec.client.clientId']);

  // EC REST CLIENT
  Route::get('/ec/client/index', ['uses'=>'EcRestClientController@index','as'=>'ec-rest-client.index']);

  // EC REST CLIENT V2
  Route::get('/ec/client/v2/index', ['uses'=>'EcRestClientV2Controller@index','as'=>'ec-rest-client-v2.index']);

  // EC REST SERVER
  Route::get('/ec/server/index', ['uses'=>'EcRestServerController@index','as'=>'ec-rest-server.index']);
  Route::post('/ec/server/payment/create', ['uses'=>'EcRestServerController@createPayment','as'=>'ec.server.payment.create']);
  Route::post('/ec/server/payment/execute', ['uses'=>'EcRestServerController@executePayment','as'=>'ec.server.payment.execute']);

  // EC REST SERVER AUTH CAP
  Route::get('/ec/server/authcap/index', ['uses'=>'EcRestServerAuthCapController@index','as'=>'ec.server.authcap.index']);
  Route::post('/ec/server/authcap/create', ['uses'=>'EcRestServerAuthCapController@createAuth','as'=>'ec.server.authcap.create']);
  Route::post('/ec/server/authcap/auth', ['uses'=>'EcRestServerAuthCapController@confirmAuth','as'=>'ec.server.authcap.auth']);
  Route::post('/ec/server/authcap/execute', ['uses'=>'EcRestServerAuthCapController@executeCap','as'=>'ec.server.authcap.execute']);
  // Route::get('/ec/authcap', ['uses'=>'EcRestController@authcap','as'=>'ec.authcap']);
  // Route::post('/ec/server/create/auth', ['uses'=>'EcRestController@createAuth','as'=>'ec.server.create.auth']);
  // Route::post('/ec/server/confirm/auth', ['uses'=>'EcRestController@confirmAuth','as'=>'ec.server.confirm.auth']);
  // Route::post('/ec/server/authcap/execute', ['uses'=>'EcRestController@executeCap','as'=>'ec.server.authcap.execute']);

  // EC REST SERVER V2
  Route::get('/ec/server/v2/index', ['uses'=>'EcRestServerV2Controller@index','as'=>'ec.server.v2.index']);
  Route::post('/ec/server/v2/order/create', ['uses'=>'EcRestServerV2Controller@createOrder','as'=>'ec.server.v2.order.create']);
  Route::post('/ec/server/v2/order/capture', ['uses'=>'EcRestServerV2Controller@captureOrder','as'=>'ec.server.v2.order.capture']);
  // Route::get('/ec/serverV2', ['uses'=>'EcRestController@serverV2','as'=>'ec.serverV2']);
  // Route::post('/ec/serverV2/create/order', ['uses'=>'EcRestController@createOrder','as'=>'ec.server.create.order']);
  // Route::post('/ec/serverV2/capture/order', ['uses'=>'EcRestController@captureOrder','as'=>'ec.server.capture.order']);

  Route::get('/pplus/index', ['uses'=>'PPlusController@index','as'=>'pplus.index']);
  Route::post('/pplus/payment/create', ['uses'=>'PPlusController@createPayment','as'=>'pplus.payment.create']);
  Route::post('/pplus/payment/execute', ['uses'=>'PPlusController@executePayment','as'=>'pplus.payment.execute']);

  Route::get('/pplus/ctb/index', ['uses'=>'PPlusController@indexCTB','as'=>'pplus.ctb.index']);
  Route::post('/pplus/ctb/payment/create', ['uses'=>'PPlusController@createPaymentCTB','as'=>'pplus.payment.ctb.create']);
  Route::post('/pplus/ctb/payment/execute', ['uses'=>'PPlusController@executePaymentCTB','as'=>'pplus.payment.ctb.execute']);

  // Sub
  Route::get('/sub/index', ['uses'=>'SubController@index','as'=>'sub.index']);
  // Route::post('/sub/ba/token', ['uses'=>'SubController@rtBaToken','as'=>'sub.ba.token']);
  Route::post('/sub/plan/create', ['uses'=>'SubController@subPlanCreate','as'=>'sub.plan.create']);
  Route::post('/sub/plan/active', ['uses'=>'SubController@subPlanActive','as'=>'sub.plan.active']);
  Route::post('/sub/plan/remove', ['uses'=>'SubController@subPlanRemove','as'=>'sub.plan.remove']);
  Route::post('/sub/ba', ['uses'=>'SubController@subBa','as'=>'sub.ba']);
  Route::post('/sub/ba/execute', ['uses'=>'SubController@subBaExecute','as'=>'sub.ba.execute']);


  // RT
  Route::get('/rt/index', ['uses'=>'RtController@index','as'=>'rt.index']);
  Route::post('/rt/ba/token', ['uses'=>'RtController@rtBaToken','as'=>'rt.ba.token']);
  Route::post('/rt/ba/create', ['uses'=>'RtController@rtBaCreate','as'=>'rt.ba.create']);
  Route::post('/rt/ba/delete', ['uses'=>'RtController@rtBaRemove','as'=>'rt.ba.remove']);
  Route::post('/rt/payment', ['uses'=>'RtController@rtPayment','as'=>'rt.payment']);

  // RT INSTALLS
  Route::get('/rt/installs/index', ['uses'=>'RtInstallsController@index','as'=>'rt.installs.index']);
  Route::post('/rt/installs/ba/token', ['uses'=>'RtInstallsController@rtInstallsBaToken','as'=>'rt.installs.ba.token']);
  Route::post('/rt/installs/ba/create', ['uses'=>'RtInstallsController@rtInstallsBaCreate','as'=>'rt.installs.ba.create']);
  Route::post('/rt/installs/ba/delete', ['uses'=>'RtInstallsController@rtInstallsBaRemove','as'=>'rt.installs.ba.remove']);
  Route::post('/rt/installs/payment', ['uses'=>'RtInstallsController@rtInstallsPayment','as'=>'rt.installs.payment']);

  // RT CTB
  Route::get('/rt/ctb/index', ['uses'=>'RtCtbController@index','as'=>'rt.ctb.index']);
  Route::post('/rt/ctb/ba/token', ['uses'=>'RtCtbController@rtBaToken','as'=>'rt.ctb.ba.token']);
  Route::post('/rt/ctb/cfo', ['uses'=>'RtCtbController@rtCtbCfo','as'=>'rt.ctb.cfo']);
  Route::post('/rt/ctb/ba/create', ['uses'=>'RtCtbController@rtCtbCreate','as'=>'rt.ctb.ba.create']);
  Route::post('/rt/ctb/ba/delete', ['uses'=>'RtCtbController@rtCtbBaRemove','as'=>'rt.ctb.ba.remove']);
  Route::post('/rt/ctb/payment', ['uses'=>'RtCtbController@rtCtbPayment','as'=>'rt.ctb.payment']);

  // RT DTB
  Route::get('/rt/dtb/index', ['uses'=>'RtDtbController@index','as'=>'rt.dtb.index']);
  Route::post('/rt/dtb/ba/token', ['uses'=>'RtDtbController@rtBaToken','as'=>'rt.dtb.ba.token']);
  Route::post('/rt/dtb/cfo', ['uses'=>'RtDtbController@rtdtbCfo','as'=>'rt.dtb.cfo']);
  Route::post('/rt/dtb/ba/create', ['uses'=>'RtDtbController@rtdtbCreate','as'=>'rt.dtb.ba.create']);
  Route::post('/rt/dtb/ba/delete', ['uses'=>'RtDtbController@rtdtbBaRemove','as'=>'rt.dtb.ba.remove']);
  Route::post('/rt/dtb/payment', ['uses'=>'RtDtbController@rtdtbPayment','as'=>'rt.dtb.payment']);

  // RT CTB NO CFO
  Route::get('/rt/ctb/no/cfo/index', ['uses'=>'RtCtbNoCfoController@index','as'=>'rt.ctb.no.cfo.index']);
  Route::post('/rt/ctb/no/cfo/ba/token', ['uses'=>'RtCtbNoCfoController@rtBaToken','as'=>'rt.ctb.no.cfo.ba.token']);
  Route::post('/rt/ctb/no/cfo/cfo', ['uses'=>'RtCtbNoCfoController@rtCtbCfo','as'=>'rt.ctb.no.cfo.cfo']);
  Route::post('/rt/ctb/no/cfo/ba/create', ['uses'=>'RtCtbNoCfoController@rtCtbCreate','as'=>'rt.ctb.no.cfo.ba.create']);
  Route::post('/rt/ctb/no/cfo/ba/delete', ['uses'=>'RtCtbNoCfoController@rtCtbBaRemove','as'=>'rt.ctb.no.cfo.ba.remove']);
  Route::post('/rt/ctb/no/cfo/payment', ['uses'=>'RtCtbNoCfoController@rtCtbPayment','as'=>'rt.ctb.no.cfo.payment']);

  // Payouts
  Route::get('/payouts/index', ['uses'=>'PayoutsController@index','as'=>'payouts.index']);
  Route::post('/payouts/execute', ['uses'=>'PayoutsController@payoutsExecute','as'=>'payouts.execute']);

  // Invoicing
  Route::get('/invoicing/index', ['uses'=>'InvoicingController@index','as'=>'invoicing.index']);
  Route::post('/invoicing/create', ['uses'=>'InvoicingController@invoicingCreate','as'=>'invoicing.create']);
  Route::post('/invoicing/execute', ['uses'=>'InvoicingController@invoicingExecute','as'=>'invoicing.execute']);

  // Ticket
  Route::get('/ticket/index', function(){
    return view('ticket.index');
  });



  Route::get('/search/{id}', ['uses'=>'SearchController@search','as'=>'search.id']);
  Route::get('/home/parameters', ['uses'=>'HomeController@parameters','as'=>'home.parameters']);
  Route::get('/home/error-messages', ['uses'=>'HomeController@errors','as'=>'home.error-messages']);
  Route::get('/home/status-code', ['uses'=>'HomeController@statusCode','as'=>'home.status-code']);

  // Route::get('/rt/ctb/index', ['uses'=>'RtCtbController@index','as'=>'rt-ctb.index']);
  // Route::post('/rt/ctb/ba/token', ['uses'=>'RtCtbController@rtBaToken','as'=>'rt-ctb.ba.token']);
  // Route::post('/rt/ctb/ba/create', ['uses'=>'RtCtbController@rtBaCreate','as'=>'rt-ctb.ba.create']);


  Route::get('/api/token/{token}',['uses'=>'HomeController@checkToken','as'=>'app.check-token']);
  Route::get('/api/braintree/client-token',['uses'=>'HomeController@clientToken','as'=>'api.client-token']);
  Route::get('/api/braintree/create-costumer/{id}',['uses'=>'HomeController@createCostumer','as'=>'api.create-customer']);
  Route::get('/api/braintree/transaction/{id}',['uses'=>'HomeController@transaction','as'=>'api.transaction']);


  Route::get('/coding/index',['uses'=>'HomeController@coding','as'=>'coding.index']);
