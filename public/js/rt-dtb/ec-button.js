$(document).ready(function(){
  RtCtb.RenderPayPalButton();
  RtCtb.HasToken();
  $("#inactiveButton").click(RtCtb.RemoveBatoken);
  $("#updateFinalValue").click(RtCtb.CalculatedFinancingOptions);
  $("#createPayment").click(RtCtb.Payment);
});


var RtCtb = {
  RenderPayPalButton:()=>{
    // creates a paypal button object
    paypal.Button.render({
      env: 'sandbox', // Or 'production'
      // Set up the payment:
      // 1. Add a payment callback
      payment: function(data, actions) {
        // 2. Make a request to your server

        var CREATE_URL = "/rt/ba/token";

        // Excute a post to the endpoint and gets the res.id from the return from the CreatePayment method
        return actions.request.post(CREATE_URL,{_token: $('meta[name="csrf-token"]').attr('content')})

        // Return actions.request.post('/my-api/create-payment/')
        .then(function(res) {
          // 3. Return res.id from the response
          console.log("App.BillingAgreementToken.Done");
          console.log(res); // this

          let createPayment = "";

          function loopingObj(object) {

            for (key in object) {
              var value = object[key];
              if (typeof value === 'object') {
                loopingObj(value);
              } else {
                createPayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";
              }
            }

            return createPayment;
          }

          let newoutput = JSON.stringify(res, null, '\t')
          let textArea = document.getElementById("BaTokenJsonResponseOutputTextArea");
          textArea.value = newoutput;
          // getting the line numbers from the string
          let lines = newoutput.split("\n");
          textArea.rows = (lines.length+10);
          document.getElementById("baTokenWaitingNotification").innerHTML = "";
          document.getElementById("baTokentNotification").innerHTML = "Done";
          var d = new Date(Date.now());
          let baToken = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
          document.getElementById("baTokenTimeDiv").innerHTML = baToken;

          return res.token_id; //this

        });
      },

      // Execute the payment:
      // 1. Add an onAuthorize callback
      onAuthorize: function(data, actions) {
        console.log("App.Authorizes.Done");
        console.log(data);
        // 2. Make a request to your server
        //
        var localhost = window.location.hostname;

        var EXECUTE_URL = "/rt/ba/create";

        // excute a post to the endpoint then SEND the payerId and paymentId FOR
        // the the ExecutePayment method that can use it on accessing the post vars
        return actions.request.post(EXECUTE_URL, {
          _token: $('meta[name="csrf-token"]').attr('content'),
          'baToken': data.billingToken
        })
        .then(function(res) {
          $("#paypal-button").hide();


          // print the return from the execute payment from the backend method
          console.log("App.App.BillingAgreementCreate.Done");
          console.log(res);

          let executePayment = "";

          function loopingObj(object) {
            for (key in object) {
              var value = object[key];
              if (typeof value === 'object') {
                // console.log('{');
                loopingObj(value);
                // console.log('}');
              } else {
                // console.log(key + " : " + value);
                // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML += "<span class='text-danger'>"+key+"</span> : " + value + "</br>";
                executePayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";

              }
            }

          }
          let newoutput = JSON.stringify(res, null, '\t')
          var textArea = document.getElementById("BaCreateJsonResponseOutputTextArea");
          textArea.value = newoutput;
          // getting the line numbers from the string
          var lines = newoutput.split("\n");
          textArea.rows = (lines.length+15);
          document.getElementById("baCreateWaitingNotification").innerHTML = "";
          document.getElementById("baCreateNotification").innerHTML = "Done";
          var d = new Date(Date.now());
          let executeTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
          document.getElementById("baTokenTimeDiv").innerHTML = executeTime;


        });
      }
    }, '#paypal-button');

  },
  HasToken:()=>{
    let hasToken = $("#hasToken").html();
    // console.log(hasToken);
    if(hasToken){
      console.log('tem token');

      RtCtb.CalculatedFinancingOptions();
    }else{
      console.log('nao tem token');
    }
  },
  CalculatedFinancingOptions:()=>{
    let finalValue = document.getElementById('finalValue').value;
    window.document.getElementById('payDiv').classList.add('disabledbutton');

    $.ajax({
      url: "/rt/ctb/cfo",// cloud9
      type: "POST",
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: "json", //error dataType since the callback response isnt a json
      // timeout: 10000, //error timeout when it delays
      data: {finalValue:finalValue},
    }).done(function(data, textStatus, jqxhr) {
      console.log("---------------------------------App.CalculatedFinancingOptions.Done---------------------------------");
      console.log(data);
      console.log("App.CheckToken.Done");

      data.financing_options[0].qualifying_financing_options.map(function(data){
        console.log(data.monthly_payment);
      })
      console.log("\n");


      console.log("----------------- Reference Transaction Installments -----------------");
      data.financing_options[0].qualifying_financing_options.map(function(data,i){
        console.log("Terms: " + data.credit_financing.term + " - " +
        "Montly(Init): " + data.monthly_payment.value
      );
    })

    console.log("\n");
    console.log("----------------- Reference Transaction Installments Cost to Buyer (CFO)-----------------");

    // Remove all options from droplist
    document.getElementById('cfoSelect').options.length = 0;

    // let taxs = [0,1,2,3,4,5,6,7,8,9,10,11,12]; // each term get its respective tax // CTB
    let taxs = [-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,-0,0]; // each term get its respective tax // DTB

    data.financing_options[0].qualifying_financing_options.map(function(data,i){
      let discountPercentage=0,discount = 0;
      let amountInit,amountTax,amountWithTax;
      let term = data.credit_financing.term;

      if(term==1 && (data.discount_amount!=null)){
        // if(term==1 && data.hasOwnProperty(discount_amount.value)){
        console.log('data.discount_amount is not undefined');
        discountPercentage = data.discount_percentage;
        discount = data.discount_amount.value;
      }

      amountInit = parseFloat(data.monthly_payment.value).toFixed(2);
      amountTax = parseFloat(((amountInit*taxs[i])/100)).toFixed(2);
      amountWithTax = parseFloat(parseFloat(amountInit) + parseFloat(amountTax)).toFixed(2);
      amountFinal = parseFloat(amountWithTax * term).toFixed(2);

      console.log("Amount(Init): " + amountInit + " - " +
      // "Montly(Init): " + amountInit + " - " +
      "Terms: " + term + " | " +
      "Taxs amount : " +  amountTax + " - " +
      "Montly(Final): " +  amountWithTax + " - " +
      "Amount(Final): " +  amountFinal
    );

    // Populates the droplist
    let select = document.getElementById("cfoSelect");
    let el = document.createElement("option");
    el.textContent = term + "x de R$ " +amountWithTax + " (" + (taxs[i]*-1) + "% de desconto)" ;
    el.value = term;
    el.hasAttribute('amount_final');
    el.setAttribute('amount_final', amountFinal);
    el.hasAttribute('amount_final_montlhy');
    el.setAttribute('amount_final_montlhy', amountWithTax);
    el.hasAttribute('discount');
    el.setAttribute('discount', discount);
    el.hasAttribute('discount_percentage');
    el.setAttribute('discount_percentage', discountPercentage);

    select.appendChild(el);


    document.getElementById("cfoWaitingNotification2").innerHTML = "";
    document.getElementById("cfoNotification2").innerHTML = "Done";
    var d = new Date(Date.now());
    let createTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
    document.getElementById("CfoTimeDiv").innerHTML = createTime;



  })//

  //
  let newoutput = JSON.stringify(data, null, '\t')
  var textArea = document.getElementById("cfoJsonResponseOutputTextArea");
  textArea.value = newoutput;
  var lines = newoutput.split("\n");
  textArea.rows = (lines.length+10);


  document.getElementById('payDiv').classList.remove("disabledbutton");

  // document.getElementById("createPaymentWaitingNotification").innerHTML = "";
  // document.getElementById("createPaymentNotification").innerHTML = "Done";
  //
  // document.getElementById("getTokenWaitingNotification").innerHTML = "";
  // document.getElementById("getTokenNotification").innerHTML = "Done";
  //
  // document.getElementById("executePaymentWaitingNotification").innerHTML = "";
  // document.getElementById("executePaymentNotification").innerHTML = "Done";

}).fail(function(jqxhr, textStatus, errorThrown) {
  console.log(jqxhr);
  console.log(textStatus);
  console.log(errorThrown);
  console.log("App.CheckToken.Fail");
});

},
RemoveBatoken:()=>{
  $.ajax({
    type: "POST",
    url: "/rt/ctb/ba/delete",
    data: {
    },
    headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    dataType: "json"
  })
  .done(function(response) {
    console.log( "success" );
    console.log(response);
    document.getElementById('updateFinalValue').disabled = true;
    document.getElementById('createPayment').disabled = true;

  })
  .fail(function (jqXHR, exception) {
    var msg_err = "";
    if (jqXHR.status === 0) {
      msg_err = "Not connect. Verify Network.";
    } else if (jqXHR.status == 404) {
      msg_err = "Requested page not found. [404]";
    } else if (jqXHR.status == 500) {
      msg_err = "Internal Server Error [500].";
    } else if (exception === "parsererror") {
      msg_err = "Requested JSON parse failed.";
    } else if (exception === "timeout") {
      msg_err = "Time out error.";
    } else if (exception === "abort") {
      msg_err = "Ajax request aborted.";
    } else {
      msg_err = "Uncaught Error. "+ jqXHR.responseText;
    }
    console.log(msg_err);
  })
  .always(function() {
    console.log("complete");
  });

},
Payment:()=>{
  // $('#the_id').data('original-title');
  let e = document.getElementById("cfoSelect");
  let term = e.options[e.selectedIndex].value;
  // let amount = e.options[e.selectedIndex].getAttribute('montly_amount_with_tax');
  let amountFinal = e.options[e.selectedIndex].getAttribute('amount_final');
  let amountFinalMonthly = e.options[e.selectedIndex].getAttribute('amount_final_montlhy');
  let discount = e.options[e.selectedIndex].getAttribute('discount');
  let discountPercentage = e.options[e.selectedIndex].getAttribute('discount_percentage');
  // let discount = document.getElementById('finalDiscountValue').textContent;
  // let discountPercentage = document.getElementById('finalDiscountPercentage').textContent;
  document.getElementById('finalTermsSpan').innerHTML = term;
  document.getElementById('finalAmountSpan').innerHTML = amountFinal;

  $.ajax({
    type: "POST",
    url: "/rt/ctb/payment",
    headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    data: {
      term:term,
      discount:discount,
      discountPercentage:discountPercentage,
      amountFinal:amountFinal,
      amountFinalMonthly:amountFinalMonthly
    },
    dataType: "json"
  })
  .done(function(response) {
    console.log( "success" );
    console.log(response);
    //
    let newoutput = JSON.stringify(response, null, '\t')

    var textArea = document.getElementById("CreatePaymentJsonResponseOutputTextArea");
    textArea.value = newoutput;

    var lines = newoutput.split("\n");
    textArea.rows = (lines.length+10);

    document.getElementById("createPaymentWaitingNotification2").innerHTML = "";
    document.getElementById("createPaymentNotification2").innerHTML = "Done";

    var d = new Date(Date.now());
    let createTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

    document.getElementById("paymentTimeDiv").innerHTML = createTime;

  })
  .fail(function (jqXHR, exception) {
    var msg_err = "";
    if (jqXHR.status === 0) {
      msg_err = "Not connect. Verify Network.";
    } else if (jqXHR.status == 404) {
      msg_err = "Requested page not found. [404]";
    } else if (jqXHR.status == 500) {
      msg_err = "Internal Server Error [500].";
    } else if (exception === "parsererror") {
      msg_err = "Requested JSON parse failed.";
    } else if (exception === "timeout") {
      msg_err = "Time out error.";
    } else if (exception === "abort") {
      msg_err = "Ajax request aborted.";
    } else {
      msg_err = "Uncaught Error. "+ jqXHR.responseText;
    }
    console.log(msg_err);
  })
  .always(function() {
    console.log("complete");
  });



}

}
