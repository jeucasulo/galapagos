$(document).ready(function(){
  Authcap.RenderPayPalButton();
  $("#executeCapBtn").click(Authcap.ExecuteCap);
});

var Authcap = {
  RenderPayPalButton:()=>{
    // button customize options
    // https://developer.paypal.com/docs/archive/checkout/how-to/customize-button/#button-styles

    // creates a paypal button object
    paypal.Button.render({
        style: {
            // size:'responsive',
            // layout:'horizontal',
            // fundingicons:true,
            // label:'paypal',//checkout//pay//buynow//installment//credit
            // locale:'pt_BR'
        },
        // locale: 'en_US',
        // style: {
        //  size: 'small',
        //  color: 'gold',
        //  shape: 'pill',
        //  label: 'checkout',
        //  tagline: 'true'
        // }
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

        env: 'sandbox', // Or 'production'
        // Set up the payment:
        // 1. Add a payment callback
        payment: function(data, actions) {
            // 2. Make a request to your server

            var CREATE_URL = "/ec/server/authcap/create";

            // Excute a post to the endpoint and gets the res.id from the return from the CreatePayment method
            return actions.request.post(CREATE_URL,{_token: $('meta[name="csrf-token"]').attr('content')})

            // Return actions.request.post('/my-api/create-payment/')
            .then(function(res) {
                // 3. Return res.id from the response
                console.log("App.CreateAuthResponse");
                console.log(res); // this

                let createPayment = "";

                function loopingObj(object) {

                    for (key in object) {
                        var value = object[key];
                        if (typeof value === 'object') {
                            loopingObj(value);
                        } else {''
                            createPayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";
                        }
                    }

                    return createPayment;
                }

                // createPayment = loopingObj(res);
                // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = createPayment;

                // let output = JSON.stringify(res);
                // output = output.replace(/,/g, ",<br/>");
                // output = output.replace(/{"/g, "{<br/>\"");
                // output = output.replace(/"}/g, "\"<br/>}");
                // output = output.replace(/\[/g, "\[<br/>");
                // output = output.replace(/\]/g, "<br/>\]");

                let newoutput = JSON.stringify(res, null, '\t');
                // alert(newoutput.length);
                // var lines = $("#ptest").val().split("\n");




                document.getElementById("CreatePaymentJsonResponseOutputTextArea").value = newoutput;

                var textArea = document.getElementById("CreatePaymentJsonResponseOutputTextArea");

                // getting the line numbers from the string
                var lines = newoutput.split("\n");
                textArea.rows = (lines.length+5);

                document.getElementById("createPaymentWaitingNotification").innerHTML = "";
                document.getElementById("createPaymentNotification").innerHTML = "Done";

                // document.getElementById("getTokenWaitingNotification").innerHTML = "";
                // document.getElementById("getTokenNotification").innerHTML = "Done";

                var d = new Date(Date.now());
                let createTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

                document.getElementById("createTimeDiv").innerHTML = createTime;

                return res.id; //this

            });
        },

        // Execute the payment:
        // 1. Add an onAuthorize callback
        onAuthorize: function(data, actions) {
            // 2. Make a request to your server
            //
            var localhost = window.location.hostname;
            // var EXECUTE_URL = "http://"+localhost+"/PPEC/ExecutePayment.php"; // localhost
            var EXECUTE_URL = "/ec/server/authcap/auth"; // cloud9

            // excute a post to the endpoint then SEND the payerId and paymentId FOR
            // the the ExecutePayment method that can use it on accessing the post vars
            return actions.request.post(EXECUTE_URL, {
                    _token: $('meta[name="csrf-token"]').attr('content'), // laravel add
                    paymentID: data.paymentID,
                    payerID: data.payerID
                })
                .then(function(res) {
                    // print the return from the execute payment from the backend method
                    console.log("App.ConfirmAuthResponse");
                    console.log(res);

                    let executePayment = "";

                    function loopingObj(object) {
                        for (key in object) {
                            var value = object[key];
                            if (typeof value === 'object') {
                                // console.log('{');
                                loopingObj(value);
                                // console.log('}');
                            } else {
                                // console.log(key + " : " + value);
                                // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML += "<span class='text-danger'>"+key+"</span> : " + value + "</br>";
                                executePayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";

                            }
                        }

                    }

                    // loopingObj(res);
                    // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML = executePayment;
                    // ExecutePaymentJsonResponseOutputTextArea

                    let executeOutput = JSON.stringify(res, null, '\t')



                    // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
                    document.getElementById("ExecutePaymentJsonResponseOutputTextArea").value = executeOutput;
                    var textAreaExecute = document.getElementById("ExecutePaymentJsonResponseOutputTextArea");
                    var lines = executeOutput.split("\n");
                    textAreaExecute.rows = lines.length+10;

                    document.getElementById("executePaymentWaitingNotification").innerHTML = "";
                    document.getElementById("executePaymentNotification").innerHTML = "Done";

                    var d = new Date(Date.now());
                    let executeTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

                    document.getElementById("executeTimeDiv").innerHTML = executeTime;
                    // document.getElementById("payIdToCap").innerHTML = res.id;
                    document.getElementById("payIdToCap").innerHTML = res.transactions[0].related_resources[0].authorization.id;


                });
        }
    }, '#paypal-button');

    $(".paypal-button-text").hide();

  },
  ExecuteCap:()=>{
    let payIdToCap = $("#payIdToCap").html();
    // alert(payIdToCap);
    // alert($('meta[name="csrf-token"]').attr('content'));
    $.ajax({
      type: "POST",
      // url: "clientId",
      // url: "executeCap",
      url: "/ec/server/authcap/execute",
      data: {
        _token: $('meta[name="csrf-token"]').attr('content'),
        payIdToCap: payIdToCap
      },
      dataType: "json"
    })
    .done(function(response) {
      console.log("success");
      // console.log(response);

      //
      console.log("App.ConfirmAuthResponse");
      console.log(response);

      let executePayment = "";

      function loopingObj(object) {
          for (key in object) {
              var value = object[key];
              if (typeof value === 'object') {
                  // console.log('{');
                  loopingObj(value);
                  // console.log('}');
              } else {
                  // console.log(key + " : " + value);
                  // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML += "<span class='text-danger'>"+key+"</span> : " + value + "</br>";
                  executePayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";

              }
          }

      }

      // loopingObj(res);
      // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML = executePayment;
      // ExecutePaymentJsonResponseOutputTextArea

      let executeOutput = JSON.stringify(response, null, '\t')
      var lines = executeOutput.split("\n");


      // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
      document.getElementById("ExecuteCaptureJsonResponseOutputTextArea").value = executeOutput;
      var textAreaExecute = document.getElementById("ExecuteCaptureJsonResponseOutputTextArea");
      textAreaExecute.rows = lines.length+10;

      document.getElementById("executeCaptureWaitingNotification").innerHTML = "";
      document.getElementById("executeCaptureNotification").innerHTML = "Done";

      var d = new Date(Date.now());
      let executeTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

      document.getElementById("executeCapTimeDiv").innerHTML = executeTime;
      // document.getElementById("payIdToCap").innerHTML = res.id;
      // document.getElementById("payIdToCap").innerHTML = res.transactions[0].related_resources[0].authorization.id;

      //
    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      alert(msg_err);
    })
    .always(function() {
      console.log("complete");
    });

  }

}
