$(document).ready(function(){
  console.log('App.Token');
  $("#GetAccessTokenBtn").click(Token.GetAccessToken);
});

var Token = {
  GetAccessToken:()=>{
    console.log('App.GetAccessToken');
    let client_id = $("#client_id").val();
    let secret = $("#secret").val();
    let app_name = $("#app_name").val();
    let app_desc = $("#app_desc").val();
    let app_id = $("#app_id").val();


    $.ajax({
      type: "POST",
      url: "/token/store",
      data: {
        client_id:client_id,
        secret:secret,
        app_name:app_name,
        app_desc:app_desc,
        app_id:app_id
      },
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: "json"
    })
    .done(function(response) {
      console.log( "success" );
      console.log(response);

      let newoutput = JSON.stringify(response, null, '\t')
      let textArea = document.getElementById("getTokenJsonResponseOutputTextArea");
      textArea.value = newoutput;
      // getting the line numbers from the string
      let lines = newoutput.split("\n");
      textArea.rows = (lines.length+10);
      document.getElementById("getTokenWaitingNotification").innerHTML = "";
      document.getElementById("getTokenNotification").innerHTML = "Done";
      var d = new Date(Date.now());
      let tokenTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
      document.getElementById("tokenTimeDiv").innerHTML = tokenTime;

    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });


  }
}
