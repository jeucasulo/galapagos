$(document).ready(function(){
  console.log('App.Payouts');
  $("#executePayoutsBtn").click(Invoicing.ExecuteInvoicing);
  // $("#sendInvoicingBtn").click(Invoicing.SendInvoicing);
});

var Invoicing = {

  ExecuteInvoicing:()=>{
    console.log('App.ExecutePayouts');
    // console.log(args.callee.toString().match(/function ([^\(]+)/)[1]);


    // var myName = argsuments.callee.toString();
    // myName = myName.substr('function '.length);
    // myName = myName.substr(0, myName.indexOf('('));
    //
    // alert(myName);


    $.ajax({
      type: "POST",
      url: "/payouts/execute",
      data: {

      },
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: "json"
    })
    .done(function(response) {
      console.log( "success" );
      console.log(response);
      // console.log(response.id);
      // document.getElementById('invoicingId').innerHTML = response.id;

      let newoutput = JSON.stringify(response, null, '\t')
      let textArea = document.getElementById("PayoutsExecuteJsonResponseOutputTextArea");
      textArea.value = newoutput;
      // getting the line numbers from the string
      let lines = newoutput.split("\n");
      textArea.rows = (lines.length+10);
      document.getElementById("payoutsExecuteWaitingNotification").innerHTML = "";
      document.getElementById("payoutsExecuteNotification").innerHTML = "Done";
      var d = new Date(Date.now());
      let payoutsTimeDiv = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
      document.getElementById("payoutsTimeDiv").innerHTML = payoutsTimeDiv;

      mySearchId = $("#searchIdPayouts");
      mySearchId.text(response.batch_header.payout_batch_id);
      mySearchId.attr('href','/search/'+response.batch_header.payout_batch_id);
      mySearchId.attr('target','_blank');


    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });


  }
}
