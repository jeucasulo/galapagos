var Client = {
  GetClientId:()=>{
    console.log('App.GetClientId');

      //
      $.ajax({
        type: "GET",
        url: "/ec/client/clientId",
        data: {
        },
        dataType: "json"
      })
      .done(function(response) {
        console.log("success");
        console.log(response);
        Client.RenderPayPalButton(response.clientId);
      })
      .fail(function (jqXHR, exception) {
        var msg_err = "";
        if (jqXHR.status === 0) {
          msg_err = "Not connect. Verify Network.";
        } else if (jqXHR.status == 404) {
          msg_err = "Requested page not found. [404]";
        } else if (jqXHR.status == 500) {
          msg_err = "Internal Server Error [500].";
        } else if (exception === "parsererror") {
          msg_err = "Requested JSON parse failed.";
        } else if (exception === "timeout") {
          msg_err = "Time out error.";
        } else if (exception === "abort") {
          msg_err = "Ajax request aborted.";
        } else {
          msg_err = "Uncaught Error. "+ jqXHR.responseText;
        }
        alert(msg_err);
      })
      .always(function() {
        console.log("complete");
      });

      //

  },
  RenderPayPalButton:(clientId)=>{
    paypal.Button.render({
    // Configure environment
    env: 'sandbox',
    client: {
      sandbox: clientId,
      production: ''
    },
    shipping_address: {
      recipient_name: 'Usuário Teste',
      line1: ', 354 Casa',
      line2: 'Vila Alpina',
      city: 'São Paulo',
      country_code: 'BR',
      postal_code: '03204030',
      phone: '',
      state: 'SP'
    },
    // Customize button (optional)
    locale: 'pt_BR',
    style: {
      size: 'small',
      color: 'gold',
      shape: 'pill',
    },
    //https://developer.paypal.com/docs/checkout/integration-features/customize-button/

    // Enable Pay Now checkout flow (optional)
    commit: true,

    // Set up a payment
    payment: function(data, actions) {
      return actions.payment.create({
        transactions: [{
          amount: {
            total: '155.4',
            currency: 'BRL',
            details: {
              subtotal: '155.4',
              tax: '0.00',
              shipping: '0.00',
              handling_fee: '0.00',
              shipping_discount: '0.00',
              insurance: '0.00'
          }
        },
        description: 'Compra na loja Porta Perfumes',
        custom: '8572060',
        invoice_number: new Date().getUTCMilliseconds()+Math.random(),
        payment_options: {
          allowed_payment_method: 'INSTANT_FUNDING_SOURCE'
        },
        soft_descriptor: 'ECHI5786786',
            item_list: {
              items: [
                {
                  name: 'Perfume 5 ml',
                  description: 'Decant / Amostra do Perfume Masculino Joop! Homme Eau de Toilette (EDT) 5 ml',
                  quantity: '3',
                  price: '15.90',
                  tax: '0.00',
                  sku: 'PP_37',
                  currency: 'BRL'
                },
                {
                  name: 'Perfume 2,5 ml',
                  description: 'Decant / Amostra do Perfume Unissex Prada Les Infusions Amande Eau de Parfum (EDP) 2,5 ml',
                  quantity: '3',
                  price: '24.90',
                  tax: '0.00',
                  sku: 'PP_38',
                  currency: 'BRL'
                },
                {
                  name: 'Frete',
                  description: 'SEDEX',
                  quantity: '1',
                  price: '33.00',
                  tax: '0.00',
                  sku: 'Correios',
                  currency: 'BRL'
                }
              ],
            }
          }
        ],
      },);
      },
      // Execute the payment
      onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function(res) {
      // Show a confirmation message to the buyer
      console.log('--------------------res:onAuthorize--------------------');
      console.log(res);
      console.log('--------------------data:onAuthorize--------------------');
      console.log(data);
      window.alert('Pagamento efetuado!');
      });
      }
    }, '#paypal-button');

  }
}
$(document).ready(function(){
    Client.GetClientId();
});
