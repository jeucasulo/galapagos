// sessionStorage.clear();
// let installments = $("#installments").val();

var e = document.getElementById("installments");
var installments = e.options[e.selectedIndex].value;
// var initAmount = 100;
var initAmount = document.getElementById('initAmount').value;

createPayment(initAmount, installments);



function createPayment(initAmount,installments){
    console.log("---------------------------------Create Payment---------------------------------"+initAmount+"");

        // --------------CTB-------------- //
        let termsMax = 12; // max terms on set on paypal account
        // let termsSelected = 5; // number of terms
        let termsSelected = installments; // number of terms
        let shipping = 1;

        // let amountMax = 100;
        let amountMax = parseFloat(initAmount)+parseFloat(shipping);
        console.log('---------------------------------------------------------------------------------------amountMax: '+amountMax);
        let amountMountly = amountMax / termsSelected;

        let taxsTable = ['0.0',0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,'1.0',1.1];
        let taxsTerm = taxsTable[termsSelected-1];


        console.log('App.Init\n\n');
        console.log('Terms selected: ' + termsSelected + " | Taxs: " + taxsTerm + " | Amount: " + amountMax + " | Mountly: " + amountMountly);
        console.log(`Terms selected: ${termsSelected} | Taxs: ${taxsTerm} | Amount: ${amountMax} | Mountly: ${amountMountly}`);
        // for(tax in taxs){
        //   console.log(tax);
        // }
        for(let i = 1; i <= 12; i++ ){

          let amountMountlyNew = amountMax/i;
          // let amountMountlyNewTax = ((amountMax/100)*taxsTable[i-1])+amountMountlyNew; // formatted in the line below
          let amountMountlyNewTax = parseFloat(((amountMax/100)*taxsTable[i-1])+amountMountlyNew).toFixed(2);
          // let amountMaxTax = amountMountlyNewTax * i; // formatted in the line below
          let amountMaxTax = parseFloat(amountMountlyNewTax * i).toFixed(2);

          console.log(
          "[Terms: " + i + " - Tax: " + taxsTable[i-1] + "% ] => " +
          "Initial Amount: " + amountMax.toFixed(2) + " ("+i+"x"+amountMountlyNew.toFixed(2)+") | " +
          "Final Amount: " + amountMaxTax + " ("+i+"x"+amountMountlyNewTax+")" +
          (i==termsSelected ? " <= selected term":"")+
          (i==termsSelected ? finalAmount=amountMaxTax:"")
          );

        }
        console.log("finalAmount: "+finalAmount);
    // --------------CTB-------------- //
    $.ajax({
    // url: "execute-payment.php",// localhost
    // url: "PPP-CTB/create-payment.php?installments="+installments+"&finalAmount="+finalAmount,// cloud9
    url: "/pplus/ctb/payment/create",// cloud9
    type: "POST",
    dataType: "json", //error dataType since the callback response isnt a json
    headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    data:{installments:installments,finalAmount:finalAmount}
    // timeout: 10000, //error timeout when it delays
    // data: JSON.stringify(reqs),
    // data: { "payer_id": payerID },
}).done(function(data, textStatus, jqxhr) {
    console.log("---------------------------------data---------------------------------");
    console.log(data);
    console.log("---------------------------------data.links[1].href---------------------------------");
    console.log(data.links[1].href);
    // approval[0] = data.links[1].href;
    // sessionStorage.removeItem('approval_url');
    approval_url = data.links[1].href;
    sessionStorage.setItem('approval_url', data.links[1].href);
    sessionStorage.setItem('execute_url', data.links[2].href);



    var imported = document.createElement('script');
    imported.src = '/js/pplus-ctb/script.js';
    document.head.appendChild(imported);

      //
    let newoutput = JSON.stringify(data, null, '\t')

    // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
    document.getElementById("CreatePaymentJsonResponseOutputTextArea").value = newoutput;

    var textArea = document.getElementById("CreatePaymentJsonResponseOutputTextArea");
    // getting the line numbers from the string
    var lines = newoutput.split("\n");
    textArea.rows = (lines.length+10);

    document.getElementById("createPaymentWaitingNotification").innerHTML = "";
    document.getElementById("createPaymentNotification").innerHTML = "Done";

    document.getElementById("getTokenWaitingNotification").innerHTML = "";
    document.getElementById("getTokenNotification").innerHTML = "Done";

    var d = new Date(Date.now());
    let createTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

    document.getElementById("createTimeDiv").innerHTML = createTime;


    console.log("App.CreatePayment.Done");
}).fail(function(jqxhr, textStatus, errorThrown) {
    console.log(jqxhr);
    console.log(textStatus);
    console.log(errorThrown);
    console.log("App.CreatePayment.Fail");
});





}
