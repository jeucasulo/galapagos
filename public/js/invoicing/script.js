$(document).ready(function(){
  console.log('App.Invoicing');
  $("#createInvoicingBtn").click(Invoicing.CreateInvoicing);
  $("#sendInvoicingBtn").click(Invoicing.SendInvoicing);
});

var Invoicing = {
  CreateInvoicing:()=>{
    console.log('App.CreateInvoicing');


    $.ajax({
      type: "POST",
      url: "/invoicing/create",
      data: {
      },
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: "json"
    })
    .done(function(response) {
      console.log( "success" );
      console.log(response);
      console.log(response.id);
      document.getElementById('invoicingId').innerHTML = response.id;

      let newoutput = JSON.stringify(response, null, '\t')
      let textArea = document.getElementById("InvoicingCreateJsonResponseOutputTextArea");
      textArea.value = newoutput;
      // getting the line numbers from the string
      let lines = newoutput.split("\n");
      textArea.rows = (lines.length+10);
      document.getElementById("invoicingCreateWaitingNotification").innerHTML = "";
      document.getElementById("invoicingCreateNotification").innerHTML = "Done";
      var d = new Date(Date.now());
      let invoicingCreateTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
      document.getElementById("invoicingCreateTimeDiv").innerHTML = invoicingCreateTime;

      mySearchId = $("#searchIdInvoicing");
      mySearchId.text(response.id);
      mySearchId.attr('href','/search/'+response.id);
      mySearchId.attr('target','_blank');


    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });


  },
  SendInvoicing:()=>{
    console.log('App.ExecuteInvoicing');
    let invoice_id = $("#invoicingId").html();
    console.log(invoice_id);

    $.ajax({
      type: "POST",
      url: "/invoicing/execute",
      data: {
        invoice_id:invoice_id
      },
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: "json"
    })
    .done(function(response) {
      console.log( "success" );
      console.log(response);
      console.log(response.id);
      // document.getElementById('invoicingId').innerHTML = response.id;

      let newoutput = JSON.stringify(response, null, '\t')
      let textArea = document.getElementById("InvoicingExecuteJsonResponseOutputTextArea");
      textArea.value = newoutput;
      // getting the line numbers from the string
      let lines = newoutput.split("\n");
      textArea.rows = (lines.length+10);
      document.getElementById("invoicingExecuteWaitingNotification").innerHTML = "";
      document.getElementById("invoicingExecuteNotification").innerHTML = "Done";
      var d = new Date(Date.now());
      let invoicingExecuteTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
      document.getElementById("invoicingExecuteTimeDiv").innerHTML = invoicingExecuteTime;

    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });


  }
}
