$(document).ready(function(){
  RT.RenderPayPalButton();
  $("#inactiveButton").click(RT.RemoveBatoken);
  $("#executePayment").click(RT.CreatePayment);
});


var RT = {
  RenderPayPalButton:()=>{
    // creates a paypal button object
    paypal.Button.render({
      env: 'sandbox', // Or 'production'
      // Set up the payment:
      // 1. Add a payment callback
      payment: function(data, actions) {
        // 2. Make a request to your server

        var CREATE_URL = "/rt/ba/token";

        // Excute a post to the endpoint and gets the res.id from the return from the CreatePayment method
        return actions.request.post(CREATE_URL,{
          _token: $('meta[name="csrf-token"]').attr('content')
        })

        // Return actions.request.post('/my-api/create-payment/')
        .then(function(res) {
          // 3. Return res.id from the response
          console.log("App.BillingAgreementToken.Done");
          console.log(res); // this

          let createPayment = "";

          function loopingObj(object) {

            for (key in object) {
              var value = object[key];
              if (typeof value === 'object') {
                loopingObj(value);
              } else {
                createPayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";
              }
            }

            return createPayment;
          }

          let newoutput = JSON.stringify(res, null, '\t')
          let textArea = document.getElementById("BaTokenJsonResponseOutputTextArea");
          textArea.value = newoutput;
          // getting the line numbers from the string
          let lines = newoutput.split("\n");
          textArea.rows = (lines.length+10);
          document.getElementById("baTokenWaitingNotification").innerHTML = "";
          document.getElementById("baTokentNotification").innerHTML = "Done";
          var d = new Date(Date.now());
          let baToken = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
          document.getElementById("baTokenTimeDiv").innerHTML = baToken;

          mySearchId = $("#serchIdBillingToken");
          // console.log('res.token_id');
          // console.log(res.token_id);
          mySearchId.text(res.token_id);
          mySearchId.attr('href','/search/'+res.token_id);
          mySearchId.attr('target','_blank');

          return res.token_id; //this

        });
      },

      // Execute the payment:
      // 1. Add an onAuthorize callback
      onAuthorize: function(data, actions) {
        console.log("App.Authorizes.Done");
        console.log(data);
        $("#paypal-button").hide();

        // 2. Make a request to your server
        //
        var localhost = window.location.hostname;

        var EXECUTE_URL = "/rt/ba/create";

        // excute a post to the endpoint then SEND the payerId and paymentId FOR
        // the the ExecutePayment method that can use it on accessing the post vars
        return actions.request.post(EXECUTE_URL, {
          _token: $('meta[name="csrf-token"]').attr('content'),
          'baToken': data.billingToken
        })
        .then(function(res) {
          // print the return from the execute payment from the backend method
          console.log("App.App.BillingAgreementCreate.Done");
          console.log(res);

          let executePayment = "";

          function loopingObj(object) {
            for (key in object) {
              var value = object[key];
              if (typeof value === 'object') {
                // console.log('{');
                loopingObj(value);
                // console.log('}');
              } else {
                // console.log(key + " : " + value);
                // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML += "<span class='text-danger'>"+key+"</span> : " + value + "</br>";
                executePayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";

              }
            }

          }

          let newoutput = JSON.stringify(res, null, '\t')
          var textArea = document.getElementById("BaCreateJsonResponseOutputTextArea");
          textArea.value = newoutput;
          // getting the line numbers from the string
          var lines = newoutput.split("\n");
          textArea.rows = (lines.length+15);
          document.getElementById("baCreateWaitingNotification").innerHTML = "";
          document.getElementById("baCreateNotification").innerHTML = "Done";
          var d = new Date(Date.now());
          let executeTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
          document.getElementById("baCreateTimeDiv").innerHTML = executeTime;

          mySearchId = $("#serchIdBillingAgreement");
          // console.log('res.token_id');
          // console.log(res.token_id);
          mySearchId.text(res.token_id);
          mySearchId.attr('href','/search/'+res.token_id);
          mySearchId.attr('target','_blank');




        });
      }
    }, '#paypal-button');

  },
  CreatePayment:()=>{
    $.ajax({
      type: "POST",
      url: "/rt/payment",
      data: {
      },
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: "json"
    })
    .done(function(response) {
      console.log( "success" );
      console.log(response);

      let newoutput = JSON.stringify(response, null, '\t')

      var textArea = document.getElementById("CreatePaymentJsonResponseOutputTextArea");
      textArea.value = newoutput;

      var lines = newoutput.split("\n");
      textArea.rows = (lines.length+10);

      document.getElementById("createPaymentWaitingNotification2").innerHTML = "";
      document.getElementById("createPaymentNotification2").innerHTML = "Done";

      var d = new Date(Date.now());
      let createTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

      document.getElementById("paymentTimeDiv").innerHTML = createTime;

      mySearchId = $("#searchIdPayment");
      mySearchId.text(response.id);
      mySearchId.attr('href','/search/'+response.id);
      mySearchId.attr('target','_blank');


      // document.getElementById("createPayment").disabled = false;


    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });

  },
  RemoveBatoken:()=>{
    $.ajax({
      type: "POST",
      url: "/rt/ba/delete",
      data: {
      },
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: "json"
    })
    .done(function(response) {
      document.getElementById('executePayment').disabled = true;
      console.log( "success" );
      console.log(response);
    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });

  }
}
