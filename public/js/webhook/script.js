$(document).ready(function(){
  console.log('App.Webhook.init');
  $("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
  });
  $("#getEvents").click(Webhook.GetEvents);


  // $("#createPlanBtn").click(Sub.CreatePlan);
  // $("#activatePlanBtn").click(Sub.ActivePlan);
  // $("#removePlan").click(Sub.RemovePlan);
  // $("#createAgreementBtn").click(Sub.CreateAgreement);
});

var Webhook = {
  CreateWebhook:(eventObj)=>{
    console.log('App.CreateWebhook');
    console.log(eventObj);
    $.ajax({
      type: "POST",
      url: "/webhook/store",
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        eventObj:eventObj
      },
      dataType: "json"
    })
    .done(function(response) {
      console.log( "success" );
      console.log(response);
      // let webhookCreated = response.id === undefined ? 'Error' : true;
      // console.log(webhookCreated);
      if(response.id === undefined){
        return console.log('error..');
      }
      console.log(response.id);
      // document.getElementById("createPlanBtn").disabled = true;
      // document.getElementById("activatePlanBtn").disabled = false;
      //
      // let newoutput = JSON.stringify(response, null, '\t')
      // let textArea = document.getElementById("CreatePlanJsonResponseOutputTextArea");
      // textArea.value = newoutput;
      // // getting the line numbers from the string
      // let lines = newoutput.split("\n");
      // textArea.rows = (lines.length+10);
      // document.getElementById("createPlanWaitingNotification").innerHTML = "";
      // document.getElementById("createPlanNotification").innerHTML = "Done";
      // var d = new Date(Date.now());
      // let createPlan = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
      // document.getElementById("createPlanTimeDiv").innerHTML = createPlan;
      Webhook.UpdateWebhook(response.id);

    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });
    // });
  },
  UpdateWebhook:(webhook)=>{
    console.log('App.UpdateWebhook');
    $.ajax({
      type: "PUT",
      url: "/webhook/update",
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        webhook:webhook
      },
      dataType: "json"
    })
    .done(function(response) {
      console.log( "success" );
      console.log(response);

      // document.getElementById("createPlanBtn").disabled = true;
      // document.getElementById("activatePlanBtn").disabled = false;
      //
      // let newoutput = JSON.stringify(response, null, '\t')
      // let textArea = document.getElementById("CreatePlanJsonResponseOutputTextArea");
      // textArea.value = newoutput;
      // // getting the line numbers from the string
      // let lines = newoutput.split("\n");
      // textArea.rows = (lines.length+10);
      // document.getElementById("createPlanWaitingNotification").innerHTML = "";
      // document.getElementById("createPlanNotification").innerHTML = "Done";
      // var d = new Date(Date.now());
      // let createPlan = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
      // document.getElementById("createPlanTimeDiv").innerHTML = createPlan;
      // Webhook.UpdateWebhook();

    })
    .fail(function (jqXHR, exception) {
      var msg_err = "";
      if (jqXHR.status === 0) {
        msg_err = "Not connect. Verify Network.";
      } else if (jqXHR.status == 404) {
        msg_err = "Requested page not found. [404]";
      } else if (jqXHR.status == 500) {
        msg_err = "Internal Server Error [500].";
      } else if (exception === "parsererror") {
        msg_err = "Requested JSON parse failed.";
      } else if (exception === "timeout") {
        msg_err = "Time out error.";
      } else if (exception === "abort") {
        msg_err = "Ajax request aborted.";
      } else {
        msg_err = "Uncaught Error. "+ jqXHR.responseText;
      }
      console.log(msg_err);
    })
    .always(function() {
      console.log("complete");
    });

  },
  GetEvents:()=>{
    console.log('App.GetEvents');
    let count = 0;
    $.each($("input[name='events']:checked"), function(i){
      count++;
    });

    if(count === 0){
      return;
    }

    function Events(event) {
      this.name = event;
    }

    let eventObj = [];
    var eventsArray = [];
    $.each($("input[name='events']:checked"), function(i){
      eventsArray.push($(this).val());
      eventObj[i] = new Events($(this).val());
    });
    // Here array.values() function is called.
    var iterator = eventsArray.values();

    // Here all the elements of the array is being printed.
    for (let elements of iterator) {
      console.log(elements);
    }
    Webhook.CreateWebhook(eventObj);
    // console.log(eventObj[0].event);
    // console.log(eventObj);

  },
}
