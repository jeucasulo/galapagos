$(document).ready(function(){
  EcRestServerV2.RenderPayPalButton();
});
var EcRestServerV2 = {
  RenderPayPalButton:()=>{

    paypal.Buttons({
      createOrder: function() {
          // var form = new FormData(document.getElementById('myForm'));
          // return fetch('/my-server/create-paypal-transaction')
          // return fetch('/EC-V2/script.php')
          return fetch('/ec/server/v2/order/create',{
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },

          method: 'post',
          // body: {_token: $('meta[name="csrf-token"]').attr('content')}

          // headers: {
          // Accept: 'application/json',
          //   'Content-Type': 'application/x-www-form-urlencoded'
          // }
          // body: 'name=jeu&age=30&city=sao paulo'






        })
          .then(function(res) {
            console.log('------------------Sucesso------------------');

            console.log("App.CreatePaymentResponse");
            console.log(res); // this

            let createPayment = "";

            function loopingObj(object) {

                for (key in object) {
                    var value = object[key];
                    if (typeof value === 'object') {
                        loopingObj(value);
                    } else {''
                        createPayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";
                    }
                }

                return createPayment;
            }

            let newoutput = JSON.stringify(res, null, '\t')

            // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
            document.getElementById("CreatePaymentJsonResponseOutputTextArea").value = newoutput;

            var textArea = document.getElementById("CreatePaymentJsonResponseOutputTextArea");
            // getting the line numbers from the string
            var lines = newoutput.split("\n");
            textArea.rows = (lines.length+10);

            document.getElementById("createPaymentWaitingNotification").innerHTML = "";
            document.getElementById("createPaymentNotification").innerHTML = "Done";

            // document.getElementById("getTokenWaitingNotification").innerHTML = "";
            // document.getElementById("getTokenNotification").innerHTML = "Done";

            var d = new Date(Date.now());
            let createTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

            document.getElementById("createTimeDiv").innerHTML = createTime;

            console.log(res);
            return res.json();
          }).then(function(data) {
            console.log(data);
            return data.id;
          });
      },
          // Finalize the transaction
      onApprove: function(data, actions) {
          console.log(data);
          return fetch('/ec/server/v2/order/capture' , {
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'content-type': 'application/json'
              },

              method: 'post',

              body: JSON.stringify({
                orderID: data.orderID
              })
          }).then(function(res) {
              console.log('---------------Sucesso---------------');
              console.log(res);
              return res.json();
          }).then(function(details) {
              // Show a success message to the buyer
              console.log(details);
              alert('Transaction completed by ' + details.payer.name.given_name + '!');
          });
      }
    }).render('#paypal-button-container');


  }
}
