$(document).ready(function () {
    Sub.CreateBaButton();
    $("#createPlanBtn").click(Sub.CreatePlan);
    $("#activatePlanBtn").click(Sub.ActivePlan);
    $("#removePlan").click(Sub.RemovePlan);
    $("#createAgreementBtn").click(Sub.CreateAgreement);
});

var Sub = {
    CheckPlan: () => {

    },
    CreateAgreementDone: (res) => {

        let newoutput = JSON.stringify(res, null, '\t')

        // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
        document.getElementById("CreateAgreementJsonResponseOutputTextArea").value = newoutput;

        var textArea = document.getElementById("CreateAgreementJsonResponseOutputTextArea");
        var lines = newoutput.split("\n");
        textArea.rows = (lines.length + 10);

        document.getElementById("createAgreementWaitingNotification").innerHTML = "";
        document.getElementById("createAgreementNotification").innerHTML = "Done";

        // document.getElementById("getTokenWaitingNotification").innerHTML = "";
        // document.getElementById("getTokenNotification").innerHTML = "Done";

        var d = new Date(Date.now());
        let createTime = "Resposta gerada às " + d.getHours() + " horas, " + d.getMinutes() + " minutos e " + d.getSeconds() + " segundos";

        document.getElementById("createAgreementTimeDiv").innerHTML = createTime;


    },
    ExecuteAgreementDone: (res) => {

        let newoutput = JSON.stringify(res, null, '\t')

        // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
        document.getElementById("ExecuteAgreementJsonResponseOutputTextArea").value = newoutput;

        var textArea = document.getElementById("ExecuteAgreementJsonResponseOutputTextArea");
        var lines = newoutput.split("\n");
        textArea.rows = (lines.length + 10);

        document.getElementById("executeAgreementWaitingNotification").innerHTML = "";
        document.getElementById("executeAgreementNotification").innerHTML = "Done";

        // document.getElementById("getTokenWaitingNotification").innerHTML = "";
        // document.getElementById("getTokenNotification").innerHTML = "Done";

        var d = new Date(Date.now());
        let createTime = "Resposta gerada às " + d.getHours() + " horas, " + d.getMinutes() + " minutos e " + d.getSeconds() + " segundos";

        document.getElementById("executeAgreementTimeDiv").innerHTML = createTime;


    },
    CreateBaButton: () => {


        paypal.Button.render({
            env: 'sandbox', // Or 'production'
            // Set up the payment:
            // 1. Add a payment callback
            payment: function (data, actions) {
                // 2. Make a request to your server
                // return actions.request.post('/my-api/create-payment/')
                return actions.request.post('/sub/ba', {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                })
                    .then(function (res) {
                        Sub.CreateAgreementDone(res);
                        console.log('############## sub/ba ##############');
                        console.log(res);
                        // 3. Return res.id from the response
                        console.log(res.links[0].href);
                        return res.links[0].href;
                    });
            },
            // Execute the payment:
            // 1. Add an onAuthorize callback
            onAuthorize: function (data, actions) {
                // 2. Make a request to your server
                // return actions.request.post('/my-api/execute-payment/', {

                console.log('############ data ############')
                console.log(data)
                return actions.request.post('/sub/ba/execute', {
                    // paymentID: data.paymentID,
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    // ecToken: data.orderID
                    ecToken: data.paymentToken
                })
                    .then(function (res) {
                        Sub.ExecuteAgreementDone(res);
                        console.log(res);
                        // 3. Show the buyer a confirmation message.
                    });
            }
        }, '#paypal-button');



    },
    CreatePlan: () => {
        // $(document).ready(function(){
        console.log('App.CreatePlan');
        $.ajax({
            type: "POST",
            url: "/sub/plan/create",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
            },
            dataType: "json"
        })
            .done(function (response) {
                console.log("success");
                console.log(response);

                document.getElementById("createPlanBtn").disabled = true;
                document.getElementById("activatePlanBtn").disabled = false;

                let newoutput = JSON.stringify(response, null, '\t')
                let textArea = document.getElementById("CreatePlanJsonResponseOutputTextArea");
                textArea.value = newoutput;
                // getting the line numbers from the string
                let lines = newoutput.split("\n");
                textArea.rows = (lines.length + 10);
                document.getElementById("createPlanWaitingNotification").innerHTML = "";
                document.getElementById("createPlanNotification").innerHTML = "Done";
                var d = new Date(Date.now());
                let createPlan = "Resposta gerada às " + d.getHours() + " horas, " + d.getMinutes() + " minutos e " + d.getSeconds() + " segundos";
                document.getElementById("createPlanTimeDiv").innerHTML = createPlan;

                mySearchId = $("#searchIdPlan");
                mySearchId.text(response.id);
                mySearchId.attr('href', '/search/' + response.id);
                mySearchId.attr('target', '_blank');


            })
            .fail(function (jqXHR, exception) {
                var msg_err = "";
                if (jqXHR.status === 0) {
                    msg_err = "Not connect. Verify Network.";
                } else if (jqXHR.status == 404) {
                    msg_err = "Requested page not found. [404]";
                } else if (jqXHR.status == 500) {
                    msg_err = "Internal Server Error [500].";
                } else if (exception === "parsererror") {
                    msg_err = "Requested JSON parse failed.";
                } else if (exception === "timeout") {
                    msg_err = "Time out error.";
                } else if (exception === "abort") {
                    msg_err = "Ajax request aborted.";
                } else {
                    msg_err = "Uncaught Error. " + jqXHR.responseText;
                }
                console.log(msg_err);
            })
            .always(function () {
                console.log("complete");
            });
        // });
    },
    ActivePlan: () => {
        console.log('App.ActivePlan');
        // $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "/sub/plan/active",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
            },
            dataType: "json"
        })
            .done(function (response) {
                console.log("success");
                console.log(response);
                document.getElementById("activatePlanBtn").disabled = true;


                let newoutput = JSON.stringify(response, null, '\t')
                let textArea = document.getElementById("ActivePlanJsonResponseOutputTextArea");
                textArea.value = newoutput;
                // getting the line numbers from the string
                let lines = newoutput.split("\n");
                textArea.rows = (lines.length + 10);
                document.getElementById("activePlanWaitingNotification").innerHTML = "";
                document.getElementById("activePlanNotification").innerHTML = "Done";
                var d = new Date(Date.now());
                let activePlan = "Resposta gerada às " + d.getHours() + " horas, " + d.getMinutes() + " minutos e " + d.getSeconds() + " segundos";
                document.getElementById("activePlanTimeDiv").innerHTML = activePlan;

            })
            .fail(function (jqXHR, exception) {
                var msg_err = "";
                if (jqXHR.status === 0) {
                    msg_err = "Not connect. Verify Network.";
                } else if (jqXHR.status == 404) {
                    msg_err = "Requested page not found. [404]";
                } else if (jqXHR.status == 500) {
                    msg_err = "Internal Server Error [500].";
                } else if (exception === "parsererror") {
                    msg_err = "Requested JSON parse failed.";
                } else if (exception === "timeout") {
                    msg_err = "Time out error.";
                } else if (exception === "abort") {
                    msg_err = "Ajax request aborted.";
                } else {
                    msg_err = "Uncaught Error. " + jqXHR.responseText;
                }
                console.log(msg_err);
            })
            .always(function () {
                console.log("complete");
            });
        // });
    },
    RemovePlan: () => {
        console.log('App.RemovePlan');
        // $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "/sub/plan/remove",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
            },
            dataType: "json"
        })
            .done(function (response) {
                console.log("success");
                console.log(response);
                document.getElementById("createAgreementBtn").disabled = true;
                document.getElementById("removePlan").disabled = true;


                // let newoutput = JSON.stringify(response, null, '\t')
                // let textArea = document.getElementById("ActivePlanJsonResponseOutputTextArea");
                // textArea.value = newoutput;
                // // getting the line numbers from the string
                // let lines = newoutput.split("\n");
                // textArea.rows = (lines.length+10);
                // document.getElementById("activePlanWaitingNotification").innerHTML = "";
                // document.getElementById("activePlanNotification").innerHTML = "Done";
                // var d = new Date(Date.now());
                // let activePlan = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";
                // document.getElementById("activePlanTimeDiv").innerHTML = activePlan;

            })
            .fail(function (jqXHR, exception) {
                var msg_err = "";
                if (jqXHR.status === 0) {
                    msg_err = "Not connect. Verify Network.";
                } else if (jqXHR.status == 404) {
                    msg_err = "Requested page not found. [404]";
                } else if (jqXHR.status == 500) {
                    msg_err = "Internal Server Error [500].";
                } else if (exception === "parsererror") {
                    msg_err = "Requested JSON parse failed.";
                } else if (exception === "timeout") {
                    msg_err = "Time out error.";
                } else if (exception === "abort") {
                    msg_err = "Ajax request aborted.";
                } else {
                    msg_err = "Uncaught Error. " + jqXHR.responseText;
                }
                console.log(msg_err);
            })
            .always(function () {
                console.log("complete");
            });
        // });
    },
    CreateAgreement: () => {
        console.log('App.CreateAgreement');
        // $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "/sub/ba",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
            },
            dataType: "json"
        })
            .done(function (response) {
                console.log("success");
                console.log(response);
                document.getElementById("activatePlanBtn").disabled = true;


                let newoutput = JSON.stringify(response, null, '\t')
                let textArea = document.getElementById("CreateAgreementJsonResponseOutputTextArea");
                textArea.value = newoutput;
                // getting the line numbers from the string
                let lines = newoutput.split("\n");
                textArea.rows = (lines.length + 10);
                document.getElementById("createAgreementWaitingNotification").innerHTML = "";
                document.getElementById("createAgreementNotification").innerHTML = "Done";
                var d = new Date(Date.now());
                let createAgreement = "Resposta gerada às " + d.getHours() + " horas, " + d.getMinutes() + " minutos e " + d.getSeconds() + " segundos";
                document.getElementById("createAgreementTimeDiv").innerHTML = createAgreement;


            })
            .fail(function (jqXHR, exception) {
                var msg_err = "";
                if (jqXHR.status === 0) {
                    msg_err = "Not connect. Verify Network.";
                } else if (jqXHR.status == 404) {
                    msg_err = "Requested page not found. [404]";
                } else if (jqXHR.status == 500) {
                    msg_err = "Internal Server Error [500].";
                } else if (exception === "parsererror") {
                    msg_err = "Requested JSON parse failed.";
                } else if (exception === "timeout") {
                    msg_err = "Time out error.";
                } else if (exception === "abort") {
                    msg_err = "Ajax request aborted.";
                } else {
                    msg_err = "Uncaught Error. " + jqXHR.responseText;
                }
                console.log(msg_err);
            })
            .always(function () {
                console.log("complete");
            });

    }

}
