var approval_url = sessionStorage.getItem('approval_url')

ppp = PAYPAL.apps.PPP(
    {
        "approvalUrl": approval_url,
        "placeholder": "ppplus",
        "mode": "sandbox",
        "payerFirstName": "Jeu",
        "payerLastName": " Junior",
        "payerPhone": " customerPhone",
        "payerTaxId": "12345678909",
        "payerTaxIdType": "BR_CPF",
        "language": "pt_BR",
        "country": "BR",
        "rememberedCards": "customerRememberedCardHash",
        "payerEmail": "jeucasulo@hotmail.com",
        // custom
        // "merchantInstallmentSelectionOptional": "false",
        // "merchantInstallmentSelection": "2",
    });

document.getElementById('load').style.display = 'none';
