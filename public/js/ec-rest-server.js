$(document).ready(function(){
  EC.RenderPayPalButton();
  EC.Json();
})
var EC = {
  RenderPayPalButton:()=>{
    console.log("App.RenderPayPalButton");

    // button customize options
    // https://developer.paypal.com/docs/archive/checkout/how-to/customize-button/#button-styles

    // creates a paypal button object
    paypal.Button.render({
        style: {
            // size:'responsive',
            // layout:'horizontal',
            // fundingicons:true,
            // label:'paypal',//checkout//pay//buynow//installment//credit
            // locale:'pt_BR'
        },
        // locale: 'en_US',
        // style: {
        //  size: 'small',
        //  color: 'gold',
        //  shape: 'pill',
        //  label: 'checkout',
        //  tagline: 'true'
        // }
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

        env: 'sandbox', // Or 'production'
        // Set up the payment:
        // 1. Add a payment callback
        payment: function(data, actions) {
            // 2. Make a request to your server

            var CREATE_URL = "/ec/server/payment/create";
            let createPaymentJson = document.getElementById("textareaJsonCreatePayment").value;
            let createPaymentJsonCustom = document.getElementById('createPaymentJsonCustom').checked;
            let textareaJsonCreatePaymentHeader = document.getElementById('textareaJsonCreatePaymentHeader').value;
            let createPaymentJsonMock = document.getElementById('createPaymentJsonMock').value;
            console.log(createPaymentJsonMock);
            // let createPaymentJsonCustom = document.getElementById('createPaymentJsonCustom').checked? true:false;
            // console.log(createPaymentJsonCustom);

            // Excute a post to the endpoint and gets the res.id from the return from the CreatePayment method
            return actions.request.post(CREATE_URL,{
              _token: $('meta[name="csrf-token"]').attr('content'),
              createPaymentJson : createPaymentJson,
              createPaymentJsonCustom : createPaymentJsonCustom,
              textareaJsonCreatePaymentHeader : textareaJsonCreatePaymentHeader,
              createPaymentJsonMock : createPaymentJsonMock
            })

            // Return actions.request.post('/my-api/create-payment/')
            .then(function(res) {
                // 3. Return res.id from the response
                console.log("App.CreatePaymentResponse");
                console.log(res); // this

                let createPayment = "";

                function loopingObj(object) {

                    for (key in object) {
                        var value = object[key];
                        if (typeof value === 'object') {
                            loopingObj(value);
                        } else {''
                            createPayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";
                        }
                    }

                    return createPayment;
                }

                // createPayment = loopingObj(res);
                // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = createPayment;

                // let output = JSON.stringify(res);
                // output = output.replace(/,/g, ",<br/>");
                // output = output.replace(/{"/g, "{<br/>\"");
                // output = output.replace(/"}/g, "\"<br/>}");
                // output = output.replace(/\[/g, "\[<br/>");
                // output = output.replace(/\]/g, "<br/>\]");

                let newoutput = JSON.stringify(res, null, '\t')

                // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
                document.getElementById("CreatePaymentJsonResponseOutputTextArea").value = newoutput;

                var textArea = document.getElementById("CreatePaymentJsonResponseOutputTextArea");
                var lines = newoutput.split("\n");
                textArea.rows = (lines.length+10);

                document.getElementById("createPaymentWaitingNotification").innerHTML = "";
                document.getElementById("createPaymentNotification").innerHTML = "Done";

                // document.getElementById("getTokenWaitingNotification").innerHTML = "";
                // document.getElementById("getTokenNotification").innerHTML = "Done";

                var d = new Date(Date.now());
                let createTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

                document.getElementById("createTimeDiv").innerHTML = createTime;

                

                // console.log(searchLink);
                // document.getElementById("testando").text = res.id;
                // document.getElementById("testando").href = res.id;
                mySearchId = $("#testando");
                mySearchId.text(res.id);
                mySearchId.attr('href','/search/'+res.id);
                mySearchId.attr('target','_blank');


                return res.id; //this

            });
        },

        // Execute the payment:
        // 1. Add an onAuthorize callback
        onAuthorize: function(data, actions) {
            // 2. Make a request to your server
            //
            var localhost = window.location.hostname;
            // var EXECUTE_URL = "http://"+localhost+"/PPEC/ExecutePayment.php"; // localhost
            var EXECUTE_URL = "/ec/server/payment/execute"; // cloud9

            // excute a post to the endpoint then SEND the payerId and paymentId FOR
            // the the ExecutePayment method that can use it on accessing the post vars
            return actions.request.post(EXECUTE_URL, {
                    _token: $('meta[name="csrf-token"]').attr('content'), // laravel add
                    paymentID: data.paymentID,
                    payerID: data.payerID
                })
                .then(function(res) {
                    // print the return from the execute payment from the backend method
                    console.log("App.ExecutePaymentResponse");
                    console.log(res);

                    let executePayment = "";

                    function loopingObj(object) {
                        for (key in object) {
                            var value = object[key];
                            if (typeof value === 'object') {
                                // console.log('{');
                                loopingObj(value);
                                // console.log('}');
                            } else {
                                // console.log(key + " : " + value);
                                // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML += "<span class='text-danger'>"+key+"</span> : " + value + "</br>";
                                executePayment += "<span class='text-danger'>" + key + "</span> : " + value + "</br>";

                            }
                        }

                    }

                    // loopingObj(res);
                    // document.getElementById("ExecutePaymentJsonResponseOutput").innerHTML = executePayment;
                    // ExecutePaymentJsonResponseOutputTextArea

                    let executeOutput = JSON.stringify(res, null, '\t')

                    // document.getElementById("CreatePaymentJsonResponseOutput").innerHTML = output;
                    document.getElementById("ExecutePaymentJsonResponseOutputTextArea").value = executeOutput;
                    var textAreaExecute = document.getElementById("ExecutePaymentJsonResponseOutputTextArea");

                    // getting the line numbers from the string
                    var lines = executeOutput.split("\n");
                    textAreaExecute.rows = (lines.length+10);

                    document.getElementById("executePaymentWaitingNotification").innerHTML = "";
                    document.getElementById("executePaymentNotification").innerHTML = "Done";

                    var d = new Date(Date.now());
                    let executeTime = "Resposta gerada às " + d.getHours() +" horas, "+ d.getMinutes()+" minutos e "+d.getSeconds()+" segundos";

                    document.getElementById("executeTimeDiv").innerHTML = executeTime;



                });
        }
    }, '#paypal-button');

    $(".paypal-button-text").hide();

  },
  Json:()=>{
    // let jsonCreatePayment = "{ \n" +
    //     "\"intent\": \"sale\", \n" +
    //     "\"redirect_urls\": {\n" +
    //     "\"return_url\": \"https://example.com/your_redirect_url.html\",\n" +
    //     "\"cancel_url\": \"https://example.com/your_cancel_url.html\"\n" +
    //     "},\n" +
    //     "\"payer\": {\n" +
    //     "\"payment_method\": \"paypal\"\n" +
    //     "},\n" +
    //     "\"transactions\": [{\n" +
    //     "\"amount\": {\n" +
    //     "\"total\": \"7.47\",\n" +
    //     "\"currency\": \"BRL\"\n" +
    //     "}\n" +
    //     "}]\n" +
    //     "}";

    let jsonCreatePayment = {
      intent:"sale",
      redirect_urls:{
        return_url:"https://example.com/your_redirect_url.html",
        cancel_url:"https://example.com/your_cancel_url.html",
      },
      payer:{
        payment_method:"paypal"
      },
      transactions:[{
        amount:{
          total:"7.47",
          currency:"BRL"
        }
      }]
    }
    // jsonCreatePayment = JSON.stringify(jsonCreatePayment);
    let jsonCreatePaymentOuput = JSON.stringify(jsonCreatePayment, undefined, 4)



    document.getElementById("textareaJsonCreatePayment").value = jsonCreatePaymentOuput;
    // console.log(JSON.stringify(jsonCreatePayment));

  }
}
