@extends('layouts.master')

@section('content')

<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />


<div id="planDiv" @if($isActive) hidden @endif>
<div class="container" id="planDiv" @if($isActive) hidden @endif>
    <div class="row">

        <div class="col">
            <!-- paypalbutton -->
            <h1 class="text-center">
                <button class="btn btn-success" id='createPlanBtn' @if($hasPlan) disabled @endif >Create Plan</button><br>
                <!-- <button class="btn btn-default" id='createPlanBtn' >Create Plan</button><br> -->
            </h1>

            <h6>

            </h6>
        </div>
        <div class="col">
            <!-- paypalbutton -->
            <h1 class="text-center">
                <button class="btn btn-success" id='activatePlanBtn' @if((!$hasPlan)||($isActive)) disabled @endif>Activate Plan</button><br>
                <!-- <button class="btn btn-default" id='activatePlanBtn' >Activate Plan</button><br> -->
            </h1>

        </div>

    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col">

            <!--plan div-->

            <div id="accordion">

                <!--Get Token-->
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <!--<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->

                  Get Token
                </button>
                @if(Session::has('expiredTime'))
                  <small> <span class="badge badge-{{Session::get('hasExpired')}} float-right">{{Session::get('expiredTime')}}</span></small>
                @endif

              </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Response</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Create Payment-->
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Create Plan
                </button>
                <span id="createPlanWaitingNotification" class="badge badge-warning float-right">Waiting</span>
                <span id="createPlanNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request (cURL)</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &emsp; curl -v -X POST https://api.sandbox.paypal.com/v1/payments/billing-plans/ \ <br/>
                                        &emsp; -H "Content-Type: application/json" \ <br/>
                                        &emsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &emsp; -d <span class="codeFont">'{ <br/>
                                          &emsp;&emsp;"name": "Plan with Regular and Trial Payment Definitions",</br>
                                          &emsp;&emsp;"description": "Plan with regular and trial payment definitions.",</br>
                                          &emsp;&emsp;"type": "FIXED",</br>
                                          &emsp;&emsp;"payment_definitions": [</br>
                                            &emsp;&emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;&emsp;"name": "Regular payment definition",</br>
                                              &emsp;&emsp;&emsp;&emsp;"type": "REGULAR",</br>
                                              &emsp;&emsp;&emsp;&emsp;"frequency": "MONTH",</br>
                                              &emsp;&emsp;&emsp;&emsp;"frequency_interval": "2",</br>
                                              &emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"value": "100",</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                              &emsp;&emsp;&emsp;&emsp;},</br>
                                              &emsp;&emsp;&emsp;&emsp;"cycles": "12",</br>
                                              &emsp;&emsp;&emsp;&emsp;"charge_models": [</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;{</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"type": "SHIPPING",</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"value": "10",</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;}</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;},</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;{</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"type": "TAX",</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"value": "12",</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;}</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;}</br>
                                              &emsp;&emsp;&emsp;&emsp;]</br>
                                            &emsp;&emsp;&emsp;},</br>
                                            &emsp;&emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;&emsp;"name": "Trial payment definition",</br>
                                              &emsp;&emsp;&emsp;&emsp;"type": "TRIAL",</br>
                                              &emsp;&emsp;&emsp;&emsp;"frequency": "WEEK",</br>
                                              &emsp;&emsp;&emsp;&emsp;"frequency_interval": "5",</br>
                                              &emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"value": "9.19",</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                              &emsp;&emsp;&emsp;&emsp;},</br>
                                              &emsp;&emsp;&emsp;&emsp;"cycles": "2",</br>
                                              &emsp;&emsp;&emsp;&emsp;"charge_models": [</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;{</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"type": "SHIPPING",</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"value": "1",</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;}</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;},</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;{</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"type": "TAX",</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"value": "2",</br>
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;}</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;}</br>
                                              &emsp;&emsp;&emsp;&emsp;]</br>
                                            &emsp;&emsp;&emsp;}</br>
                                          &emsp;&emsp;],</br>
                                          &emsp;&emsp;"merchant_preferences": {</br>
                                            &emsp;&emsp;"setup_fee": {</br>
                                              &emsp;&emsp;&emsp;"value": "1",</br>
                                              &emsp;&emsp;&emsp;"currency": "USD"</br>
                                            &emsp;&emsp;},</br>
                                            &emsp;&emsp;"return_url": "https://example.com",</br>
                                            &emsp;&emsp;"cancel_url": "https://example.com/cancel",</br>
                                            &emsp;&emsp;"auto_bill_amount": "YES",</br>
                                            &emsp;&emsp;"initial_fail_amount_action": "CONTINUE",</br>
                                            &emsp;&emsp;"max_fail_attempts": "0"</br>
                                          &emsp;}</br>
                                        }'</span></br>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>

                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Get token:</b> <a href="#">Link</a></li>
                                  <!-- <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="PPEC/pp-ec-button.png"></img> </li> -->
                                  <li class="list-group-item"><b>Trigger:</b> Plan Button Button  </li>
                                  <li class="list-group-item"><b>Action:</b> Creates a Subscription Plan</li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#define-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#define-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#define-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#define-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#define-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#define-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>

                                  </div>



                            </div>
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="CreatePaymentJsonResponseOutput"></div>-->
                                    <div class="form-group textarea-div">

                                        <textarea id="CreatePlanJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    </div>
                                    <p class="card-text"><small class="text-muted" id="createPlanTimeDiv">Aguardando a requisição</small></p>
                                    <p class="card-text"><small class="text-muted"><a href="#" id="searchIdPlan"></a></small></p>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Execute Payment-->
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Active Plan
                </button>
                <span id="activePlanWaitingNotification" class="badge badge-warning float-right">Waiting</span>

                <span id="activePlanNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-group">

                                                        <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v -X PATCH https://api.sandbox.paypal.com/v1/payments/billing-plans/P-7DC96732KA7763723UOPKETA/ \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'[ <br/>
                                        &emsp;&emsp;&emsp;{</br>
                                        &emsp;&emsp;&emsp;&emsp;"op": "replace",</br>
                                        &emsp;&emsp;&emsp;&emsp;"path": "/",</br>
                                        &emsp;&emsp;&emsp;&emsp;"value": {</br>
                                          &emsp;&emsp;&emsp;&emsp;&emsp;"state": "ACTIVE"</br>
                                        &emsp;&emsp;&emsp;&emsp;}</br>
                                        &emsp;&emsp;&emsp;}</br>
                                        &emsp;&emsp;]'</span></br>

                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>


                                </div>

                                <ul class="list-group list-group-flush">
                                  <!-- <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="PPEC/pp-ec-button.png"></img> </li> -->
                                  <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button </li>
                                  <li class="list-group-item"><b>Action:</b> Calls the incontext PayPal Auth form
                                  <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                      Exibir
                                    </button>
                                  </li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#execute-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#execute-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#execute-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#execute-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#execute-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#execute-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>
                                  </div>
                            </div>


                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="ExecutePaymentJsonResponseOutput"></div>-->
                                    <textarea id="ActivePlanJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    <p class="card-text"><small class="text-muted" id="activePlanTimeDiv">Aguardando a requisição</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- plan div -->


          </div>

      </div>

  </div>
  </div>


<div id="agreementDiv" @if(!$isActive) hidden @endif>

<div class="container">
  <div class="row">
    <div class="col">


            <!-- agreement -->

            <div class="container">
              <div class="row">
                <div class="col">
                    <!-- paypalbutton -->
                    <h1 class="text-center">
                        <!-- <button class="btn btn-success" id='createAgreementBtn' disabled>Create Agreement</button><br> -->
                        <!-- <button class="btn btn-success" id='createAgreementBtn' @if(!$isActive) disabled @endif>Create Agreement</button><br> -->
                        <div @if(!$isActive) disabled @endif id="paypal-button"></div>

                    </h1>
                </div>
                <div class="col">
                    <!-- paypalbutton -->
                    <h1 class="text-center">
                        <!-- <button class="btn btn-success" id='createAgreementBtn' disabled>Create Agreement</button><br> -->
                        <button class="btn btn-danger" id='removePlan'>Remove Plan</button><br>
                    </h1>
                </div>


              </div>
            </div>


            <div id="accordion">

                <!--Get Token-->
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <!--<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->

                            Access Token
                            </button>
                            @if(Session::has('expiredTime'))
                            <small> <span class="badge badge-{{Session::get('hasExpired')}} float-right">{{Session::get('expiredTime')}}</span></small>
                            @endif

                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Response</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Create Agreement-->
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                Create Agreement
                            </button>
                            <span id="createAgreementWaitingNotification" class="badge badge-warning float-right">Waiting</span>
                            <span id="createAgreementNotification" class="badge badge-success float-right"></span>
                            </h5>
                    </div>

                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request (cURL)</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v -X POST https://api.sandbox.paypal.com/v1/payments/billing-agreements/ \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'{ <br/>
                                          &emsp;&emsp;&emsp;"name": "Magazine Subscription",</br>
                                          &emsp;&emsp;&emsp;"description": "Monthly agreement with a regular monthly payment definition and two-month trial payment definition.",</br>
                                          &emsp;&emsp;&emsp;"start_date": "2017-12-22T09:13:49Z",</br>
                                          &emsp;&emsp;&emsp;"plan":</br>
                                          &emsp;&emsp;&emsp;{</br>
                                            &emsp;&emsp;&emsp;&emsp;"id": "P-7DC96732KA7763723UOPKETA"</br>
                                          &emsp;&emsp;&emsp;},</br>
                                          &emsp;&emsp;&emsp;"payer":</br>
                                          &emsp;&emsp;&emsp;{</br>
                                            &emsp;&emsp;&emsp;&emsp;"payment_method": "paypal"</br>
                                          &emsp;&emsp;&emsp;},</br>
                                          &emsp;&emsp;&emsp;"shipping_address":</br>
                                          &emsp;&emsp;&emsp;{</br>
                                            &emsp;&emsp;&emsp;&emsp;"line1": "751235 Stout Drive",</br>
                                            &emsp;&emsp;&emsp;&emsp;"line2": "0976249 Elizabeth Court",</br>
                                            &emsp;&emsp;&emsp;&emsp;"city": "Quimby",</br>
                                            &emsp;&emsp;&emsp;&emsp;"state": "IA",</br>
                                            &emsp;&emsp;&emsp;&emsp;"postal_code": "51049",</br>
                                            &emsp;&emsp;&emsp;&emsp;"country_code": "US"</br>
                                          &emsp;&emsp;&emsp;}</br>
                                          &emsp;&emsp;}'</span></br>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>

                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Get token:</b> <a href="#">Link</a></li>
                                  <!-- <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="PPEC/pp-ec-button.png"></img> </li> -->
                                  <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button  </li>
                                  <li class="list-group-item"><b>Action:</b> Calls the incontext PayPal Auth form
                                  <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                      Exibir
                                    </button>
                                  </li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#define-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#define-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#define-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#define-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#define-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#define-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>

                                  </div>



                            </div>
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="CreatePaymentJsonResponseOutput"></div>-->
                                    <div class="form-group textarea-div">

                                        <textarea id="CreateAgreementJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    </div>
                                    <p class="card-text"><small class="text-muted" id="createAgreementTimeDiv">Aguardando a requisição</small></p>


                                </div>
                            </div>
                        </div>





                    </div>
                </div>
                <!--Create Agreement-->







                <!--Execute Agreement-->
                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                Execute Agreement
                            </button>
                            <span id="executeAgreementWaitingNotification" class="badge badge-warning float-right">Waiting</span>
                            <span id="executeAgreementNotification" class="badge badge-success float-right"></span>
                            </h5>
                    </div>

                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request (cURL)</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v -X POST https://api.sandbox.paypal.com/v1/payments/billing-agreements/ \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'{ <br/>
                                          &emsp;&emsp;&emsp;"name": "Magazine Subscription",</br>
                                          &emsp;&emsp;&emsp;"description": "Monthly agreement with a regular monthly payment definition and two-month trial payment definition.",</br>
                                          &emsp;&emsp;&emsp;"start_date": "2017-12-22T09:13:49Z",</br>
                                          &emsp;&emsp;&emsp;"plan":</br>
                                          &emsp;&emsp;&emsp;{</br>
                                            &emsp;&emsp;&emsp;&emsp;"id": "P-7DC96732KA7763723UOPKETA"</br>
                                          &emsp;&emsp;&emsp;},</br>
                                          &emsp;&emsp;&emsp;"payer":</br>
                                          &emsp;&emsp;&emsp;{</br>
                                            &emsp;&emsp;&emsp;&emsp;"payment_method": "paypal"</br>
                                          &emsp;&emsp;&emsp;},</br>
                                          &emsp;&emsp;&emsp;"shipping_address":</br>
                                          &emsp;&emsp;&emsp;{</br>
                                            &emsp;&emsp;&emsp;&emsp;"line1": "751235 Stout Drive",</br>
                                            &emsp;&emsp;&emsp;&emsp;"line2": "0976249 Elizabeth Court",</br>
                                            &emsp;&emsp;&emsp;&emsp;"city": "Quimby",</br>
                                            &emsp;&emsp;&emsp;&emsp;"state": "IA",</br>
                                            &emsp;&emsp;&emsp;&emsp;"postal_code": "51049",</br>
                                            &emsp;&emsp;&emsp;&emsp;"country_code": "US"</br>
                                          &emsp;&emsp;&emsp;}</br>
                                          &emsp;&emsp;}'</span></br>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>

                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Get token:</b> <a href="#">Link</a></li>
                                  <!-- <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="PPEC/pp-ec-button.png"></img> </li> -->
                                  <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button  </li>
                                  <li class="list-group-item"><b>Action:</b> Calls the incontext PayPal Auth form
                                  <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                      Exibir
                                    </button>
                                  </li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#define-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#define-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#define-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#define-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#define-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#define-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>

                                  </div>



                            </div>
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="CreatePaymentJsonResponseOutput"></div>-->
                                    <div class="form-group textarea-div">

                                        <textarea id="ExecuteAgreementJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    </div>
                                    <p class="card-text"><small class="text-muted" id="executeAgreementTimeDiv">Aguardando a requisição</small></p>


                                </div>
                            </div>
                        </div>





                    </div>
                </div>
                <!--Execute Agreement-->






            </div>
            <!-- agreement -->



          </div>
        </div>
      </div>
</div>


<!--modals-->
<!-- auth form modal -->

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-body">
        <!-- <img src="PPEC/pp-auth-form.png"></img> -->

      </div>
  </div>
</div>



<!-- <script src="PPEC/jquery-3.3.1.min.js"></script> -->
<script src="{{url('js/sub/script.js')}}"></script>

<style type="text/css">
    #CreatePaymentJsonResponseOutput,
    #ExecutePaymentJsonResponseOutput {
        font-size: 10pt;
    }

    textarea {
        resize: none;
        overflow: hidden;
        border: none !important;
        font-size: 7pt !important;
        color: black !important;
    }
    .codeFont{
        color:#02cf92;
    }
    .accessTokenTag{
        color: #f59000;
    }

</style>


<script>
</script>
@endsection
