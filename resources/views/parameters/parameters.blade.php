@extends('layouts.master')

@section('content')


<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">


      <div class="card">
        <div class="card-header">Parameters</div>



        <div class="card-body">

          <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for parameters..." class="form-control">

          <div class="myUL">



            <br>
            <h3>Request</h3>
            <br>

            <div class="newparameter">
              <p id="intent"><b>intent</b> <span class="text-muted">enum</span> <span class="text-warning">required</span></p>
              <p>The payment intent. Value is:</p>
              <ul>
                <li><span class="badge badge-secondary">sale</span>. Makes an immediate payment.</li>
                <li><span class="badge badge-secondary">authorize</span>. Authorizes a payment for capture later.</li>
                <li><span class="badge badge-secondary">order</span>. Creates an order.</li>
              </ul>
              <p>Allowed values: sale, authorize, order.</p>
              <hr>
            </div>

            <div class="newparameter">
              <p id="payer" class="newparameter"><b>payer</b> <span class="text-primary">object</span> <span class="text-warning">required</span></p>
              <p>The source of the funds for this payment. Payment method is PayPal Wallet payment or bank direct debit.</p>
              <hr>
            </div>

            <div class="newparameter">
              <p id="application_context" class="newparameter"><b>application_context</b> <span class="text-primary">object</span>  </p>
              <p>Use the application context resource to customize payment flow experience for your buyers.</p>
              <hr>
            </div>

            <div class="newparameter">
              <p id="transactions"><b>transactions</b> <span class="text-muted">array</span>  (contains the <a href="#transaction"> transaction </a>object)</p>
              <p>An array of payment-related transactions. A transaction defines what the payment is for and who fulfills the payment. For update and execute payment calls, the transactions object accepts the amount object only.</p>
              <hr>
            </div>


            <div class="newparameter">
              <p id="experience_profile_id"><b>experience_profile_id</b> <span class="text-muted">string</span></p>
              <p>The PayPal-generated ID for the merchant's payment experience profile. For information, see create web experience profile.</p>
              <hr>
            </div>

            <div class="newparameter">
              <p id="note_to_payer"><b>note_to_payer</b> <span class="text-muted">string</span></p>
              <p>A free-form field that clients can use to send a note to the payer.</p>
              <p><span class="text-muted">Maximum length: 165.</span></p>
              <hr>
            </div>

            <div class="newparameter">
              <p id="redirect_urls"><b>redirect_urls</b> <span class="text-primary">object</span></p>
              <p>          A set of redirect URLs that you provide for PayPal-based payments.</p>
            </div>

            <br>
            <h3>Response</h3>
            <br>

            <div class="newparameter">
              <p id="id"><b>id</b> <span class="text-muted">string</span></p>
              <p>The ID of the payment.</p>
              <p><span class="text-muted">Read only.</span></p>
              <hr>
            </div>

            <div class="newparameter">
              <p id="payer"><b>payer</b> <span class="text-primary">object</span></p>
              <p>The source of the funds for this payment. Payment method is PayPal Wallet payment or bank direct debit.</p>
              <hr>
            </div>


            <div class="newparameter">

              <p id="state"><b>state</b> <span class="text-muted">enum</span></p>
              <p>The state of the payment, authorization, or order transaction. Value is:</p>
              <ul>
                <li><span class="badge badge-secondary">created</span>. The transaction was successfully created.</li>
                <li><span class="badge badge-secondary">approved</span>. The customer approved the transaction. The state changes from created to approved on generation of the sale_id for sale transactions, authorization_id for authorization transactions, or order_id for order transactions.
                  <li><span class="badge badge-secondary">failed</span>. The transaction request failed.</li>
                </ul>
                <p><span class="text-muted">Read only</span>.</p>
                <p>Possible values: created, approved, failed.</p>
                <hr>
              </div>

              <div class="newparameter">

                <p id="experience_profile_id"><b>experience_profile_id</b> <span class="text-muted">object</span></p>
                <p>The PayPal-generated ID for the merchant's payment experience profile. For information, see create web experience profile.</p>
                <hr>
              </div>

              <div class="newparameter">
                <p id="failure_reason"><b>failure_reason</b> <span class="text-muted">enum</span></p>
                <p>The reason code for a payment failure.</p>
                <p><span class="text-muted">Read only.</span></p>
                <p>Possible values: UNABLE_TO_COMPLETE_TRANSACTION, INVALID_PAYMENT_METHOD, PAYER_CANNOT_PAY, CANNOT_PAY_THIS_PAYEE, REDIRECT_REQUIRED, PAYEE_FILTER_RESTRICTIONS.</p>
                <hr>
              </div>



              <div class="newparameter">
                <p id="create_time"><b>create_time</b> <span class="text-muted">string</span></p>
                <p>The date and time when the payment was created, in Internet date and time format.</p>
                <p><span class="text-muted">Read only.</span></p>
                <hr>
              </div>

              <div class="newparameter">
                <p id="update_time"><b>update_time</b> <span class="text-muted">string</span></p>
                <p>The date and time when the payment was updated, in Internet date and time format.</p>
                <p><span class="text-muted">Read only.</span></p>
              </div>










            </div>












          </div>
        </div>


      </div>
    </div>
  </div>
  <style media="screen">

  html {
    scroll-behavior: smooth;
  }

  </style>

  <script>
  function myFunction() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    // li = ul.getElementsByTagName('li');
    li = document.getElementsByClassName('newparameter');
    // console.log(li)

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      b = li[i].getElementsByTagName("b")[0];
      // console.log(b);
      txtValue = b.textContent || b.innerText;
      // console.log(txtValue);
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }
  </script>


  @endsection
