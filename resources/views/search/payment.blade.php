@extends('layouts.master')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">


            <div class="card">
                <div class="card-header">Details</div>
                <div class="card-body">

                  {!! $res !!}

                </div>
            </div>


        </div>
    </div>
</div>
<style media="screen">
  ul{
    font-size: 12px;
    list-style-type: none;
  }
  li{
    font-size: 12px;
  }
  .valueClass{
    font-size: 10px;
  }
</style>
@endsection
