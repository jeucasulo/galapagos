@extends('layouts.master')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

          @if(session()->has('msg'))
            <div class="alert {{Session::get('alert')}} alert-dismissible fade show" role="alert">
              <!-- <strong>Holy guacamole!</strong>  -->
              {{ Session::get('msg') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @else
          @endif


            <div class="card">
                <div class="card-header">Credentials
                  <span class="float-right">
                    <a href="{{route('credential.create')}}">Adicionar</a>
                  </span>
                </div>



                <div class="card-body">
                  @forelse($credentials as $credential)
                     <div class="text-{{$credential->status === "disabled" ?'danger':'success'}}">
                       <h6><b>{{$credential->app_name}}</b><span class="float-right"><a href="{{route('credential.edit',$credential->id)}}">Edit</a></span></h6>
                     </div>
                     <h6><i>{{$credential->app_desc}}</i></h6>

                     <hr>
                     @empty
                     Nenhuma credencial inserida
                  @endforelse

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
