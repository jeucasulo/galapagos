@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Credentials</div>

                <div class="card-body">
                  <form class="" action="{{route('credential.store')}}" method="post" enctype='multipart/form-data'>
                    {{ csrf_field() }}

                    <input type="text" name="app_name" value="" class="form-control" placeholder="App name">
                    <input type="text" name="app_desc" value="" class="form-control" placeholder="Info">
                    <input type="text" name="client_id" value="" class="form-control" placeholder="Client Id">
                    <input type="text" name="secret" value="" class="form-control" placeholder="Secret">
                    <br>
                    <button name="button" class="btn btn-success">Save</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
