@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Credentials</div>

                <div class="card-body">
                  <form class="" action="{{route('credential.update',$credential->id)}}" method="post" enctype='multipart/form-data'>
                    <input type='hidden' name='_method' value='put'>

                    {{ csrf_field() }}

                    <div class="form-group">
                      <label for="app_name">App name</label>
                      <input type="text" class="form-control" name="app_name" id="app_name" aria-describedby="app_name" value="{{$credential->app_name}}">
                      <small id="app_nameHelp" class="form-text text-muted">App name on PayPal Developer website</small>
                    </div>

                    <div class="form-group">
                      <label for="app_name">Info</label>
                      <input type="text" class="form-control" name="app_desc" id="app_desc" aria-describedby="app_desc" value="{{$credential->app_desc}}">
                      <small id="app_descHelp" class="form-text text-muted">More details to this app</small>
                    </div>

                    <div class="form-group">
                      <label for="client_id">Client id</label>
                      <input type="text" class="form-control" name="client_id" id="client_id" aria-describedby="client_id" value="{{$credential->client_id}}">
                      <small id="client_idHelp" class="form-text text-muted">Client id from the app (sandbox only)</small>
                    </div>

                    <div class="form-group">
                      <label for="secret">Secret</label>
                      <input type="text" class="form-control" name="secret" id="secret" aria-describedby="secret" value="{{$credential->secret}}">
                      <small id="secretHelp" class="form-text text-muted">Secret from the app (sandbox only)</small>
                    </div>

                    <div class="form-group">
                      <label for="status">Status ({{$credential->status}})</label>
                      <select type="text" class="form-control" name="status" id="status" aria-describedby="status">
                        <option value='disabled'>Disabled</option>
                        <option value='enabled' {{($credential->status==='enabled')?'selected':''}}>Enabled</option>
                      </select>
                      <small id="secretHelp" class="form-text text-muted">Activate this credencial so you can call a new Bearer Token</small>
                    </div>


                    <!-- <input type="text" name="app_name" class="form-control" value="{{$credential->app_name}}"> -->
                    <!-- <input type="text" name="app_desc" class="form-control" value="{{$credential->app_desc}}"> -->
                    <!-- <input type="text" name="client_id" class="form-control" value="{{$credential->client_id}}"> -->
                    <!-- <input type="text" name="secret" class="form-control" value="{{$credential->secret}}"> -->
                    <!-- <input type="text" name="status" class="form-control" value="{{$credential->status}}"> -->
                    <br>
                    <button name="button" class="btn btn-success">Update</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
