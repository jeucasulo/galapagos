@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col">

      <!-- <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Profile</li>
        </ol>
      </nav>

      <br> -->

      <div class="card" style="">
        <div class="card-header">

            <a href="/">Home</a></li> <span class="text-muted">/ Profile</span>


        </div>
      </div>
      <br>





      <div class="card" style="">
        <div class="card-header">
          @auth
          {{Auth::user()->name}}
          @endauth
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">{{Auth::user()->email}}</li>
        </ul>
      </div>
      <br>

      <div class="card" style="">
        <div class="card-header">
          Credentials
        </div>
        @if($credential)
        <ul class="list-group list-group-flush">
          <li class="list-group-item">App:  <a href="{{route('credential.edit', $credential->id)}}" target="_blank">{{$credential->app_name}}</a></li>
          <li class="list-group-item">Desc: {{$credential->app_desc}}</li>
          <li class="list-group-item">Created: {{$credential->created_at}}</li>
          <li class="list-group-item">ClientId: {{$credential->client_id}}</li>
          <li class="list-group-item">Secret: {{$credential->secret}}</li>
          <li class="list-group-item" hidden>Id: {{$credential->id}}</li>
        </ul>
        @else
        <ul class="list-group list-group-flush">
          <li class="list-group-item">No <a href="#">credentials</a> found.</li>
        </ul>

        @endif
      </div>
      <br>

      <div class="card" style="">
        <div class="card-header">
          Subscrition Plan
        </div>
        @if($plan)
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Id: <a href="/search/{{$plan->id}}" target="_blank">{{$plan->id}}</a></li>
          <li class="list-group-item">State: {{$plan->state}}</li>
          <li class="list-group-item">Name: {{$plan->name}}</li>
          <li class="list-group-item">Description: {{$plan->description}}</li>
          <li class="list-group-item">Type: {{$plan->type}}</li>
          <li class="list-group-item">Create time: {{$plan->create_time}}</li>
        </ul>
        @else
        <ul class="list-group list-group-flush">
          <li class="list-group-item">No <a href="#" target="_blank">plan</a> found.</li>
        </ul>

        @endif
      </div>
      <br>

      <div class="card" style="">
        <div class="card-header">
          Billing Agreement <small>(Reference Transaction)</small>
        </div>
        @if($baToken)
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Id: <a href="/search/{{$baToken->id}}" target="_blank">{{$baToken->id}}</a></li>
          <li class="list-group-item">State: {{$baToken->state}}</li>
          <li class="list-group-item">Create time: {{$baToken->create_time}}</li>

        </ul>
        @else
        <ul class="list-group list-group-flush">
          <li class="list-group-item">No <a href="#" target="_blank">Billing Agreement</a> found.</li>
        </ul>

        @endif
      </div>
      <br>


      <!-- <div class="card">
      <div class="card-body">
      <h5 class="card-title">Card title</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
    <img src="{{url('/img/paypal-logo.png')}}" class="card-img-top" alt="...">
  </div> -->

  <!--  -->
</div>
</div>
</div>
@endsection
