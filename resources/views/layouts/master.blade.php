<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Bootrap theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/yeti/bootstrap.min.css">

    <!-- Icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">



</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="{{ url('/') }}">
            <b>  {{ config('app.name', 'Laravel') }}</b>
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse ml-auto" id="navbarSupportedContent">



            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="{{route('credential.index')}}">Credentials</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('token.index')}}">Token</a>
              </li>



              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  EC
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href=""><del>EC NVP</del></a>
                  <a class="dropdown-item" href=""><del>EC NVP (Auth/Cap)</del></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{route('ec-rest-client.index')}}">EC Rest Client</a>
                  <a class="dropdown-item" href="{{route('ec-rest-server.index')}}">EC Rest Server</a>
                  <a class="dropdown-item" href="{{route('ec.server.authcap.index')}}">EC Rest Server (Auth/Cap)</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href=""><del>EC Braintree</del></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{route('ec-rest-client-v2.index')}}">EC Rest Client V2</a>
                  <a class="dropdown-item" href="{{route('ec.server.v2.index')}}">EC Rest Server V2</a>
                  <!-- <a class="dropdown-item" href=""><del>EC Rest Server (Auth/Cap)V2</del></a> -->



                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  PPP
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{route('pplus.index')}}">PayPal Plus</a>
                  <a class="dropdown-item" href="{{route('pplus.ctb.index')}}">PayPal Plus CTB</a>
                  <a class="dropdown-item" href="{{route('pplus.ctb.index')}}">PayPal Plus DTB</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Subs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{route('sub.index')}}">Subscripions</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#"><del>Subscripions V2</del></a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  RT
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{route('rt.index')}}">Reference Transaction</a>
                  <a class="dropdown-item" href="{{route('rt.installs.index')}}">Reference Transaction Installments</a>
                  <a class="dropdown-item" href="{{route('rt.ctb.index')}}">Reference Transaction CTB</a>
                  <a class="dropdown-item" href="{{route('rt.dtb.index')}}">Reference Transaction DTB</a>
                  <a class="dropdown-item" href="{{route('rt.ctb.no.cfo.index')}}"><del>Reference Transaction CTB - No CFO</del></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href=""><del>Vault (Braintree)</del></a>
                </div>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="{{route('payouts.index')}}">Payouts <span class="sr-only">(current)</span></a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="{{route('invoicing.index')}}">Invoicing</a>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Webhook
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{route('webhook.index')}}">Webhook(Notifications)</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{route('my.webhook.index')}}">Webhook(My Notifications)</a>
                  <a class="dropdown-item" href="{{route('webhook.create')}}">Settings</a>
                </div>
              </li>


              <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link disabled" href="#">Summary</a>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link disabled" href="#">Fauna</a>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link disabled" href="#">Diagnostics</a>
              </li> -->
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Misc
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="/home/parameters">Parameters</a>
                  <a class="dropdown-item" href="/home/error-messages">Error messages</a>
                  <a class="dropdown-item" href="/home/status-code">HTTP Status code</a>
                  <a class="dropdown-item" href="/coding/index">Coding</a>
                  <a class="dropdown-item" href="/ticket/index"><del>Ticket</del></a>
                                  </div>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="#"><p id='toggleNotes'>Notes</p></a>
              </li>


              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" id="searchIdInput">
                <button class="btn btn-outline-success my-2 my-sm-0" type="button" id="searchBtn">Search</button>
              </form>

            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('profile') }}">
                            Profile
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest


            </ul>

          </div>
          </nav>


          <!-- Draggable DIV -->
            <div id="mydiv">
                <!-- Include a header DIV with the same name as the draggable DIV, followed by "header" -->
                <div id="mydivheader">Click here to move <span class='float-right' id='closeNotes' style="cursor:pointer">X</span></div>
                <!-- <p>Move</p>
                <p>this</p>
                <p>DIV</p> -->

                <textarea name="" id="" cols="30" rows="10" class='form-control'></textarea>
            </div>

            <style>
            #mydiv {
                position: absolute;
                z-index: 9;
                background-color: #f1f1f1;
                border: 1px solid #d3d3d3;
                text-align: center;
                display:none
                }

                #mydivheader {
                padding: 10px;
                cursor: move;
                z-index: 10;
                background-color: black;
                color: #fff;
                }

            </style>




            <script>
            $(document).ready(function() {
                $("#toggleNotes").click(function() {
                    $("#mydiv").toggle();
                });
                $("#closeNotes").click(function() {
                    $("#mydiv").hide();
                });
            });


            // Make the DIV element draggable:
            dragElement(document.getElementById("mydiv"));

            function dragElement(elmnt) {
            var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
            if (document.getElementById(elmnt.id + "header")) {
                // if present, the header is where you move the DIV from:
                document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
            } else {
                // otherwise, move the DIV from anywhere inside the DIV:
                elmnt.onmousedown = dragMouseDown;
            }

            function dragMouseDown(e) {
                e = e || window.event;
                e.preventDefault();
                // get the mouse cursor position at startup:
                pos3 = e.clientX;
                pos4 = e.clientY;
                document.onmouseup = closeDragElement;
                // call a function whenever the cursor moves:
                document.onmousemove = elementDrag;
            }

            function elementDrag(e) {
                e = e || window.event;
                e.preventDefault();
                // calculate the new cursor position:
                pos1 = pos3 - e.clientX;
                pos2 = pos4 - e.clientY;
                pos3 = e.clientX;
                pos4 = e.clientY;
                // set the element's new position:
                elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
            }

            function closeDragElement() {
                // stop moving when mouse button is released:
                document.onmouseup = null;
                document.onmousemove = null;
            }
            }

            </script>
          <!-- Draggable DIV -->



        <main class="py-4">
            @yield('content')
        </main>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript">
        $(document).ready(function(){
        // alert('asdf');
        $("#searchBtn").click(Master.Search);


        });
        var Master = {
          // let searchId = ;
          // var searchId = document.getElementById("searchIdInput").value;
          // var x = document.getElementById("myText").value;


          Search:()=>{
            var searchId = document.getElementById("searchIdInput").value;
            // window.location.href = "/search/PAYID-LYLKYXI45L913318A792993A";
            window.location.href = "/search/"+searchId;
          }
        }

        </script>

    </div>
</body>
</html>
