@extends('layouts.master')

@section('content')


<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">


      <div class="card">
        <div class="card-header">HTTP status codes</div>



        <div class="card-body">

          <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for HTTP status code..." class="form-control">

          <div class="myUL">

<!-- link -->
<!-- https://developer.paypal.com/docs/api/payments/v1/?mark=malfor#error-MALFORMED_REQUEST -->


<!-- link -->
<!-- https://developer.paypal.com/docs/api/reference/api-responses/#http-status-codes -->
            <br>
              <h3>HTTP status codes</h3>
            <br>

            <div class="newerror">
              <p>
                <b>
                  200 OK
                </b>
              </p>
              <p>
                The request succeeded.
              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  201 Created
                </b>
              </p>
              <p>
                A POST method successfully created a resource. If the resource was already created by a previous execution of the same method, for example, the server returns the HTTP 200 OK status code.

              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  202 Accepted
                </b>
              </p>
              <p>
                The server accepted the request and will execute it later.
              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  204 No Content
                </b>
              </p>
              <p>
                The server successfully executed the method but returns no response body.
              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  400 Bad Request
                </b>
              </p>
              <p>
                INVALID_REQUEST. Request is not well-formed, syntactically incorrect, or violates schema.

              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  401 Unauthorized
                </b>
              </p>
              <p>
                AUTHENTICATION_FAILURE. Authentication failed due to invalid authentication credentials.

              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  403 Forbidden
                </b>
              </p>
              <p>
                NOT_AUTHORIZED. Authorization failed due to insufficient permissions.

              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  404 Not Found
                </b>
              </p>
              <p>
                RESOURCE_NOT_FOUND. The specified resource does not exist.


              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>
                  405 Method Not Allowed
                </b>
              </p>
              <p>
                METHOD_NOT_SUPPORTED. The server does not implement the requested HTTP method.
              </p>
              <hr>
            </div>


          </div>












          </div>
        </div>


      </div>
    </div>
  </div>
  <style media="screen">

  html {
    scroll-behavior: smooth;
  }

  </style>

  <script>
  function myFunction() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    // li = ul.getElementsByTagName('li');
    li = document.getElementsByClassName('newerror');
    // console.log(li)

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      b = li[i].getElementsByTagName("b")[0];
      // console.log(b);
      txtValue = b.textContent || b.innerText;
      // console.log(txtValue);
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }
  </script>


  @endsection
