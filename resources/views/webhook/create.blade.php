@extends('layouts.master')

@section('content')


<div class="container">
  <div class="row">
    <div class="col">
      <h2>Webhook</h2>
      <h6>Configure webhooks to notify your app when certain events occur. To configure a webhook, define your webhook listener URL and a list of events for which to listen. You can configure up to ten webhooks. Each webhook can subscribe to either specific events or all events. To learn more about webhooks, see webhooks notifications.</h6>
    </div>
  </div>
</div>
<hr>
<div class="container">
  <div class="row">
    <div class="col">
      <h2>App Webhooks <i> <span class="text-success">({{$app_name}})</span></i></h2>
      <br>
      <div class="">

        @forelse($webhooks['webhooks'] as $wh)

        <div class="card" style="">
          <div class="card-body">
            <h5 class="card-title"><a href="/search/{{$wh['id']}}" target="_blank">{{$wh['id']}}</a></h5>
            <!-- <h5 class="card-title">{{$wh['id']}}</h5> -->
            <h6 class="card-subtitle mb-2 text-muted">{{$wh['url']}}</h6>
            <p class="card-text">
              @forelse($wh['event_types'] as $events)

              <span class="badge badge-light">{{$events['name']}}</span>

              @empty
              Nothing...
              @endforelse
            </p>
            <a href="#" class="card-link">Edit</a>
            <a href="#" class="card-link text-danger">Delete</a>
          </div>
        </div>




        <!-- <p><b></b></p> -->
        <!-- <p><i></i></p> -->
        <!-- <p> -->

        <!-- </p> -->
        <br>
        @empty

        Nenhum webhook encontrado...
        @endforelse

      </div>
    </div>
  </div>
</div>
<hr>
<div class="container">
  <div class="row">
    <div class="col">
      <h1 class='title'>Create a new webhook</h1>
    </div>
  </div>
</div>
<br>
<div class="container" id="eventsDiv">
  <div class="row">
    <div class="col" >

      <label><input type="checkbox" value="all-events" id="checkAll">ALL EVENTS</label></br>
      <label><input type="checkbox" value="PAYMENT.AUTHORIZATION.CREATED" name="events">PAYMENT.AUTHORIZATION.CREATED</label></br>
      <label><input type="checkbox" value="PAYMENT.AUTHORIZATION.VOIDED" name="events">PAYMENT.AUTHORIZATION.VOIDED</label></br>
      <label><input type="checkbox" value="PAYMENT.CAPTURE.COMPLETED" name="events">PAYMENT.CAPTURE.COMPLETED</label></br>
      <label><input type="checkbox" value="PAYMENT.CAPTURE.DENIED" name="events">PAYMENT.CAPTURE.DENIED</label></br>
      <label><input type="checkbox" value="PAYMENT.CAPTURE.PENDING" name="events">PAYMENT.CAPTURE.PENDING</label></br>
      <label><input type="checkbox" value="PAYMENT.CAPTURE.REFUNDED" name="events">PAYMENT.CAPTURE.REFUNDED</label></br>
      <label><input type="checkbox" value="PAYMENT.CAPTURE.REVERSED" name="events">PAYMENT.CAPTURE.REVERSED</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTSBATCH.DENIED" name="events">PAYMENT.PAYOUTSBATCH.DENIED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTSBATCH.PROCESSING" name="events">PAYMENT.PAYOUTSBATCH.PROCESSING</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTSBATCH.SUCCESS" name="events">PAYMENT.PAYOUTSBATCH.SUCCESS	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.BLOCKED" name="events">PAYMENT.PAYOUTS-ITEM.BLOCKED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.CANCELED" name="events">PAYMENT.PAYOUTS-ITEM.CANCELED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.DENIED" name="events">PAYMENT.PAYOUTS-ITEM.DENIED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.FAILED" name="events">PAYMENT.PAYOUTS-ITEM.FAILED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.HELD" name="events">PAYMENT.PAYOUTS-ITEM.HELD</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.REFUNDED" name="events">PAYMENT.PAYOUTS-ITEM.REFUNDED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.RETURNED" name="events">PAYMENT.PAYOUTS-ITEM.RETURNED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.SUCCEEDED" name="events">PAYMENT.PAYOUTS-ITEM.SUCCEEDED	</label></br>
      <label><input type="checkbox" value="PAYMENT.PAYOUTS-ITEM.UNCLAIMED" name="events">PAYMENT.PAYOUTS-ITEM.UNCLAIMED	</label></br>
      <label><input type="checkbox" value="VAULT.CREDIT-CARD.CREATED" name="events">VAULT.CREDIT-CARD.CREATED	</label></br>
      <label><input type="checkbox" value="VAULT.CREDIT-CARD.DELETED" name="events">VAULT.CREDIT-CARD.DELETED	</label></br>
      <label><input type="checkbox" value="VAULT.CREDIT-CARD.UPDATED" name="events">VAULT.CREDIT-CARD.UPDATED	</label></br>

    </div>

    <div class="col" >

      <label><input type="checkbox" value="BILLING_AGREEMENTS.AGREEMENT.CREATED" name="events">BILLING_AGREEMENTS.AGREEMENT.CREATED	</label></br>
      <label><input type="checkbox" value="BILLING_AGREEMENTS.AGREEMENT.CANCELLED" name="events">BILLING_AGREEMENTS.AGREEMENT.CANCELLED</label></br>
      <label><input type="checkbox" value="BILLING.PLAN.CREATED" name="events">BILLING.PLAN.CREATED	</label></br>
      <label><input type="checkbox" value="BILLING.PLAN.UPDATED" name="events">BILLING.PLAN.UPDATED	</label></br>
      <label><input type="checkbox" value="BILLING.SUBSCRIPTION.CANCELLED" name="events">BILLING.SUBSCRIPTION.CANCELLED</label></br>
      <label><input type="checkbox" value="BILLING.SUBSCRIPTION.CREATED" name="events">BILLING.SUBSCRIPTION.CREATED	</label></br>
      <label><input type="checkbox" value="BILLING.SUBSCRIPTION.RE-ACTIVATED" name="events">BILLING.SUBSCRIPTION.RE-ACTIVATED</label></br>
      <label><input type="checkbox" value="BILLING.SUBSCRIPTION.SUSPENDED" name="events">BILLING.SUBSCRIPTION.SUSPENDED</label></br>
      <label><input type="checkbox" value="BILLING.SUBSCRIPTION.UPDATED" name="events">BILLING.SUBSCRIPTION.UPDATED	</label></br>
      <label><input type="checkbox" value="IDENTITY.AUTHORIZATION-CONSENT.REVOKED	" name="events">IDENTITY.AUTHORIZATION-CONSENT.REVOKED	</label></br>
      <label><input type="checkbox" value="PAYMENTS.PAYMENT.CREATED" name="events">PAYMENTS.PAYMENT.CREATED	</label></br>
      <label><input type="checkbox" value="CHECKOUT.ORDER.APPROVED" name="events">CHECKOUT.ORDER.APPROVED	</label></br>
      <label><input type="checkbox" value="CUSTOMER.DISPUTE.CREATED" name="events">CUSTOMER.DISPUTE.CREATED</label></br>
      <label><input type="checkbox" value="CUSTOMER.DISPUTE.RESOLVED" name="events">CUSTOMER.DISPUTE.RESOLVED</label></br>
      <label><input type="checkbox" value="CUSTOMER.DISPUTE.UPDATED" name="events">CUSTOMER.DISPUTE.UPDATED</label></br>
      <label><input type="checkbox" value="RISK.DISPUTE.CREATED" name="events">RISK.DISPUTE.CREATED</label></br>
      <label><input type="checkbox" value="INVOICING.INVOICE.CANCELLED" name="events">INVOICING.INVOICE.CANCELLED</label></br>
      <label><input type="checkbox" value="INVOICING.INVOICE.CREATED" name="events">INVOICING.INVOICE.CREATED</label></br>
      <label><input type="checkbox" value="INVOICING.INVOICE.PAID" name="events">INVOICING.INVOICE.PAID</label></br>
      <label><input type="checkbox" value="INVOICING.INVOICE.REFUNDED" name="events">INVOICING.INVOICE.REFUNDED</label></br>
      <label><input type="checkbox" value="INVOICING.INVOICE.SCHEDULED" name="events">INVOICING.INVOICE.SCHEDULED</label></br>
      <label><input type="checkbox" value="INVOICING.INVOICE.UPDATED" name="events">INVOICING.INVOICE.UPDATED</label></br>

    </div>

    <div class="col">

      <label><input type="checkbox" value="MERCHANT.ONBOARDING.COMPLETED	" name="events">MERCHANT.ONBOARDING.COMPLETED	</label></br>
      <label><input type="checkbox" value="MERCHANT.PARTNER-CONSENT.REVOKED" name="events">MERCHANT.PARTNER-CONSENT.REVOKED</label></br>
      <label><input type="checkbox" value="CHECKOUT.ORDER.PROCESSED" name="events">CHECKOUT.ORDER.PROCESSED	</label></br>
      <label><input type="checkbox" value="CUSTOMER.ACCOUNT-LIMITATION.ADDED" name="events">CUSTOMER.ACCOUNT-LIMITATION.ADDED</label></br>
      <label><input type="checkbox" value="CUSTOMER.ACCOUNT-LIMITATION.ESCALATED" name="events">CUSTOMER.ACCOUNT-LIMITATION.ESCALATED</label></br>
      <label><input type="checkbox" value="CUSTOMER.ACCOUNT-LIMITATION.LIFTED" name="events">CUSTOMER.ACCOUNT-LIMITATION.LIFTED</label></br>
      <label><input type="checkbox" value="CUSTOMER.ACCOUNT-LIMITATION.UPDATED" name="events">CUSTOMER.ACCOUNT-LIMITATION.UPDATED</label></br>
      <label><input type="checkbox" value="PAYMENT.REFERENCED-PAYOUT-ITEM.COMPLETED" name="events">PAYMENT.REFERENCED-PAYOUT-ITEM.COMPLETED</label></br>
      <label><input type="checkbox" value="PAYMENT.REFERENCED-PAYOUT-ITEM.FAILED" name="events">PAYMENT.REFERENCED-PAYOUT-ITEM.FAILED</label></br>
      <label><input type="checkbox" value="PAYMENT.ORDER.CANCELLED" name="events">PAYMENT.ORDER.CANCELLED	</label></br>
      <label><input type="checkbox" value="PAYMENT.ORDER.CREATED" name="events">PAYMENT.ORDER.CREATED	</label></br>
      <label><input type="checkbox" value="PAYMENT.SALE.COMPLETED" name="events">PAYMENT.SALE.COMPLETED</label></br>
      <label><input type="checkbox" value="PAYMENT.SALE.DENIED" name="events">PAYMENT.SALE.DENIED</label></br>
      <label><input type="checkbox" value="PAYMENT.SALE.PENDING" name="events">PAYMENT.SALE.PENDING</label></br>
      <label><input type="checkbox" value="PAYMENT.SALE.REFUNDED" name="events">PAYMENT.SALE.REFUNDED</label></br>
      <label><input type="checkbox" value="PAYMENT.SALE.REVERSED" name="events">PAYMENT.SALE.REVERSED	</label></br>


    </div>


  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col">
      <button type="button" name="button" class="btn btn-primary" id="getEvents">Save</button>
    </div>
  </div>
</div>

<script src="{{url('js/webhook/script.js')}}"></script>
<!-- <script src="{{url('js/sub/ec-button.js')}}"></script> -->






<style type="text/css">
#eventsDiv{
  font-size: 10px;
}
#CreatePaymentJsonResponseOutput,
#ExecutePaymentJsonResponseOutput {
  font-size: 8px;
}

textarea {
  resize: none;
  overflow: hidden;
  border: none !important;
  font-size: 7pt !important;
  color: black !important;
}
.codeFont{
  color:#02cf92;
}
.accessTokenTag{
  color: #f59000;
}

</style>

@endsection
