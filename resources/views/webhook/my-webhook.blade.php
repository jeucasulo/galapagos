@extends('layouts.master')

@section('content')


<div class="container">
  <div class="row">

    <div class="col-2">


      Total: {{$webhooksCount}}


    </div>
    <div class="col-10">

<a href="{{route('credential.index')}}" target="_blank">
{{$credential->app_name}} | {{$credential->app_desc}}
</a>


    </div>
  </div>
</div>

<hr>

<div class="container">





  @forelse($webhooks as $webhook)

  <div class="row">


      <div class="col">
        <a href="{{route('webhook.details',$webhook->id)}}" target="_blank">{{$webhook->webhook}}</a>
      </div>
      <!-- <div class="col">{{$webhook->id}}</div> -->
      <!-- <div class="col">{{$webhook->body}}</div> -->
      <!-- <div class="col">{{$webhook->header}}</div> -->
      <div class="col">{{$webhook->event}}</div>
      <div class="col">{{$webhook->created_at}}</div>

    <!-- <div class="col">
      {{$webhook->webhook}}
    </div>
    <div class="col">
      {{$webhook->id}}
    </div>
    <div class="col">
      {{$webhook->body}}
    </div>
    <div class="col">
      {{$webhook->header}}
    </div>
    <div class="col">
      {{$webhook->event}}
    </div>
    <div class="col">
      {{$webhook->credential_id}}
    </div> -->

  </div>


  <hr>

  @empty
  No webhooks found
  @endforelse


</div>
<!--modals-->



<!-- <script src="{{url('js/payouts/script.js')}}"></script> -->


<!--<div id="containerControllers">-->

<!--</div>-->



<style type="text/css">
#CreatePaymentJsonResponseOutput,
#ExecutePaymentJsonResponseOutput {
  font-size: 10pt;
}

textarea {
  resize: none;
  overflow: hidden;
  border: none !important;
  font-size: 7pt !important;
  color: black !important;
}
.codeFont{
  color:#02cf92;
}
.accessTokenTag{
  color: #f59000;
}

</style>

@endsection
