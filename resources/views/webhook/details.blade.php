@extends('layouts.master')

@section('content')


<div class="container">
  <div class="row">

    <div class="col">



      <p>{{$webhook->event}}</p>
      <p>{{$webhook->created_at}}</p>
      <hr>
      <p><b>Headers</b></p>
      <p>{{$webhook->header}}</p>
      <hr>
      <p><b>Body</b></p>
      <p>{{$webhook->body}}</p>
      <p><b>Credentials</b></p>
      <p>{{$webhook->credential_id}}</p>

    </div>
  </div>
</div>






<style type="text/css">
#CreatePaymentJsonResponseOutput,
#ExecutePaymentJsonResponseOutput {
  font-size: 10pt;
}

textarea {
  resize: none;
  overflow: hidden;
  border: none !important;
  font-size: 7pt !important;
  color: black !important;
}
.codeFont{
  color:#02cf92;
}
.accessTokenTag{
  color: #f59000;
}

</style>

@endsection
