@extends('layouts.master')
@section('content')

<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Coding</h1>
            <br>

            <div id='ajax'>
            <h4>Ajax</h4>

                <div class="row">
                    <div class="col" id="ajaxLeft">
                    <br>
                    <form>
                        <div class="form-group">
                            <label for="ajaxURL">Ajax URL</label>
                            <input type="email" class="form-control" id="ajaxURLChange" aria-describedby="ajaxURLHelp" placeholder="Enter ajax URL">
                            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Method</label>
                                    <select class="form-control" id="ajaxTypeChange">
                                    <option>POST</option>
                                    <option>GET</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Data Type</label>
                                    <select class="form-control" id="ajaxDataTypeChange">
                                        <option>json</option>
                                        <option><s>jsonp</s></option>
                                        <option><del>xml</del></option>
                                        <option><del>html</del></option>
                                        <option><del>text</del></option>
                                        <option><del>script</del></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Data field</label>
                                <div class="row">
                                    <div class="col">
                                        <label for="">Input 1</label>
                                        <input type="text" class="form-control" id="input1change" placeholder="Input 1">
                                    </div>
                                    <div class="col">
                                        <label for="">Value 1</label>
                                        <input type="text" class="form-control" id="value1change" placeholder="Value 1">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="">Input 2</label>
                                        <input type="text" class="form-control" id="input2change" placeholder="Input 2">
                                    </div>
                                    <div class="col">
                                        <label for="">Value 2</label>
                                        <input type="text" class="form-control" id="value2change" placeholder="Value 2">
                                    </div>
                                </div>

                        </div>

                        <!--
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        -->
                    </form>



                    </div>

                    <div class="col" id='ajaxRight'>
                    <br>
                    Code
                    <div id="codeBlock">
                        &lt;script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">&lt;/script><br>

                        $(<span class='orangeCode'>document</span>).ready(<span class='blueCode'>function</span>(){<br>
                            $.ajax({<br>
                            &emsp;&emsp;type: <span class="stringCode"><span id='ajaxType'>"POST"</span></span>,<br>
                            &emsp;&emsp;url: <span class="stringCode"><span id="ajaxURL">"https://api.ipify.org?format=json<span id='getData'>?<span class="input1get">input1</span>=<span class='value1get'>value1</span>&<span class="input2get">input2</span>=<span class='value2get'>value2</span></span>"</span></span>,<br>
                            <div id='postData'>
                            &emsp;&emsp;data: {<br>
                                &emsp;&emsp;&emsp;&emsp;<span class="stringCode"><span class='input1'>"input1"</span>:<span class='value1'>"value1"</span></span>,<br>
                                &emsp;&emsp;&emsp;&emsp;<span class="stringCode"><span class='input2'>"input2"</span>:<span class='value2'>"value2"</span></span><br>
                            &emsp;&emsp;},<br>
                            </div>
                            &emsp;&emsp;dataType: <span id='ajaxDataType'>"json"</span><br>
                        })
                        .done(function(response) {<br>
                            &emsp;&emsp;<span class='blueCode'>alert</span>( <span class="stringCode">"success"</span> );<br>
                            &emsp;&emsp;<span class='orangeCode'>console</span>.log(response);<br>
                        })<br>
                        .fail(<span class='blueCode'>function</span> (jqXHR, exception) {<br>
                            &emsp;&emsp;var msg_err = "";<br>
                            &emsp;&emsp;<span class='blueCode'>if</span> (jqXHR.status === 0) {<br>
                                &emsp;&emsp;&emsp;&emsp;msg_err = <span class="stringCode">"Not connect. Verify Network."</span>;<br>
                            &emsp;&emsp;} <span class='blueCode'>else if</span> (jqXHR.status == 404) {<br>
                                &emsp;&emsp;&emsp;&emsp;msg_err = <span class="stringCode">"Requested page not found. [404]"</span>;<br>
                            &emsp;&emsp;} <span class='blueCode'>else if</span> (jqXHR.status == 500) {<br>
                                &emsp;&emsp;&emsp;&emsp;msg_err = <span class="stringCode">"Internal Server Error [500]."</span>;<br>
                            &emsp;&emsp;} <span class='blueCode'>else if</span> (exception === <span class="stringCode">"parsererror"</span>) {<br>
                                &emsp;&emsp;&emsp;&emsp;msg_err = "Requested JSON parse failed."</span>;<br>
                            &emsp;&emsp;} <span class='blueCode'>else if</span> (exception === <span class="stringCode">"timeout"</span>) {<br>
                                &emsp;&emsp;&emsp;&emsp;msg_err = <span class="stringCode">"Time out error."</span>;<br>
                            &emsp;&emsp;} <span class='blueCode'>else if</span> (exception === <span class="stringCode">"abort"</span>) {<br>
                                &emsp;&emsp;&emsp;&emsp;msg_err = <span class="stringCode">"Ajax request aborted."</span>;<br>
                            &emsp;&emsp;} <span class='blueCode'>else</span> {<br>
                                &emsp;&emsp;&emsp;&emsp;msg_err = <span class="stringCode">"Uncaught Error. "</span>+ jqXHR.responseText;<br>
                            &emsp;&emsp;}<br>
                            &emsp;&emsp;<span class='blueCode'>alert</span>(msg_err);<br>
                        })<br>
                        .always(<span class='blueCode'>function</span>() {<br>
                            &emsp;&emsp;<span class='blueCode'>alert</span>(<span class="stringCode">"complete"</span>);<br>
                        });<br>
                        });<br>

                    </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>







<style type="text/css">
    #CreatePaymentJsonResponseOutput,
    #ExecutePaymentJsonResponseOutput {
        font-size: 10pt;
    }

    textarea {
        resize: none;
        overflow: hidden;
        border: none !important;
        font-size: 7pt !important;
        color: black !important;
    }
    .codeFont{
        color:#02cf92;
    }
    .accessTokenTag{
        color: #f59000;
    }
    /*#paypal-button {*/
    /*  content: 'whatever it is you want to add';*/
    /*}*/
    /*#paypal-animation-content > div.paypal-button-container.paypal-button-layout-horizontal.paypal-button-shape-pill.paypal-button-branding-branded.paypal-button-number-single.paypal-button-env-sandbox.paypal-should-focus > div.paypal-button.paypal-button-number-0.paypal-button-layout-horizontal.paypal-button-shape-pill.paypal-button-branding-branded.paypal-button-number-single.paypal-button-env-sandbox.paypal-should-focus.paypal-button-label-checkout.paypal-button-color-gold.paypal-button-logo-color-blue > span{*/
    /*  content: 'test' !important;*/
    /*}*/
    #codeBlock{
        margin-top:8px;
        background-color:#272822;
        color: #fefefe;
        font-size:11px;
    }
    #getData{
        display:none
    }
    .blueCode{
        color:#66D9EF;
        font-style: italic;
    }
    .stringCode{
        color:#DADB74
    }
    .orangeCode{
        color:#D19345
    }

</style>

<script>
$(document).ready(function(){
    $("#ajaxURLChange").on('change',function(){
        $("#ajaxURL").html('"'+this.value+'"');
    })

    $("#ajaxTypeChange").on('change', function(){
        $("#ajaxType").html('"'+this.value+'"');
        // if(this.value==='GET'){
        //     $("#postData").hide();
        //     $("#getData").show();
        // }else{
        //     $("#postData").show();
        //     $("#getData").hide();
        // }
        $("#postData").toggle();
        $("#getData").toggle();
    })

    $("#ajaxDataTypeChange").on('change', function(){
        $("#ajaxDataType").html('"'+this.value+'"');
    })

    $("#input1change").on('change',function(){
        $(".input1").html('"'+this.value+'"');
        $(".input1get").html(this.value);
    })
    $("#input2change").on('change',function(){
        $(".input2").html('"'+this.value+'"');
        $(".input2get").html(this.value);
    })
    $("#value1change").on('change',function(){
        $(".value1").html('"'+this.value+'"');
        $(".value1get").html(this.value);
    })
    $("#value2change").on('change',function(){
        $(".value2").html('"'+this.value+'"');
        $(".value2get").html(this.value);
    })


})
</script>

@endsection
