@extends('layouts.master')

@section('content')

<!--https://developer.paypal.com/docs/payouts/-->
<!--https://developer.paypal.com/docs/payouts/integrate/api-integration/-->
<div class="container">
    <div class="row">

        <div class="col">
            <!-- paypalbutton -->
            <!--<h1 class="text-center"><div id="paypal-button"></div></h1>-->
            <!-- <h6>Payouts</h6> -->
            <button id="executePayoutsBtn" class="btn btn-success">Execute Payouts</button>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col">

            <!--HERE-->

            <div id="accordion">

                <!--Get Token-->
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <!--<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->

                  Access Token
                </button>
                @if(Session::has('expiredTime'))
                  <small> <span class="badge badge-{{Session::get('hasExpired')}} float-right">{{Session::get('expiredTime')}}</span></small>
                @endif
              </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Response</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Execute Payouts-->
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Execute Payouts
                </button>
                <span id="payoutsExecuteWaitingNotification" class="badge badge-warning float-right">Waiting</span>
                <span id="payoutsExecuteNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request (cURL)</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v https://api.sandbox.paypal.com/v1/payments/payouts \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'{ <br/>
                                          &emsp;&emsp;"sender_batch_header": {</br>
                                            &emsp;&emsp;&emsp;"sender_batch_id": "Payouts_2018_100007",</br>
                                            &emsp;&emsp;&emsp;"email_subject": "You have a payout!",</br>
                                            &emsp;&emsp;&emsp;"email_message": "You have received a payout! Thanks for using our service!"</br>
                                          &emsp;&emsp;},</br>
                                          &emsp;&emsp;"items": [</br>
                                            &emsp;&emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;&emsp;"recipient_type": "EMAIL",</br>
                                              &emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"value": "9.87",</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                              &emsp;&emsp;&emsp;&emsp;},</br>
                                              &emsp;&emsp;&emsp;&emsp;"note": "Thanks for your patronage!",</br>
                                              &emsp;&emsp;&emsp;&emsp;"sender_item_id": "201403140001",</br>
                                              &emsp;&emsp;&emsp;&emsp;"receiver": "receiver@example.com",</br>
                                              &emsp;&emsp;&emsp;&emsp;"alternate_notification_method": {</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"phone": {</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"country_code": "91",</br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"national_number": "9999988888"</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;}</br>
                                              &emsp;&emsp;&emsp;&emsp;}</br>
                                            &emsp;&emsp;&emsp;},</br>
                                            &emsp;&emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;&emsp;"recipient_type": "PHONE",</br>
                                              &emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"value": "112.34",</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                              &emsp;&emsp;&emsp;&emsp;},</br>
                                              &emsp;&emsp;&emsp;&emsp;"note": "Thanks for your support!",</br>
                                              &emsp;&emsp;&emsp;&emsp;"sender_item_id": "201403140002",</br>
                                              &emsp;&emsp;&emsp;&emsp;"receiver": "91-734-234-1234"</br>
                                            &emsp;&emsp;&emsp;},</br>
                                            &emsp;&emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;&emsp;"recipient_type": "PAYPAL_ID",</br>
                                              &emsp;&emsp;&emsp;&emsp;"amount": {</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"value": "5.32",</br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;"currency": "USD"</br>
                                              &emsp;&emsp;&emsp;&emsp;},</br>
                                              &emsp;&emsp;&emsp;&emsp;"note": "Thanks for your patronage!",</br>
                                              &emsp;&emsp;&emsp;&emsp;"sender_item_id": "201403140003",</br>
                                              &emsp;&emsp;&emsp;&emsp;"receiver": "G83JXTJ5EHCQ2"</br>
                                            &emsp;&emsp;&emsp;}</br>
                                          &emsp;&emsp;]</br>
                                        &nbsp; }' </span><br/>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>

                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Get token:</b> <a href="#">Link</a></li>
                                  <!-- <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="PPEC/pp-ec-button.png"></img> </li> -->
                                  <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button  </li>
                                  <li class="list-group-item"><b>Action:</b> Calls the incontext PayPal Auth form
                                  <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                      Exibir
                                    </button>
                                  </li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#define-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#define-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#define-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#define-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#define-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#define-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>

                                  </div>



                            </div>
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="CreatePaymentJsonResponseOutput"></div>-->
                                    <div class="form-group textarea-div">

                                        <textarea id="PayoutsExecuteJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    </div>
                                    <p class="card-text"><small class="text-muted" id="payoutsTimeDiv">Aguardando a requisição</small></p>
                                    <p class="card-text"><small class="text-muted"><a href="#" id="searchIdPayouts"></a></small></p>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <!--HERE-->

        </div>

    </div>

</div>

<!--modals-->
<!-- auth form modal -->

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-body">
        <!-- <img src="PPEC/pp-auth-form.png"></img> -->

      </div>
  </div>
</div>
<!--modals-->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-body">
        <!-- <img src="PPEC/pp-execute-btn.png"></img> -->

      </div>
  </div>
</div>
<!--modals-->



<script src="{{url('js/payouts/script.js')}}"></script>


<!--<div id="containerControllers">-->

<!--</div>-->



<style type="text/css">
    #CreatePaymentJsonResponseOutput,
    #ExecutePaymentJsonResponseOutput {
        font-size: 10pt;
    }

    textarea {
        resize: none;
        overflow: hidden;
        border: none !important;
        font-size: 7pt !important;
        color: black !important;
    }
    .codeFont{
        color:#02cf92;
    }
    .accessTokenTag{
        color: #f59000;
    }

</style>

@endsection
