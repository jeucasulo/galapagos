@extends('layouts.master')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

          @if(session()->has('msg'))
            <div class="alert {{Session::get('alert')}} alert-dismissible fade show" role="alert">
              <!-- <strong>Holy guacamole!</strong>  -->
              {{ Session::get('msg') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @else
          @endif


            <div class="card">
              <div class="card-header">V2</div>
                <div class="card-body">
                  <!-- Set up a container element for the button -->
                  <div id="paypal-button-container"></div>

                  <!-- src="https://www.paypal.com/sdk/js?client-id=SB_CLIENT_ID" -->
                  <!-- Include the PayPal JavaScript SDK -->


                  <script
                    src="https://www.paypal.com/sdk/js?client-id={{$clientId}}&currency=BRL">
                  </script>

                    <!-- AdYLZtwY8zHLgVLR7uawFMLHXWT-jswUL0jnyZJAIfjjYzsWfR9mxHhKQaAcDR409oZmujTDAh207JJI -->


                  <script src="{{url('js/ec-rest-client-v2.js')}}"></script>


                  <script>
                  </script>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
