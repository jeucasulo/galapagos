@extends('layouts.master')
@section('content')
<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

<div class="container">
    <div class="row">

        <div class="col" style="width:1000px">
            <!-- paypalbutton -->
            <h1 class="text-center"><div id="paypal-button"></div></h1>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col">

            <!--HERE-->

            <div id="accordion">

                <!--Get Token-->
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <!--<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->

                  Access Token
                </button>
                @if(Session::has('expiredTime'))
                  <small> <span class="badge badge-{{Session::get('hasExpired')}} float-right">{{Session::get('expiredTime')}}</span></small>
                @endif

              </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Response</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <!--Create Button-->
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneButtonCreate" aria-expanded="true" aria-controls="collapseOne">
                  Create Button (Smart Payment Button)
                </button>
                <span id="createButtonWaitingNotification" class="badge badge-success float-right">Done</span>
                <span id="createButtonNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseOneButtonCreate" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">HTML & JS (FrontEnd)</h5>

                                      <p class="card-text bg-dark text-light">

                                        <span class="text-info">&lt;</span><span class="text-danger">script</span> src=<span class="text-success">"https://www.paypalobjects.com/api/checkout.js"</span><span class="text-info">&gt;</span><span class="text-info">&lt;/</span><span class="text-danger">script</span><span class="text-info">&gt;</span> </br>
                                        <span class="text-info">&lt;</span><span class="text-danger">div</span> id=<span class="text-success">"paypal-button"</span><span class="text-info">&gt;</span><span class="text-info">&lt;/</span><span class="text-danger">div</span><span class="text-info">&gt;</span> </br>
                                        <span class="text-info">&lt;</span><span class="text-danger">script</span><span class="text-info">&gt;</span> </br>
                                          &emsp;paypal.Button.render({ </br>
                                            &emsp;&emsp;env: <span class="text-success">'sandbox'</span>, // Or 'production' </br>
                                            &emsp;&emsp;// Set up the payment: </br>
                                            &emsp;&emsp;// 1. Add a payment callback </br>
                                            &emsp;&emsp;payment: <span class="text-info">function</span>(<span class="text-warning">data</span>, <span class="text-warning">actions</span>) { </br>
                                              &emsp;&emsp;&emsp;// 2. Make a request to your server </br>
                                              &emsp;&emsp;&emsp;<span class="text-info">return</span> actions.request.post(<span class="text-success">'/my-api/create-payment/'</span>) </br>
                                                &emsp;&emsp;&emsp;&emsp;.then(<span class="text-info">function</span>(<span class="text-warning">res</span>) { </br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;// 3. return res.id from the response </br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;<span class="text-info">return</span> res.id; </br>
                                                &emsp;&emsp;&emsp;&emsp;}); </br>
                                            &emsp;&emsp;&emsp;}, </br>
                                            &emsp;&emsp;&emsp;// Execute the payment: </br>
                                            &emsp;&emsp;&emsp;// 1. Add an onAuthorize callback </br>
                                            &emsp;&emsp;&emsp;onAuthorize: <span class="text-info">function</span>(<span class="text-warning">data</span>, <span class="text-warning">actions</span>) { </br>
                                              &emsp;&emsp;&emsp;&emsp;// 2. Make a request to your server </br>
                                              &emsp;&emsp;&emsp;&emsp;<span class="text-info">return</span> actions.request.post(<span class="text-success">'/my-api/execute-payment/'</span>, { </br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;paymentID: data.paymentID, </br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;payerID:   data.payerID </br>
                                              &emsp;&emsp;&emsp;&emsp;}) </br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;.then(<span class="text-info">function</span>(<span class="text-warning">res</span>) { </br>
                                                  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;// 3. Show the buyer a confirmation message. </br>
                                                &emsp;&emsp;&emsp;&emsp;&emsp;}); </br>
                                            &emsp;&emsp;&emsp;&emsp;} </br>
                                          &emsp;&emsp;&emsp;}, '#paypal-button'); </br>
                                        <span class="text-info">&lt;/</span><span class="text-danger">script</span><span class="text-info">&gt;</span> </br>


                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>

                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Smart Payment Button V1:</b> <a href="https://developer.paypal.com/docs/archive/checkout/how-to/server-integration/">Link</a></li>
                                  <li class="list-group-item"><b>Action:</b> Creates the PayPal Checkout Button <img src="{{url('img/pp-ec-button.png')}}"></img> </li>
                                  <!-- <li class="list-group-item"><b>Action:</b> Calls the incontext PayPal Auth form -->
                                  <!-- Button trigger modal -->
                                    <!-- <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                      Exibir
                                    </button> -->
                                  <!-- </li> -->
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank" data-toggle="modal" data-target=".bd-example-modal-xl">See all</a></li>
                                  <!-- <li class="list-group-item"><b>Source:</b> createPayment( ) function</li> -->
                                </ul>




                            </div>
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">PayPal Smart Payment Button</h5>

                                    <br>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="CreatePaymentJsonResponseOutput"></div>-->
                                    <div class="form-group textarea-div">

                                        <!-- <textarea id="CreatePaymentJsonResponseOutputTextArea" class="form-control" readonly> </textarea> -->
                                        </br>
                                        <h5>1. Set up your client to call your server</h5>
                                        In this step, modify your existing client-side code to call your server to set up and execute the payment.
                                        </br>
                                        </br>
                                        </br>
                                        <h5>Set up the payment</h5>
                                        To set up your payment, follow these steps along with the corresponding comments in the example code.

                                        <ol>
                                          <li>Add a payment callback function to the button, which is called when a buyer clicks the button.</li>
                                          <li>In the payment callback function, call actions.request.post() with your server URL.</li>
                                          <li>Return res.id from the response with the Payment ID returned from your server.</li>
                                        </ol>

                                        </br>
                                        </br>
                                        <h5> Execute the payment</h5>
                                        To execute the payment, follow these steps with the corresponding comments in the example code.
                                        <ol>
                                        <li>Add an onAuthorize callback function to the button, which is called after the buyer authorizes the payment on PayPal.</li>
                                        <li>In the onAuthorize callback, call actions.request.post() with your server URL, and pass the data.paymentID and data.payerID parameters to the server.</li>
                                        <li>Show the buyer a confirmation message.</li>
                                        </ol>

                                    </div>
                                    <!-- <p class="card-text"><small class="text-muted" id="createTimeDiv">Aguardando a requisição</small></p> -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>




                <!--Create Auth-->
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Create Auth (Buyer)
                </button>
                <span id="createPaymentWaitingNotification" class="badge badge-warning float-right">Waiting</span>
                <span id="createPaymentNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request (cURL)</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v https:  //api.sandbox.paypal.com/v1/payments/payment \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'{ <br/>
                                        &nbsp; &emsp; "intent": "authorize", <br/>
                                        &nbsp; &emsp; "redirect_urls": { <br/>
                                        &nbsp; &emsp;&emsp;    "return_url": "https://example.com/your_redirect_url.html", <br/>
                                        &nbsp; &emsp;&emsp;    "cancel_url": "https://example.com/your_cancel_url.html" <br/>
                                        &nbsp;   }, <br/>
                                        &nbsp; &emsp; "payer": { <br/>
                                        &nbsp; &emsp;&emsp; "payment_method": "paypal" <br/>
                                        &nbsp;   }, <br/>
                                        &nbsp; &emsp; "transactions": [{ <br/>
                                        &nbsp; &emsp;&emsp; "amount": { <br/>
                                        &nbsp; &emsp;&emsp;&emsp;      "total": "7.47", <br/>
                                        &nbsp; &emsp;&emsp;&emsp;      "currency": "BRL" <br/>
                                        &nbsp; &emsp;&emsp;    } <br/>
                                        &nbsp; &emsp;}] <br/>
                                        &nbsp; }' </span><br/>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>

                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Get token:</b> <a href="#">Link</a></li>
                                  <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="{{url('img/ec-rest/pp-ec-button.png')}}/"></img> </li>
                                  <li class="list-group-item"><b>Action:</b> Calls the incontext PayPal Auth form
                                  <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                      Exibir
                                    </button>
                                  </li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank" data-toggle="modal" data-target=".bd-example-modal-xl">See all</a></li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#define-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#define-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#define-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#define-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#define-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#define-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>

                                  </div>



                            </div>
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="CreatePaymentJsonResponseOutput"></div>-->
                                    <div class="form-group textarea-div">

                                        <textarea id="CreatePaymentJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    </div>
                                    <p class="card-text"><small class="text-muted" id="createTimeDiv">Aguardando a requisição</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Confirm Authorization-->
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Confirm Authorization (Buyer)
                </button>
                <span id="executePaymentWaitingNotification" class="badge badge-warning float-right">Waiting</span>

                <span id="executePaymentNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-group">

                                                        <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v https://api.sandbox.paypal.com/v1/payments/payment/PAY-9N9834337A9191208KOZOQWI/execute \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'{ <br/>
                                        &emsp; "payer_id": "CR87QHB7JTRSC" <br/>
                                        &nbsp; }' </span><br/>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>


                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="{{url('img/ec-rest/pp-execute-btn - bt.png')}}"></img> </li>
                                  <li class="list-group-item"><b>Action:</b> Execute the payment
                                  <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter2">
                                      Exibir
                                    </button>
                                  </li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#execute-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#execute-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#execute-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#execute-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#execute-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#execute-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>
                                  </div>
                            </div>


                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="ExecutePaymentJsonResponseOutput"></div>-->
                                    <textarea id="ExecutePaymentJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    <p class="card-text"><small class="text-muted" id="executeTimeDiv">Aguardando a requisição</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <!--Execute Capture-->
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  Execute Capture  (Seller)
                </button>
                <span id="executeCaptureWaitingNotification" class="badge badge-warning float-right">Waiting</span>

                <span id="executeCaptureNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-group">

                              <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">
                                      Request
                                      <span class="float-right">
                                        <button id="executeCapBtn" type="button" name="button" class="btn btn-success btn-sm">Capture</button>
                                      </span>
                                    </h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v https://api.sandbox.paypal.com/v1/payments/authorization/9T287484DP554682S/capture \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'{ <br/>
                                        &emsp; "amount": { <br/>
                                        &emsp;&emsp;     "currency": "USD", <br/>
                                        &emsp;&emsp;     "total": "4.54", <br/>
                                        &nbsp; }' <br/>
                                        &nbsp;  "is_final_capture": true <br/>
                                        &nbsp; }' </span><br/>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>


                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Trigger:</b> Capture Button <img width="70%" src="{{url('img/ec-authcap/ExecuteCaptureBtn.png')}}"></img> </li>
                                  <li class="list-group-item"><b>Action:</b> Uses the AuthorizationId to capture the Authorized Payment</li>
                                  <li class="list-group-item"><b>Source:</b> executeCap( ) function</li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/integration/direct/payments/authorize-and-capture-payments/#capture-an-authorized-payment/" target="_blank">See all</a></li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#execute-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#execute-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#execute-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#execute-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#execute-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#execute-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>
                                  </div>
                            </div>


                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="ExecutePaymentJsonResponseOutput"></div>-->
                                    <textarea id="ExecuteCaptureJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    <p class="card-text"><small class="text-muted" id="executeCapTimeDiv">Aguardando a requisição</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!--HERE-->

        </div>

    </div>

</div>

<!--modals-->
<!-- auth form modal -->

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-body">
        <img src="{{url('img/ec-rest/pp-auth-form.png')}}"></img>
      </div>
  </div>
</div>
<!--modals-->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-body">
        <img src="{{url('/img/ec-rest/pp-execute-btn.png')}}"/>
      </div>
  </div>
</div>
<!--modals-->




<!-- parameters modal -->
<!-- Extra large modal -->

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Parameter</th>
              <th scope="col">Description</th>
              <th scope="col">Value</th>
              <th scope="col">Type</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row"><span class="badge badge-secondary">intent</span></th>
              <td>The payment intent. Value is:</td>
              <td>
                    <span class="badge badge-secondary">sale</span> Makes an immediate payment.<br>
                    <span class="badge badge-secondary">authorize</span> Authorizes a payment for capture later.<br>
                    <span class="badge badge-secondary">order</span> Creates an order.<br>
              </td>
              <td><span class="text-primary">enum</span> <span class="text-danger">required</span></td>
            </tr>

            <tr>
              <th scope="row"><span class="badge badge-secondary">payer</span></th>
              <td>The source of the funds for this payment. Payment method is PayPal Wallet payment or bank direct debit.</td>
              <td>
                    <span class="badge badge-secondary">"payment_method": "paypal"</span><br>
              </td>
              <td><span class="text-primary">object</span> <span class="text-danger">required</span></td>
            </tr>

            <tr>
              <th scope="row"><span class="badge badge-secondary">transactions</span></th>
              <td>An array of payment-related transactions. A transaction defines what the payment is for and who fulfills the payment. For update and execute payment calls, the transactions object accepts the <i>amount</i> object only.</td>
              <td>
                    <span class="badge badge-secondary">amount</span><br>
                    <span class="badge badge-secondary">description</span><br>
                    <span class="badge badge-secondary">custom</span><br>
                    <span class="badge badge-secondary">invoice_number</span><br>
                    <span class="badge badge-secondary">payment_options</span><br>
                    <span class="badge badge-secondary">soft_descriptor</span><br>
                    <span class="badge badge-secondary">item_list</span><br>
              </td>
              <td><span class="text-primary">array</span> <span class="text-muted"></span></td>
            </tr>

            <tr>
              <th scope="row"><span class="badge badge-secondary">experience_profile_id </span></th>
              <td>An array of payment-related transactions. A transaction defines what the payment is for and who fulfills the payment. For update and execute payment calls, the transactions object accepts the <i>amount</i> object only.</td>
              <td>
                    <span class="badge badge-secondary">&lt;experience_profile_id&gt;	</span><br>
              </td>
              <td><span class="text-primary">string</span></td>
            </tr>

            <tr>
              <th scope="row"><span class="badge badge-secondary">note_to_payer  </span></th>
              <td>A free-form field that clients can use to send a note to the payer. <b>Maximum length: 165</b>.</td>
              <td>
                    <span class="badge badge-secondary">"note_to_payer": "Contact us for any questions on your order."</span><br>
              </td>
              <td><span class="text-primary">string</span></td>
            </tr>

            <tr>
              <th scope="row"><span class="badge badge-secondary">redirect_urls</span></th>
              <td>A set of redirect URLs that you provide for PayPal-based payments.</td>
              <td>
                    <span class="badge badge-secondary">return_url</span><br>
                    <span class="badge badge-secondary">cancel_url</span><br>
              </td>
              <td><span class="text-primary">object</span></td>
            </tr>



          </tbody>
        </table>


    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col">
      <!-- <h2 id="payIdToCap">30M40379LN442013L</h2> -->
      <h2 id="payIdToCap"></h2>
    </div>
    <div class="col">
      <!-- <button id="executeCapBtn" type="button" name="button" class="btn btn-success btn-block">Capture</button> -->
    </div>
  </div>
</div>
<!-- 1-https://developer.paypal.com/docs/integration/direct/payments/authorize-and-capture-payments/#integration-steps <br>
2-https://developer.paypal.com/docs/integration/direct/payments/authorize-and-capture-payments/#authorize-the-payment <br>
3-https://developer.paypal.com/docs/integration/direct/payments/authorize-and-capture-payments/#capture-an-authorized-payment <br> -->

<!-- <script src="PPEC/jquery-3.3.1.min.js"></script> -->
<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<!--<div id="containerControllers">-->

<!--</div>-->

<script src="{{url('js/ec-rest-server-authcap.js')}}"></script>

<style type="text/css">
    #CreatePaymentJsonResponseOutput,
    #ExecutePaymentJsonResponseOutput {
        font-size: 10pt;
    }

    textarea {
        resize: none;
        overflow: hidden;
        border: none !important;
        font-size: 7pt !important;
        color: black !important;
    }
    .codeFont{
        color:#02cf92;
    }
    .accessTokenTag{
        color: #f59000;
    }
    /*#paypal-button {*/
    /*  content: 'whatever it is you want to add';*/
    /*}*/
    /*#paypal-animation-content > div.paypal-button-container.paypal-button-layout-horizontal.paypal-button-shape-pill.paypal-button-branding-branded.paypal-button-number-single.paypal-button-env-sandbox.paypal-should-focus > div.paypal-button.paypal-button-number-0.paypal-button-layout-horizontal.paypal-button-shape-pill.paypal-button-branding-branded.paypal-button-number-single.paypal-button-env-sandbox.paypal-should-focus.paypal-button-label-checkout.paypal-button-color-gold.paypal-button-logo-color-blue > span{*/
    /*  content: 'test' !important;*/
    /*}*/

</style>


@endsection
