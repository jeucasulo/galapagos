@extends('layouts.master')

@section('content')

<div class="container border">

  <div class="row border chooseRow" id="scheduleRow"><span data-toggle="popoverSchedule" data-placement="left" data-content="Selecione uma das opções."></span>
    <div class="col-2">
      Atendimento
    </div>
    <div class="col-5" data-container="body">
      <!-- <input type="radio" name="choseSchedule" value="Hotline">Hotline &emsp;
      <input type="radio" name="choseSchedule" value="Line Express">Line Express &emsp;
      <input type="radio" name="choseSchedule" value="Regular">Regular -->

      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline1" name="choseSchedule" class="custom-control-input" value="Hotline">
        <label class="custom-control-label" for="customRadioInline1">Hotline</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline2" name="choseSchedule" class="custom-control-input" value="Line Express">
        <label class="custom-control-label" for="customRadioInline2">Line Express</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline3" name="choseSchedule" class="custom-control-input" value="Regular">
        <label class="custom-control-label" for="customRadioInline3">Regular</label>
      </div>


    </div>
    <div class="col-4" id="chosedSchedule">
      Hotline / Line Express / Regular
    </div>
    <div class="col-1">
      <a href="#" id="" disabled class="text-success">&#x274C;</a>
      <a href="#" id="finishSchedule" disabled class="text-success">&#x2714;</a>
    </div>
  </div>

  <div class="row border chooseRow disablebDiv" id="platformRow"><span data-toggle="popoverPlatform" data-placement="left" data-content="Selecione uma das opções."></span>
    <div class="col-2">
      Plataforma
    </div>
    <div class="col-5">
      <select class="custom-select custom-select-sm" name="" id="choosePlatform">
        <option value="none">Selecione uma plataforma</option>
        <option value="Loja Integrada">Loja Integrada</option>
        <option value="Shopify">Shopify</option>
        <option value="Magento">Magento</option>
      </select>
    </div>
    <div class="col-4" id="chosedPlatform">
      Aguardando
    </div>
    <div class="col-1">
      <a href="#" id="closePlatform" disabled class="text-success">&#x274C;</a>
      <a href="#" id="finishPlatform" disabled class="text-success">&#x2714;</a>
    </div>
  </div>

  <div class="row border chooseRow disablebDiv" id="productRow">
    <div class="col-2">
      Produtos
    </div>
    <div class="col-5">
      <!-- <input type="radio" name="chooseProduct[]" value="Já possui EC" >Já possui EC&emsp; -->
      <!-- <input type="radio" name="chooseProduct[]" value="Já possui RT">Já possui RT&emsp; -->
      <!-- <input type="radio" name="chooseProduct[]" value="EC" checked>EC&emsp; -->
      <!-- <input type="radio" name="chooseProduct[]" value="RT">RT&emsp; -->

      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="productCustomRadioInline1" name="chooseProduct[]" class="custom-control-input" value="Já possui EC">
        <label class="custom-control-label" for="productCustomRadioInline1">Já possui EC</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="productCustomRadioInline2" name="chooseProduct[]" class="custom-control-input" value="Já possui RT">
        <label class="custom-control-label" for="productCustomRadioInline2">Já possui RT</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="productCustomRadioInline3" name="chooseProduct[]" class="custom-control-input" value="EC" checked>
        <label class="custom-control-label" for="productCustomRadioInline3">EC</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="productCustomRadioInline4" name="chooseProduct[]" class="custom-control-input" value="RT">
        <label class="custom-control-label" for="productCustomRadioInline4">RT</label>
      </div>

      <br>
      <div class="custom-control custom-check custom-control-inline">
        <input type="checkbox" id="productCustomRadioInline7" name="chooseProduct[]" class="custom-control-input" value="Payouts">
        <label class="custom-control-label" for="productCustomRadioInline7">Payouts</label>
      </div>
      <div class="custom-control custom-check custom-control-inline">
        <input type="checkbox" id="productCustomRadioInline8" name="chooseProduct[]" class="custom-control-input" value="Invoicing">
        <label class="custom-control-label" for="productCustomRadioInline8">Invoicing</label>
      </div>
      <div class="custom-control custom-check custom-control-inline">
        <input type="checkbox" id="productCustomRadioInline5" name="chooseProduct[]" class="custom-control-input" value="PP+">
        <label class="custom-control-label" for="productCustomRadioInline5">PP+</label>
      </div>
      <div class="custom-control custom-check custom-control-inline">
        <input type="checkbox" id="productCustomRadioInline6" name="chooseProduct[]" class="custom-control-input" value="Sub">
        <label class="custom-control-label" for="productCustomRadioInline6">Sub</label>
      </div>

      <!-- <input type="checkbox" name="chooseProduct[]" value="EC" checked disabled>EC -->
      <!-- &nbsp; -->
      <!-- <input type="checkbox" name="chooseProduct[]" value="PP+">PP+ -->
      <!-- &nbsp; -->
      <!-- <input type="checkbox" name="chooseProduct[]" value="RT">RT -->
      <!-- &nbsp; -->
      <!-- <input type="checkbox" name="chooseProduct[]" value="Sub">Sub -->
      <!-- &nbsp; -->
      <!-- <input type="checkbox" name="chooseProduct[]" value="Payouts">Payouts -->
      <!-- &nbsp; -->
      <!-- <input type="checkbox" name="chooseProduct[]" value="Invoicing">Invoicing -->



    </div>
    <div class="col-4" id="chosedProduct">
      EC / PP+ / Sub / RT / Payouts / Invoicing
    </div>
    <div class="col-1">
      <a href="#" id="closeProduct" disabled class="text-success">&#x274C;</a>
      <a href="#" id="finishProduct" disabled class="text-success">&#x2714;</a>
    </div>
  </div>

  <div class="row border chooseRow disablebDiv" id="dateRow">
    <div class="col-2">
      Data/Horário
    </div>
    <div class="col-5">
      <input type="date" value="2013-01-08" id="chooseDateInput" required="required" a="">
      <input type="time" value="10:00" id="chooseTimeInput" step="600" min="10:00" max="18:00">
      <input type="text" id="chooseDateTimeInputDetails" placeholder="Ex: Preferência após dia 20 / Preferência a tarde" class="invisible form-control">
    </div>
    <div class="col-4" >
      <span id="chosedDate">Aguardando data</span>
      <span>
        &emsp;
      </span>
      <span id="chosedTime">

      </span>
    </div>
    <div class="col-1">
      <a href="#" id="closeDate" disabled class="text-success">&#x274C;</a>
      <a href="#" id="finishDate" disabled class="text-success">&#x2714;</a>
    </div>
  </div>


  <div class="row border chooseRow disablebDiv" id="contactRow">
    <div class="col-2">
      Contato
    </div>
    <div class="col-5">
      <input type="text" id="chooseContactName" placeholder="Nome (press tab)" class="form-control">
      <input type="text" id="chooseContactEmail" placeholder="Email (press tab)" class="form-control">
      <input type="text" id="chooseContactNumber" placeholder="Tel (press tab)" class="form-control">
    </div>
    <div class="col-4" id="chosedContact">
      &lt; Contato &gt;
    </div>
    <div class="col-1">
      <a href="#" id="closeContact" disabled class="text-success">&#x274C;</a>
      <a href="#" id="finishContact" disabled class="text-success">&#x2714;</a>
    </div>
  </div>

</div>
<br>
<div class="container">
  <div class="row">
    <div class="col">
      <!-- Green -->
      <div class="progress">
        <!-- <div id="progressBar" class="progress-bar bg-success" style="width:0%"></div> -->
        <div id="progressBar" class="progress-bar progress-bar-striped progress-bar-animated bg-success"></div>

      </div>

      <span id="progressBarMsg"></span>

    </div>

  </div>

</div>

<br>
<br>
<br>
<br>


<div class="container">
  <div class="row">
    <div class="col">

      <div class="card text-center">
        <div class="card-header">
          Em Status generator
        </div>

        <div class="card-body">
          <div class="row">

            <!-- <div class="col">
            <table class='table table-bordered text-left'>
            <tr>
            <td class='col-choose-one'>Atendimento agendado?</td>
            <td>
            &emsp;
            &emsp;
            <input type="radio" name="scheduling" value="yes">Sim
            &emsp;
            <input type="radio" name="scheduling" value="no">Não
          </td>
          <td class='justify-content' align="center">
          <a href="#" id="schedulingDone" disabled class="text-success">&#x2714;</a>
          <button href="#" id="schedulingDone" disabled>↓</button>
          <button href="#" id="schedulingDone" class="btn btn-sm btn-success" disabled>↓</button>

        </td>
      </tr>
      <tr id="platformChoose">
      <td>Plataforma</td>
      <td>
      <small>
      <select id="getPlatform" class="" name="">
      <optgroup>
      <option value="">Escolher plataforma</option>
      <option value="">Loja Integrada</option>
      <option value="">Shopify</option>
      <option value="">Xtech</option>
      <option value="">VTEX</option>
    </optgroup>
    <optgroup>
    <option value="">Magento</option>
    <option value="">Plataforma própria</option>
  </optgroup>
</select>
</small>
</td>
</tr>

<tr id="productsChoose">
<td>Produtos</td>
<td>
<input type="checkbox" name="products[]" value="EC" checked disabled>EC
&nbsp;
<input type="checkbox" name="products[]" value="PP+">PP+
&nbsp;
<input type="checkbox" name="products[]" value="RT">RT
&nbsp;
<input type="checkbox" name="products[]" value="Sub">Sub
&nbsp;
<input type="checkbox" name="products[]" value="Payouts">Payouts
&nbsp;
<input type="checkbox" name="products[]" value="Invoicing">Invoicing
</td>
</tr>

<tr id="dateChoose">
<td>Data</td>
<td>Date: <input type="date" value="2013-01-08" id='getDate' onkeydown="return false" required="required"></td>
<td class='' align="">
<a href="#">↑</a>
<a href="#" id="schedulingDone" disabled class="text-success">&#x2714;</a>
<a href="#">↓</a>
</td>
</tr>
</table>

<p></p>
</div> -->


<div class="col">
  <table class='table table-bordered text-left'>
    <tr id='typeLine'>
      <td class='customIndex'>
        <b>Atendimento agendado?<b>
        </td>
        <td>
          <input type="radio" name="scheduling" value="yes">Sim
          &emsp;
          <input type="radio" name="scheduling" value="no">Não
        </td>
        <td id='type' class='text-left'>
          <span>Hotline / Line Express / Regular</span>
          <span class="float-right">
            <a href="#" id="schedulingDone" disabled class="text-success">&#x2714;</a>
            <!-- <a href="#" id="schedulingDone" disabled class="text-success">&#x274C;</a> -->
          </span>
        </td>
        <!-- <td class='justify-content' align="center">

      </td> -->

    </tr>

    <tr id='plaftormLine'>
      <td class='customIndex'>
        <b>Plataforma:<b>
        </td>
        <td>
          <small>
            <select id="getPlatform" class="" name="">
              <optgroup>
                <option value="">Escolher plataforma</option>
                <option value="">Loja Integrada</option>
                <option value="">Shopify</option>
                <option value="">Xtech</option>
                <option value="">VTEX</option>
              </optgroup>
              <optgroup>
                <option value="">Magento</option>
                <option value="">Plataforma própria</option>
              </optgroup>
            </select>
          </small>
        </td>
        <td id='platform' class='text-left'>
          <span>
            &lt;plataforma&gt;
          </span>
          <span class="float-right">
            <a href="#" id="" disabled class="text-success">&#x274C;</a>
            <a href="#" id="" disabled class="text-success">&#x2714;</a>
          </span>
        </td>
      </tr>

      <tr id='productLine'>
        <td class='customIndex'>
          <b>Produtos:<b>
          </td>
          <td>
            <input type="checkbox" name="products[]" value="EC" checked disabled>EC
            &nbsp;
            <input type="checkbox" name="products[]" value="PP+">PP+
            &nbsp;
            <input type="checkbox" name="products[]" value="RT">RT
            &nbsp;
            <input type="checkbox" name="products[]" value="Sub">Sub
            &nbsp;
            <input type="checkbox" name="products[]" value="Payouts">Payouts
            &nbsp;
            <input type="checkbox" name="products[]" value="Invoicing">Invoicing
          </td>
          <td id='product' class='text-left'>
            <span>
              EC / PP+ / Sub / RT / Payouts / Invoicing
            </span>
            <span class="float-right">
              <a href="#" id="" disabled class="text-success">&#x274C;</a>
              <a href="#" id="" disabled class="text-success">&#x2714;</a>
            </span>
          </td>
        </tr>

        <tr id='dateLine'>
          <td class='customIndex'>
            <b>Data:<b>
            </td>
            <td>
              <span>Data: <input type="date" value="2013-01-08" id='getDate' onkeydown="return false" required="required"></span>
              <span>Horário<input type="time" name="eta" step="10"></span>
            </td>
            <td id='date' class='text-left'>
              <span>
                dd/mm/aaaa h:m
              </span>
              <span class="float-right">
                <a href="#" id="" disabled class="text-success">&#x274C;</a>
                <a href="#" id="" disabled class="text-success">&#x2714;</a>
              </span>
            </td>
          </tr>
          <tr id='engineerLine'>
            <td class='customIndex'>
              <b>Responsável:<b>
              </td>
              <td id='engineer' class='text-left'>

              </td>
              <td>
                <span>
                  Renan / Francisco / Jeú
                </span>
                <span class="float-right">
                  <a href="#" id="" disabled class="text-success">&#x274C;</a>
                  <a href="#" id="" disabled class="text-success">&#x2714;</a>
                </span>

              </td>
            </tr>

            <tr>
              <td>
                <b>Contato técnico</b>
              </td>
              <td>
              </td>
              <td>
              </td>
            </tr>
          </table>
        </div>


      </div>
    </div>
    <div class="card-footer text-muted">
      6 more to go
    </div>
  </div>


</div>
</div>
</div>

<hr>
<p>• Tipo do atendimento: (Regular/Line Express/Atritado/Prioritário)</p>
<p>•	Nome do Contato Técnico:</p>
<p>•	Email do Contato Técnico:</p>
<p>•	Telefone do Contato Técnico: (ex.: Skype, Hangouts, outro)</p>
<p>•	URL do Site:</p>
<p>•	Plataforma de E-commerce:</p>
<p>•	Horário / Período de Contato de Preferência:</p>
<p>•	Data de agendamento: (caso for um Line Express ou escalonamento)</p>
<p>•	Produtos:</p>

<hr>


<style media="screen">
.customIndex{
  width: 200px;
}
input[type=date]::-webkit-inner-spin-button,
input[type=date]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
.col-choose-one{
  width: 180px;
}
.disablebDiv {
  pointer-events: none;
  opacity: 0.4;
}


</style>

<script type="text/javascript">
$(document).ready(function(){
  Ticket.Init();
  // $("[data-toggle=popover]").popover();

})
var Ticket = {
  Init:()=>{
    console.log('App.Init');

    // date
    var today = new Date();
    document.getElementById("chooseDateInput").defaultValue = today.toISOString().substr(0, 10);
    // date

    Ticket.ProgressBar(0);
    // Ticket.ChoseSchedule();
    // $("#platformChoose, #productsChoose").hide();
    // $("input[name=scheduling]").change(Ticket.Scheduling);
    Ticket.OpenSchedule();

    // $("#schedulingDone").click(Ticket.Scheduling);
    // $("#getPlatform").change(Ticket.Platform);
    $("#choosePlatform").change(Ticket.ChoosePlatform);
    $("#finishPlatform").click(Ticket.FinishPlatform);
    $("#closePlatform").click(Ticket.ClosePlatform);


    Ticket.ChooseProduct();
    $("#finishProduct").click(Ticket.FinishProduct);

    $("#closeProduct").click(Ticket.CloseProduct);

    $("#closeDate").click(Ticket.CloseDate);
    $("#finishDate").click(Ticket.FinishDate);


    $("#closeProduct").click(Ticket.CloseProduct);
    $("#finishProduct").click(Ticket.FinishProduct);
    // Ticket.Products();
    $("#closeContact").click(Ticket.CloseContact);
    // Ticket.GetDate();
    // $("#datepicker").datepicker();




  },
  OpenSchedule:()=>{
    $("#scheduleRow").removeClass('disablebDiv');
    $("#scheduleRow").removeClass('border-success');
    // $("#scheduleRow").addClass('border-success');
    $("#platformRow").removeClass('border-success');
    $("#platformRow").removeClass('border-danger');
    $("#platformRow").addClass('disablebDiv');

    $("input[name=choseSchedule]").change(Ticket.ChoseSchedule);
    $("#finishSchedule").click(Ticket.FinishSchedule);
    Ticket.ProgressBar(0);

    // $("#scheduleRow").addClass('disablebDiv');
    // $("[data-toggle=popoverSchedule]").popover('hide');
  },
  ChoseSchedule:()=>{
    // let chosedSchedule = $('#chosedSchedule').val();
    let chosedSchedule = $('input[name=choseSchedule]:checked').val();
    console.log(chosedSchedule);
    $("#chosedSchedule").html("<b>"+chosedSchedule+"</b>");

  },
  FinishSchedule:()=>{

    let chosedSchedule = $('input[name=choseSchedule]:checked').val();
    console.log(chosedSchedule);
    if(chosedSchedule===undefined){
      $("#scheduleRow").removeClass('border-success');
      $("#scheduleRow").addClass('border-danger');
      $("[data-toggle=popoverSchedule]").popover('show');
    }else{
      $("#scheduleRow").removeClass('border-danger');
      $("#scheduleRow").addClass('border-success');
      $("#platformRow").removeClass('disablebDiv');
      $("#scheduleRow").addClass('disablebDiv');
      $("[data-toggle=popoverSchedule]").popover('hide');
      Ticket.ProgressBar(1);
    }
  },
  ChoosePlatform:()=>{
    let platform = $('#choosePlatform option:selected').text();    // let platform = $('#choosePlatform option:selected').val();
    $("#chosedPlatform").html("<b>"+platform+"</b>");
    console.log(platform);
  },
  FinishPlatform:()=>{
    console.log('App.FinishPlatform');
    let platform = $('#choosePlatform option:selected').text();
    if(platform==='Selecione uma plataforma'){
      $("#platformRow").removeClass('border-success');
      $("#platformRow").addClass('border-danger');
      $("[data-toggle=popoverPlatform]").popover('show');
    }else{
      $("#platformRow").removeClass('border-danger');
      $("#platformRow").addClass('border-success');
      $("#productRow").removeClass('disablebDiv');
      $("#platformRow").addClass('disablebDiv');
      $("[data-toggle=popoverPlatform]").popover('hide');
      Ticket.ProgressBar(2);

    }
  },
  ClosePlatform:()=>{
    Ticket.OpenSchedule();
  },
  OpenPlatform:()=>{
    $("#platformRow").removeClass('disablebDiv');
    $("#platformRow").removeClass('border-success');
    // $("#scheduleRow").addClass('border-success');

    // $("#platformRow").removeClass('border-success');
    // $("#platformRow").removeClass('border-danger');
    $("#productRow").addClass('disablebDiv');
  },
  OpenProduct:()=>{
    $("#productRow").removeClass('disablebDiv');
    $("#productRow").removeClass('border-success');
    // $("#scheduleRow").addClass('border-success');

    // $("#platformRow").removeClass('border-success');
    // $("#platformRow").removeClass('border-danger');
    $("#dateRow").addClass('disablebDiv');
  },

  ChooseProduct:()=>{
    var checked = [];
    $("input[name='chooseProduct[]']:checked").each(function ()
    {
      checked.push($(this).val());
    });
    $('#chosedProduct').html("<b>"+checked.join(' | ')+"</b>");

    // $('input[name="products[]"]').click(function(){
    $('input[name="chooseProduct[]"]').click(function(){
      console.log('asdf');
      // if($(this).prop("checked") == true){
      // alert($(this).val());
      // }
      // else if($(this).prop("checked") == false){
      // alert("Checkbox is unchecked.");
      // }

      var checked = [];
      $("input[name='chooseProduct[]']:checked").each(function ()
      {
        checked.push($(this).val());
      });
      console.log(checked);
      // console.log('---New list---');
      // console.log(JSON.stringify(checked));
      // console.log('---Products list (toString)');
      // console.log(checked.toString());
      // console.log('---Products list (join)');
      // console.log(checked.join(' | '));
      $('#chosedProduct').html("<b>"+checked.join(' | ')+"</b>");

    });

  },
  FinishProduct:()=>{
    $("#productRow").removeClass('border-danger');
    $("#productRow").addClass('border-success');
    $("#productRow").addClass('disablebDiv');
    $("#dateRow").removeClass('disablebDiv');
    Ticket.ProgressBar(3);
    Ticket.ChooseDate();

    // $("[data-toggle=popoverPlatform]").popover('hide');
  },
  CloseProduct:()=>{
    Ticket.ProgressBar(2);
    Ticket.OpenPlatform();
  },
  ChooseDate:()=>{
    var myDate = document.getElementById('chooseDateInput');
    var today = new Date();
    myDate.value = today;
    today = today.toISOString().substr(0, 10);
    console.log('today');
    // console.log(today);
    document.getElementById('getDate').value = today;


    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    tomorrow = tomorrow.toISOString().substr(0, 10);
    // console.log(tomorrow);

    // var week = new Date();
    // week.setDate(week.getDate() + 7);
    // week = week.toISOString().substr(0, 10);


    let startDate = new Date();
    let chosedSchedule = $('input[name=choseSchedule]:checked').val();
    // console.log(chosedSchedule);
    // noOfDaysToAdd = 7;
    if(chosedSchedule==='Regular'){
      console.log(chosedSchedule);
      $("#chooseDateInput, #chooseTimeInput").hide();
      $("#chooseDateTimeInputDetails").removeClass('invisible');
      $("#chooseDateTimeInputDetails").change(function(){
        console.log(this.value);
        $("#chosedDate").html("<b>"+this.value+"</b>");
      });
    }
    let noOfDaysToAdd = (chosedSchedule==='Hotline')? 1 : 7;
    // let noOfDaysToAdd = (chosedSchedule==='Hotline')? 1 : (chosedSchedule==='Hotline'?7:14);
    // let noOfDaysToAdd = 1;
    var endDate = "", count = 0;
    while(count < noOfDaysToAdd){
      endDate = new Date(startDate.setDate(startDate.getDate() + 1));
      if(endDate.getDay() != 0 && endDate.getDay() != 6){
        //Date.getDay() gives weekday starting from 0(Sunday) to 6(Saturday)
        count++;
      }
    }
    let week = startDate.toISOString().substr(0, 10);

    myDate.value = today;
    myDate.setAttribute('max', week);

    $('#chooseDateInput').change(function(){
      console.log(this.value);
      $("#chosedDate").html("<b>"+this.value+"</b>");
    })
    $('#chooseTimeInput').change(function(){
      console.log(this.value);
      $("#chosedTime").html("<b>"+this.value+"</b>");
    })

  },
  CloseDate:()=>{
    Ticket.OpenProduct();
  },
  FinishDate:()=>{
    chosedDate = $("#chosedDate").text();
    console.log('chosedDate');
    console.log(chosedDate);

    // chosedDate.html()
    let myChosedDate = (chosedDate === 'Aguardando data') ? "<b>N/A</b>" : "<b>"+chosedDate+"</b>";
    // chosedDate.html(myChosedDate);
    $("#chosedDate").html(myChosedDate);


    // if(chosedDate){

    // }
    Ticket.ProgressBar(4);
    Ticket.ChooseContact();
    // Ticket.ChooseDate();
  },
  OpenDate:()=>{
    $("#contactRow").removeClass('border-danger');
    $("#contactRow").addClass('border-success');
    $("#contactRow").addClass('disablebDiv');
    $("#dateRow").removeClass('disablebDiv');
  },
  ChooseContact:()=>{
    $("#dateRow").removeClass('border-danger');
    $("#dateRow").addClass('border-success');
    $("#dateRow").addClass('disablebDiv');
    $("#contactRow").removeClass('disablebDiv');

    $( "#chooseContactName, #chooseContactEmail, #chooseContactNumber" ).keyup(function() {
      let name = $("#chooseContactName").val();
      let email = $("#chooseContactEmail").val();
      let number = $("#chooseContactNumber").val();

      // let output = this.value + " " + email + " " + number;
      let output = name + " " + email + " " + number;
      console.log(output);
      $("#chosedContact").html("<b>"+output+"</b>");
    });

  },
  CloseContact:()=>{
    Ticket.OpenDate();
    Ticket.ProgressBar(3);
  },

  GetDate:()=>{
    var myDate = document.getElementById('getDate');
    var today = new Date();
    today = today.toISOString().substr(0, 10);
    console.log('today');
    // console.log(today);
    document.getElementById('getDate').value = today;


    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    tomorrow = tomorrow.toISOString().substr(0, 10);
    // console.log(tomorrow);

    // var week = new Date();
    // week.setDate(week.getDate() + 7);
    // week = week.toISOString().substr(0, 10);


    let startDate = new Date();
    let chosedSchedule = $('input[name=choseSchedule]:checked').val();
    // console.log(chosedSchedule);
    // noOfDaysToAdd = 7;
    // let noOfDaysToAdd = (chosedSchedule==='Hotline')? 1 : 7;
    let noOfDaysToAdd = 1;
    var endDate = "", count = 0;
    while(count < noOfDaysToAdd){
      endDate = new Date(startDate.setDate(startDate.getDate() + 1));
      if(endDate.getDay() != 0 && endDate.getDay() != 6){
        //Date.getDay() gives weekday starting from 0(Sunday) to 6(Saturday)
        count++;
      }
    }
    let week = startDate.toISOString().substr(0, 10);

    myDate.value = today;
    myDate.setAttribute('max', week);

  },
  Scheduling:()=>{
    console.log('App.Scheduling');
    let scheduling = $('input[name=scheduling]:checked').val();
    console.log(scheduling);
    if(scheduling==='yes'){
      $("#type").text('Hotline / Line Express');
      $("#typeLine").addClass('bg-success');
      $("#dateChoose").show();
    }else{
      $("#type").text('Regular');
      $("#typeLine").addClass('bg-success');
      $("#dateChoose").hide();
    }
  },
  Platform:()=>{
    let platform = $('#getPlatform option:selected').text();
    $("#platform").text(platform);
    $("#plaftormLine").addClass('bg-success');
    console.log(platform);

    let hotline = [
      'Loja Integrada',
      'Shopify',
      'Xtech',
    ]
    let isHotline = hotline.includes(platform);
    if(isHotline){
      $("#type").text('Hotline');
      $("#typeLine").addClass('bg-success');
    }else{
      $("#type").text('Line Express');
      $("#typeLine").addClass('bg-success');
    }
  },
  ProgressBar:(steps)=>{
    // chooseRow
    console.log();

    $( ".chooseRow" ).each(function( index ) {
      // console.log( index + ": " + $( this ).text() );
      console.log( index );

    });

    var numItems = $('.chooseRow').length;
    // console.log('numItems');
    // console.log(numItems);
    // if()
    // console.log(steps);
    progressStatus = ( (steps*100) / numItems);
    progressStatusMsg = numItems-steps + " more to go!";

    // progressBar
    // $("#progressBar").width(30);
    $("#progressBar").css('width',progressStatus+"%");
    $("#progressBarMsg").html(progressStatusMsg);
    // $("progress-bar").css('width',progressStatus);

  }
}
</script>


<!-- <script src="PPEC/jquery-3.3.1.min.js"></script> -->
<!-- <script src="{{url('js/sub/script.js')}}"></script> -->

<style type="text/css">
#CreatePaymentJsonResponseOutput,
#ExecutePaymentJsonResponseOutput {
  font-size: 10pt;
}

textarea {
  resize: none;
  overflow: hidden;
  border: none !important;
  font-size: 7pt !important;
  color: black !important;
}
.codeFont{
  color:#02cf92;
}
.accessTokenTag{
  color: #f59000;
}

</style>


<script>
</script>
@endsection
