@extends('layouts.master')
@section('content')


<div class="container">
    <div class="row">
        <div class="col">
            <center>
                <button class="btn btn-success" id="createInvoicingBtn">Create invoicing</button>
            </center>
        </div>
        <div class="col">
            <center>
                <span id='invoicingId'></span>
            </center>
        </div>
        <div class="col">
            <center>
                <button class="btn btn-success" id="sendInvoicingBtn">Send invoicing</button>
            </center>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col">

            <!-- acordion -->
            <div id="accordion">

                <!--Get Token-->
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <!--<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->

                  Access Token
                </button>
                @if(Session::has('expiredTime'))
                  <small> <span class="badge badge-{{Session::get('hasExpired')}} float-right">{{Session::get('expiredTime')}}</span></small>
                @endif

              </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Response</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Create Invoicing-->
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Create Invoicing
                </button>
                <span id="invoicingCreateWaitingNotification" class="badge badge-warning float-right">Waiting</span>
                <span id="invoicingCreateNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-group">
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request (cURL)</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v https://api.sandbox.paypal.com/v1/invoicing/invoices/ \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        &nbsp; -d <span class="codeFont">'{ <br/>
                                          &emsp;{</br>
                                          &emsp;"merchant_info": {</br>
                                            &emsp;&emsp;"email": "merchant@example.com",</br>
                                            &emsp;&emsp;"first_name": "David",</br>
                                            &emsp;&emsp;"last_name": "Larusso",</br>
                                            &emsp;&emsp;"business_name": "Mitchell & Murray",</br>
                                            &emsp;&emsp;"phone": {</br>
                                              &emsp;&emsp;&emsp;"country_code": "001",</br>
                                              &emsp;&emsp;&emsp;"national_number": "4085551234"</br>
                                            &emsp;&emsp;},</br>
                                            &emsp;&emsp;"address": {</br>
                                              &emsp;&emsp;&emsp;"line1": "1234 First Street",</br>
                                              &emsp;&emsp;&emsp;"city": "Anytown",</br>
                                              &emsp;&emsp;&emsp;"state": "CA",</br>
                                              &emsp;&emsp;&emsp;"postal_code": "98765",</br>
                                              &emsp;&emsp;&emsp;"country_code": "US"</br>
                                            &emsp;&emsp;}</br>
                                          &emsp;},</br>
                                          &emsp;"billing_info": [</br>
                                            &emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;"email": "bill-me@example.com",</br>
                                              &emsp;&emsp;&emsp;"first_name": "Stephanie",</br>
                                              &emsp;&emsp;&emsp;"last_name": "Meyers"</br>
                                            &emsp;&emsp;}</br>
                                          &emsp;],</br>
                                          &emsp;"shipping_info": {</br>
                                            &emsp;&emsp;"first_name": "Stephanie",</br>
                                            &emsp;&emsp;"last_name": "Meyers",</br>
                                            &emsp;&emsp;"address": {</br>
                                              &emsp;&emsp;&emsp;"line1": "1234 Main Street",</br>
                                              &emsp;&emsp;&emsp;"city": "Anytown",</br>
                                              &emsp;&emsp;&emsp;"state": "CA",</br>
                                              &emsp;&emsp;&emsp;"postal_code": "98765",</br>
                                              &emsp;&emsp;&emsp;"country_code": "US"</br>
                                            &emsp;&emsp;}</br>
                                          &emsp;},</br>
                                          &emsp;"items": [</br>
                                            &emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;"name": "Zoom System wireless headphones",</br>
                                              &emsp;&emsp;&emsp;"quantity": 2,</br>
                                              &emsp;&emsp;&emsp;"unit_price": {</br>
                                                &emsp;&emsp;&emsp;&emsp;"currency": "USD",&emsp;&emsp;&emsp;</br>
                                              &emsp;&emsp;&emsp;&emsp;"value": "120"</br>
                                              &emsp;&emsp;&emsp;},</br>
                                              &emsp;&emsp;&emsp;"tax": {</br>
                                                &emsp;&emsp;&emsp;&emsp;"name": "Tax",</br>
                                                &emsp;&emsp;&emsp;&emsp;"percent": 8</br>
                                              &emsp;&emsp;&emsp;}</br>
                                            &emsp;&emsp;},</br>
                                            &emsp;&emsp;{</br>
                                              &emsp;&emsp;&emsp;"name": "Bluetooth speaker",</br>
                                              &emsp;&emsp;&emsp;"quantity": 1,</br>
                                              &emsp;&emsp;&emsp;"unit_price": {</br>
                                                &emsp;&emsp;&emsp;&emsp;"currency": "USD",</br>
                                                &emsp;&emsp;&emsp;&emsp;"value": "145"</br>
                                              &emsp;&emsp;&emsp;},</br>
                                              &emsp;&emsp;&emsp;"tax": {</br>
                                                &emsp;&emsp;&emsp;&emsp;"name": "Tax",</br>
                                                &emsp;&emsp;&emsp;&emsp;"percent": 8</br>
                                              &emsp;&emsp;&emsp;}</br>
                                            &emsp;&emsp;}</br>
                                          &emsp;],</br>
                                          &emsp;"discount": {</br>
                                            &emsp;&emsp;"percent": 1</br>
                                          &emsp;},</br>
                                          &emsp;"shipping_cost": {</br>
                                            &emsp;&emsp;"amount": {</br>
                                              &emsp;&emsp;&emsp;"currency": "USD",</br>
                                              &emsp;&emsp;&emsp;"value": "10"</br>
                                            &emsp;}</br>
                                          &emsp;},</br>
                                          &emsp;"note": "Thank you for your business.",</br>
                                          &emsp;"terms": "No refunds after 30 days."</br>
                                           }' </span><br/>
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>

                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Get token:</b> <a href="#">Link</a></li>
                                  <li class="list-group-item"><b>Trigger:</b> Page load (payment option check etc)</li>
                                  <li class="list-group-item"><b>Action:</b> Renders the PayPal Plus form</li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#define-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#define-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#define-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#define-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#define-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#define-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>

                                  </div>



                            </div>
                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="CreatePaymentJsonResponseOutput"></div>-->
                                    <div class="form-group textarea-div">

                                        <textarea id="InvoicingCreateJsonResponseOutputTextArea" class="form-control" readonly> </textarea>
                                        <p class="card-text"><small class="text-muted"><a href="#" id="searchIdInvoicing"></a></small></p>


                                    </div>
                                    <p class="card-text"><small class="text-muted" id="invoicingCreateTimeDiv">Aguardando a requisição</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--Execute Payment-->
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Execute Invoicing
                </button>
                <span id="invoicingExecuteWaitingNotification" class="badge badge-warning float-right">Waiting</span>

                <span id="invoicingExecuteNotification" class="badge badge-success float-right"></span>
              </h5>
                    </div>

                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-group">

                                                        <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Request</h5>
                                    <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                                      <p class="card-text bg-dark text-light">
                                        &nbsp; curl -v https://api.sandbox.paypal.com/v1/payments/payment/PAY-9N9834337A9191208KOZOQWI/execuhttps://api.sandbox.paypal.com/v1/invoicing/invoices/INV2-EHNV-LJ5S-A7DZ-V6NJ/send?notify_merchant=true \ <br/>
                                        &nbsp; -H "Content-Type: application/json" \ <br/>
                                        &nbsp; -H "Authorization: Bearer <a href="#"><span class="accessTokenTag">&lt;Access-Token&gt;	</span></a> " \ <br/>
                                        <!-- &nbsp; -d <span class="codeFont">'{ <br/>
                                        &emsp; "payer_id": "CR87QHB7JTRSC" <br/>
                                        &nbsp; }' </span><br/> -->
                                      </p>
                                    <p class="card-text"><small class="text-muted">Mais exemplos de samples abaixo</small></p>


                                </div>

                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button  </li>
                                  <!-- <li class="list-group-item"><b>Trigger:</b> PayPal Checkout Button <img src="PPEC/pp-ec-button.png"></img> </li> -->
                                  <li class="list-group-item"><b>Action:</b> Calls the incontext PayPal Auth form
                                  <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                      Exibir
                                    </button>
                                  </li>
                                  <li class="list-group-item"><b>Source:</b> createPayment( ) function</li>
                                  <li class="list-group-item"><b>Parameters:</b> <a href="https://developer.paypal.com/docs/api/payments/v1/" target="_blank">See all</a></li>
                                </ul>

                                  <div class="card-footer text-muted">
                                      <!--<a href="#" class="btn btn-primary">Node</a>-->
                                      <!--<a href="#" class="btn btn-primary">PHP</a>-->
                                      <!--<a href="#" class="btn btn-primary">Python</a>-->
                                      <!--<a href="#" class="btn btn-primary">Ruby</a>-->
                                      <!--<a href="#" class="btn btn-primary">Java</a>-->
                                      <!--<a href="#" class="btn btn-primary">.Net</a>-->
                                      <!--<a href="#" class="btn btn-primary">Postman</a>-->

                                      <div class="btn-group" role="group" aria-label="Basic example">
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=node#execute-payment" class="btn btn-primary" target="_blank">Node</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=php#execute-payment" class="btn btn-primary" target="_blank">PHP</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=python#execute-payment" class="btn btn-primary" target="_blank">Python</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=ruby#execute-payment" class="btn btn-primary" target="_blank">Ruby</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=java#execute-payment" class="btn btn-primary" target="_blank">Java</a>
                                          <a href="https://developer.paypal.com/docs/api/quickstart/payments/?mark=.net#execute-payment" class="btn btn-primary" target="_blank">.Net</a>
                                          <a href="https://developer.paypal.com/docs/api/overview/#make-your-first-call" class="btn btn-primary" target="_blank">Postman</a>
                                      </div>
                                  </div>
                            </div>


                            <div class="card">

                                <div class="card-body">
                                    <h5 class="card-title">Response JSON</h5>
                                    <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                                    <!--<div id="ExecutePaymentJsonResponseOutput"></div>-->
                                    <textarea id="InvoicingExecuteJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                                    <p class="card-text"><small class="text-muted" id="invoicingExecuteTimeDiv">Aguardando a requisição</small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- acordion -->

        </div>
    </div>
</div>
<!--here-->

<br>


<script src="{{url('js/invoicing/script.js')}}" type="text/javascript"> </script>



<style type="text/css">
    #CreatePaymentJsonResponseOutput,
    #ExecutePaymentJsonResponseOutput {
        font-size: 10pt;
    }

    textarea {
        resize: none;
        overflow: hidden;
        border: none !important;
        font-size: 7pt !important;
        color: black !important;
    }
    .codeFont{
        color:#02cf92;
    }
    .accessTokenTag{
        color: #f59000;
    }

</style>

@endsection
