@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <!-- <div class="card">
        <div class="card-header">Welcome</div>
        <img src="{{url('/img/paypal-logo.png')}}" class="card-img-top" alt="...">

        <div class="card-body">

          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          <p class="text-success">
            You are logged in!
          </p>
          <hr>
          <a target="_blank" href="{{route('credential.index')}}">
            First set your merchant credentials
          </a>
          <hr>
          <a target="_blank" href="{{route('token.index')}}">
            Now check for its access token
          </a>
        </div>
      </div> -->
      <!--  -->
      <br>

      <br>
      <div class="card mb-3">
        <img src="{{url('/img/paypal-logo.png')}}" class="card-img-top" alt="...">
        <br>

        <div class="card-body">
          <h5 class="card-title">Welcome,
            @auth
              {{Auth::user()->name}}!
            @endauth
          </h5>
          <br>
          <p class="text-secondary">This is your new PayPal development environment.</p>
          <p class="text-muted">First configure and enable your
            <a target="_blank" href="{{route('credential.index')}}">credentials</a>. You can have as many credentials as you want. Then check the expiration time for its
            <a target="_blank" href="{{route('token.index')}}"> access token</a>. You can also create and configure new webhooks for each of these credentials.</p>
          <p class="card-text"><small class="text-success">You are logged in</small></p>
        </div>
      </div>


      <!-- <div class="card">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
        <img src="{{url('/img/paypal-logo.png')}}" class="card-img-top" alt="...">
      </div> -->

      <!--  -->
    </div>
  </div>
</div>

<style>
  .card{
    padding:30px;
    border: solid #d9d9d9 1px;
    background-color: #f2f2f2;
    -webkit-box-shadow: 5px 5px 15px 2px rgba(134,134,134,0.68);
    box-shadow: 5px 5px 15px 2px rgba(134,134,134,0.68);  }
  img{
    max-width: 200px;
    margin-left:20px
  }
</style>
@endsection
