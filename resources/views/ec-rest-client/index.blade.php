@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

          @if(session()->has('msg'))
            <div class="alert {{Session::get('alert')}} alert-dismissible fade show" role="alert">
              <!-- <strong>Holy guacamole!</strong>  -->
              {{ Session::get('msg') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @else
          @endif


            <div class="card">
                <div class="card-header">Ec Rest Client</div>



                <div class="card-body">
                      <script src="https://www.paypalobjects.com/api/checkout.js"></script>

                      <div id="paypal-button"></div>

                      <script src="{{url('js/ec-rest-client.js')}}"></script>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection
