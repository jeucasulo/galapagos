@extends('layouts.master')

@section('content')


<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">


      <div class="card">
        <div class="card-header">Error messages</div>



        <div class="card-body">

          <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for errors messages..." class="form-control">

          <div class="myUL">

<!-- link -->
<!-- https://developer.paypal.com/docs/api/payments/v1/?mark=malfor#error-MALFORMED_REQUEST -->

            <br>
              <h3>Error messages</h3>
            <br>

            <div class="newerror">
              <p>
                <b>AGREEMENT_ALREADY_CANCELLED</b>
              </p>
              <p>
                The requested agreement is already canceled. The agreement that was used to make the payment is already canceled.
              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>AMOUNT_MISMATCH</b>
              </p>
              <p>
                The totals of the cart item amounts do not match sale amounts. The amount total does not match the item amount totals.
              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>AUTH_SETTLE_NOT_ALLOWED</b>
              </p>
              <p>
                Authorization and Capture feature is not enabled for the merchant. Make sure that the recipient of the funds is a verified business account.

              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>AUTHORIZATION_ALREADY_COMPLETED</b>
              </p>
              <p>
                Capture refused - this authorization has already been completed. The capture on the authorization failed because it was already completed by one or more previous captures on this authorization.

              </p>
              <hr>
            </div>

            <div class="newerror">
              <p>
                <b>AUTHORIZATION_AMOUNT_LIMIT_EXCEEDED</b>
              </p>
              <p>
                Authorization amount exceeds allowed order limit. The authorization amount exceeds the allowed order limit.

              </p>
              <hr>
            </div>











          </div>
        </div>


      </div>
    </div>
  </div>
</div>
  <style media="screen">

  html {
    scroll-behavior: smooth;
  }

  </style>

  <script>
  function myFunction() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    // li = ul.getElementsByTagName('li');
    li = document.getElementsByClassName('newerror');
    // console.log(li)

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      b = li[i].getElementsByTagName("b")[0];
      // console.log(b);
      txtValue = b.textContent || b.innerText;
      // console.log(txtValue);
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }
  </script>


  @endsection
