@extends('layouts.master')

@section('content')


<div class="container">
  <div class="row">
    <div class="col">


      @if(session()->has('msg'))
      <div class="alert {{Session::get('alert')}} alert-dismissible fade show" role="alert">
        <!-- <strong>Holy guacamole!</strong>  -->
        {{ Session::get('msg') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @else
      @endif




      <div class="card">
        <div class="card-header">Token</div>

        <div class="card-body">
          <!-- <h3>Credentials</h3> -->
          <div class="row">
            <div class="col-2">
              <h6><b>App name:</b></h6>
              <h6><b>Details: </b></h6>

            </div>
            <div class="col-5">
              <h6>{{$activeCredential->app_name}}</h6>
              <h6>{{$activeCredential->app_desc}}</h6>
            </div>

            <div class="col-5">
              <small> <span class="badge badge-success float-right">Credentials</span></small>
            </div>
          </div>

        </div>
        <hr>
        <div class="card-body">
          <!-- <h3>Credentials</h3> -->
          <div class="row">
            <div class="col-9">
              @if($fullAccessToken)
              <h6><b>Scope: </b> <i>  {{$fullAccessToken->scope}} </i></h6>
              <h6><b>Access Token:</b><i>  {{$fullAccessToken->access_token}} </i></h6>
              <h6><b>Type:</b><i>  {{$fullAccessToken->token_type}} </i></h6>
              <h6><b>Nonce:</b><i>  {{$fullAccessToken->nonce}} </i></h6>
              <h6><b>Expires:</b><i>  {{$fullAccessToken->expires_in}} </i></h6>
              @else
            
              @endif
            </div>


            <div class="col-3">
              <small> <span class="badge badge-{{$hasExpired}} float-right">{{$expiredTime}}</span></small>
            </div>
          </div>

        </div>
        <hr>
        <div class="card-body">
          <div class="">

            <!-- <h3>Token</h3> -->
            @if($activeToken)
            <p hidden>{{$activeToken->access_token}} </p>
            <p hidden>{{$activeToken->token_type}} </p>
            <p hidden>{{$activeToken->nonce}} </p>
            <p hidden>{{$activeToken->created_at}} </p>
            <p hidden>{{$activeToken->updated_at}} </p>
            <p hidden>Created: {{$activeToken->updated_at->diffForHumans()}}</p><br>
            @else
            Nenhum token criado...
            @endif
            <br>

          </div>
          <div class="">
            <!-- <form class="" action="{{route('token.store')}}" method="POST" class="form-horizontal"> -->
            <form class="" class="form-horizontal">
              {{ csrf_field() }}

              <input type="text" id="client_id" name="client_id" value="{{$activeCredential->client_id}}" class="form-control d-none">
              <input type="text" id="secret" name="secret" value="{{$activeCredential->secret}}" class="form-control d-none">
              <input type="text" id="app_name" name="app_name" value="{{$activeCredential->app_name}}" class="form-control d-none">
              <input type="text" id="app_desc" name="app_desc" value="{{$activeCredential->app_desc}}" class="form-control d-none">
              <input type="text" id="app_id" name="app_id" value="{{$activeCredential->id}}" class="form-control d-none">

              <!-- Add Task Button -->
              <div class="form-group">

                <button id="GetAccessTokenBtn" type="button" class="btn btn-success">
                  Get Access Token
                </button>

              </div>

            </form>
            <!-- <a href="{{route('token.store')}}" class="btn btn-success">Get Access Token</a> -->
          </div>

        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row">
    <div class="col">

      <!--HERE-->

      <div id="accordion">

        <!--Get Token-->
        <div class="card">
          <div class="card-header" id="headingThree">
            <h5 class="mb-0">
              <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <!--<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->

                Get Access Token
              </button>

              <span id="getTokenWaitingNotification" class="badge badge-warning float-right">Waiting</span>
              <span id="getTokenNotification" class="badge badge-success float-right"></span>

            </h5>
          </div>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-group">

              <div class="card">

                <div class="card-body">
                  <h5 class="card-title">Request</h5>
                  <!--<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->

                  <p class="card-text bg-dark text-light">
                    &nbsp; curl -v https://api.sandbox.paypal.com/v1/oauth2/token \ <br/>
                    &nbsp; -H "Content-Type: application/json" \ <br/>
                    &nbsp; -H "Accept-Language: en_US"  \ <br/>
                    &nbsp; -u <span class="client-secret-tag">"&lt;client_id:secret&gt;"</span> \<br/>
                    &nbsp; -d <span class="codeFont"> "grant_type=client_credentials"</span>
                  </p>
                  <p class="card-text"><small class="text-muted">Requisição em cURL</small></p>
                  <!--source: https://developer.paypal.com/docs/api/overview/#make-your-first-call#postman-->


                </div>

                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><b>Trigger:</b> Get Token Button</li>
                  <li class="list-group-item"><b>Action:</b> Sends a auth request and returns the Token</li>
                  <li class="list-group-item"><b>Source:</b> getToken( ) function</li>
                </ul>

                <div class="card-footer text-muted">

                </div>



              </div>


              <div class="card">

                <div class="card-body">
                  <h5 class="card-title">Response JSON</h5>
                  <!--<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>-->
                  <!--<div id="ExecutePaymentJsonResponseOutput"></div>-->
                  <textarea id="getTokenJsonResponseOutputTextArea" class="form-control" readonly> </textarea>

                  <p class="card-text"><small class="text-muted" id="tokenTimeDiv">Aguardando a requisição</small></p>
                </div>
              </div>
            </div>


          </div>
        </div>




      </div>

      <!--HERE-->


    </div>
  </div>
</div>


<script src="{{url('js/token/script.js')}}"></script>

<style type="text/css">
#CreatePaymentJsonResponseOutput,
#ExecutePaymentJsonResponseOutput {
  font-size: 10pt;
}

textarea {
  resize: none;
  overflow: hidden;
  border: none !important;
  font-size: 7pt !important;
  color: black !important;
}
.codeFont{
  color:#02cf92;
}
.client-secret-tag{
  color: #f59000;
}
button{
  border-radius:25px!important;
}

</style>

@endsection
