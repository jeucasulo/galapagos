<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>Events</title>
  </head>
  <body>

    <div class="container">
        <div class="row">
            <div id="">
                    <h1>Events</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div id="output">
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>


<script>

$.getJSON( "_webhookTestCollection.json", function( data ) {
    console.log(data);
  var items = [];
  $.each( data, function( key, val ) {
    items.push( "<tr id='" + key + "'>"
    + "<td><a target='_blank' href='https://webhook-jj.000webhostapp.com/details.php?id="+val.transaction.id+"'>" + val.transaction.id + " </td> "
    + "<td>" + val.transaction.create_time + " </td> "
    + "<td>" + val.transaction.event_type + " </td> "
    // + "<td>" + val.transaction.resource.amount.total + " </td> "
    + "<td>" + val.transaction.resource.id + " </td> "

    + "</tr>" );
    console.log("val: " + val.transaction.verify_sign);
  });

  $( "<table/>", {
    "class": "table table-dark",
    html: items.join( "" )
  }).appendTo("#output");
    // console.log(data[0].transaction.payment_type);
});
</script>
